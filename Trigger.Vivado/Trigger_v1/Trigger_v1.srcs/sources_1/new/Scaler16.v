`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08/27/2019 02:44:19 PM
// Design Name: 
// Module Name: Scaler16
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Scaler16(
    input  wire aclk,
    input  wire aresetn,
    input  wire enable,
    input  wire sin,
    output wire [15:0] sout
    );
    
    reg [15:0] rSCALER;
    reg rFLAG;

    assign sout = rSCALER;
         
    always @( posedge aclk )
    begin
      if ( aresetn == 1'b0 ) begin
        rSCALER <= 16'h0000;
        rFLAG   <= 1'b0;
      end
      else begin
        if ( enable == 1'b1 ) begin
          if ( sin == 1'b1 ) begin
            if ( rFLAG == 1'b0 ) begin
              rSCALER <= rSCALER + 1'b1;
              rFLAG   <= 1'b1;
            end
            else begin
              rSCALER <= rSCALER;
              rFLAG   <= rFLAG;
            end
          end
          else begin
            rSCALER <= rSCALER;
            rFLAG   <= 1'b0;
          end
        end
        else begin
          rSCALER <= rSCALER;
          rFLAG   <= rFLAG;
        end
      end
    end
     
endmodule
