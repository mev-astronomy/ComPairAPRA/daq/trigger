`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/20/2019 03:41:23 PM
// Design Name: 
// Module Name: trg_ack_gen
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//
// -- 2021Jan31
//      trg_ack --> trg_ack_long (controlled by register_1[15:8]
// 2021Jan31 --
//
//////////////////////////////////////////////////////////////////////////////////


module trg_ack_gen(
    input  wire axis_aresetn,
    input  wire axis_aclk,
    input  wire wt_ready,
    input  wire trg_resetn,
    input  wire trg_enable,
    input  wire gps_pps,
    input  wire swt_ack,
    input  wire ext_ack,
    input  wire lpc_lemo_ack,
    input  wire lpc_lemo_lock,
    input  wire hpc_lemo_ack,
    input  wire hpc_lemo_lock,
    input  wire [11:0] sit_hit_x,
    input  wire [11:0] sit_hit_y,
    input  wire csi_hit_1st,
    input  wire csi_hit_2nd,
    input  wire acd_hit_coin,
    input  wire sit_one_coin,
    input  wire sit_and_coin,
    input  wire sit_two_coin,
    input  wire czt_hit_coin,
    input  wire csi_hit_coin,
    input  wire acd_hit_lock,
    input  wire sit_hit_lock,
    input  wire czt_hit_lock,
    input  wire csi_hit_lock,
    input  wire [15:0] det_busy,
    input  wire [31:0] register_1,
    input  wire [31:0] register_2,
    input  wire [31:0] register_3,
    input  wire [31:0] register_4,
    input  wire [31:0] register_5,
    input  wire [31:0] register_6,
    input  wire [31:0] register_7,
    output wire wt_valid,
    output wire [31:0] dataBus,
    output wire [ 3:0] leds,
    output wire wt_event,
    output wire trg_ack_long,
    output wire evt_id_latch,
    output wire evt_id_data,
    output wire trg_lock,
    output wire npcs0
    );

    // ack_width == n_delay in event_id
    wire [7:0] ack_width;
    assign ack_width = register_1[15: 8];
    
    localparam IDLE=4'b0001, TRIG=4'b0010, FIFO=4'b0100, FLAG=4'b1000;
    reg [3:0] state;
    
    integer i;
    integer j;

    wire scl_enable = 1'b1;
    wire id_lock;
    assign npcs0 = !id_lock;
    
    reg  [31:0] data [0:63];
    wire [31:0] lck_scaler;        // [1]
    reg  [31:0] rRAW_GPS;          // [2]
    reg  [31:0] rRAW_CLK;          // [3]
    reg  [31:0] rRAW_CLK_ENA;      // [4]
    reg  [31:0] rRAW_CLK_LIV;      // [5]
    wire [31:0] gps_scaler;        // [6]
    wire [31:0] coi_pat_delay;     // [7]
    wire [31:0] hit_pat_delay;     // [8]
    reg  [31:0] rTIM_SECOND;       // [xx]
    reg  [31:0] rTIM_MICROS;       // [xx]
    wire [31:0] ack_scaler;        // [9]
    wire [15:0] swt_scaler;        // [10] LSB
    wire [15:0] ext_scaler;        // [10] MSB
    wire [15:0] lpc_scaler;        // [11] LSB
    wire [15:0] hpc_scaler;        // [11] MSB
    wire [15:0] cm_scaler [0:15];  // [12] - [27] LSB
    wire [15:0] cd_scaler [0:15];  // [12] - [27] MSB
    reg  [31:0] rRAW_CLK_ACD;      // [28]
    reg  [31:0] rRAW_CLK_CZT;      // [29]
    reg  [31:0] rRAW_CLK_CS1;      // [30]
    reg  [31:0] rRAW_CLK_CS2;      // [31]
    reg  [31:0] rRAW_CLK_SI0;      // [32]
    reg  [31:0] rRAW_CLK_SI1;      // [33]
    reg  [31:0] rRAW_CLK_SI2;      // [34]
    reg  [31:0] rRAW_CLK_SI3;      // [35]
    reg  [31:0] rRAW_CLK_SI4;      // [36]
    reg  [31:0] rRAW_CLK_SI5;      // [37]
    reg  [31:0] rRAW_CLK_SI6;      // [38]
    reg  [31:0] rRAW_CLK_SI7;      // [39]
    reg  [31:0] rRAW_CLK_SI8;      // [40]
    reg  [31:0] rRAW_CLK_SI9;      // [41]
    reg  [31:0] rRAW_CLK_SIA;      // [42]
    reg  [31:0] rRAW_CLK_SIB;      // [43]
    wire [31:0] acd_scaler;        // [44]
    wire [31:0] sit_one_scaler;    // [45]
    wire [31:0] sit_and_scaler;    // [46]
    wire [31:0] sit_two_scaler;    // [47]
    wire [31:0] czt_scaler;        // [48]
    wire [31:0] csi_1st_scaler;    // [49]
    wire [31:0] csi_2nd_scaler;    // [50]
    wire [15:0] six_scaler [0:11]; // [51] - [62] LSB
    wire [15:0] siy_scaler [0:11]; // [51] - [62] MSB
    
    reg [31:0] rDATA_BUS;    
    reg [5:0] rWT_COUNT;
    reg rWT_VALID;
    reg rWT_EVENT;
    reg [2:0] rFLAG_WAIT;
    
    reg [3:0] rLED;
    reg [1:0] rLED_SELECT;
        
    reg rTRG_HIT;
    reg rTRG_ACK;
    reg rTRG_LOCK;

    wire evt_ack;
    wire [15:0] mode_hit;
    wire [15:0] mode_ack;

    wire bd_resetn = axis_aresetn & trg_resetn;

    wire trg_hit = swt_ack || ext_ack || evt_ack || lpc_lemo_ack || hpc_lemo_ack;

    assign trg_ack  = rTRG_ACK;
    assign trg_lock = rTRG_LOCK | id_lock | acd_hit_lock | sit_hit_lock | czt_hit_lock | csi_hit_lock | lpc_lemo_lock | hpc_lemo_lock;
    
    // ------------------------
    // [7] Coincidence Pattern
    // ------------------------
    wire [31:0] coi_pat;
    assign coi_pat[31:16] = mode_ack;
    assign coi_pat[15: 0] = mode_hit;

    // ----------------
    // [8] Hit Pattern
    // ----------------
    wire [31:0] hit_pat;
    assign hit_pat[11: 0] = sit_hit_x;
    assign hit_pat[12] = acd_hit_coin;
    assign hit_pat[13] = czt_hit_coin;
    assign hit_pat[14] = csi_hit_1st;
    assign hit_pat[15] = csi_hit_2nd;
    assign hit_pat[27:16] = sit_hit_y;
    assign hit_pat[28] = swt_ack;
    assign hit_pat[29] = ext_ack;
    assign hit_pat[30] = lpc_lemo_ack;
    assign hit_pat[31] = hpc_lemo_ack;
                 
    assign wt_event = rWT_EVENT;
    assign wt_valid = rWT_VALID;
    assign dataBus  = rDATA_BUS;
    
    assign leds[0] = trg_enable;
    assign leds[1] = rTRG_LOCK;
    assign leds[2] = wt_ready;
    assign leds[3] = wt_valid;
    
    evt_ack_gen evt_ack_inst (
    .axis_aresetn( axis_aresetn ),
    .axis_aclk   ( axis_aclk    ),
    .acd_hit_coin( acd_hit_coin ),
    .sit_one_coin( sit_one_coin ),
    .sit_and_coin( sit_and_coin ),
    .sit_two_coin( sit_two_coin ),
    .czt_hit_coin( czt_hit_coin ),
    .csi_hit_coin( csi_hit_coin ),
    .register_2  ( register_2   ),
    .register_3  ( register_3   ),
    .register_4  ( register_4   ),
    .register_5  ( register_5   ),
    .register_6  ( register_6   ),
    .register_7  ( register_7   ),
    .evt_ack     ( evt_ack      ),
    .mode_hit    ( mode_hit     ),
    .mode_ack    ( mode_ack     ) 
    );

    // TRG_ACK_LEN
    OneShot trg_ack_len (
    .aclk   ( axis_aclk        ),
    .aresetn( axis_aresetn     ),
    .sin    ( trg_ack          ),
    .width  ( ack_width        ),
    .sout   ( trg_ack_long     )
    );
            
    always @( posedge axis_aclk )
    begin
      if ( axis_aresetn == 1'b0 ) begin
        state <= IDLE;
      end
      else begin
        case (state)
          IDLE: if ( trg_lock == 1'b0 && trg_enable == 1'b1 && trg_hit == 1'b1 ) begin
                  state <= TRIG;
                end
                else begin
                  state <= IDLE;
                end
          TRIG: begin
                  state <= FIFO;
                end
          FIFO: if ( rWT_COUNT == 63 && wt_ready == 1'b1 ) begin
                  state <= FLAG;
                end
                else begin
                  state <= FIFO;
                end
          FLAG: if ( rFLAG_WAIT == 5 ) begin
                  state <= IDLE;
                end
                else begin
                  state <= FLAG;
                end
          default: begin
                     state <= IDLE;
                   end
        endcase
      end
    end
        
    always @( posedge axis_aclk )
    begin
      if ( axis_aresetn == 1'b0 ) begin
        rTRG_ACK   <= 1'b0;
        rTRG_LOCK  <= 1'b0;
        rWT_COUNT  <= 6'b000000;
        rFLAG_WAIT <= 3'b000;
        rWT_VALID  <= 1'b0;
        rWT_EVENT  <= 1'b0;
        rDATA_BUS  <= data[rWT_COUNT];          
      end
      else begin
        case (state)
        IDLE: begin
                rTRG_ACK   <= 1'b0;
                rTRG_LOCK  <= 1'b0;
                rWT_COUNT  <= 6'b000000;
                rFLAG_WAIT <= 3'b000;
                rWT_VALID  <= 1'b0;
                rWT_EVENT  <= 1'b0;
                rDATA_BUS  <= data[rWT_COUNT];
              end
        TRIG: begin
                rTRG_ACK   <= 1'b1;
                rTRG_LOCK  <= 1'b1;
                rWT_COUNT  <= 6'b000000;
                rFLAG_WAIT <= 3'b000;
                rWT_VALID  <= 1'b0;
                rWT_EVENT  <= 1'b0;
                rDATA_BUS  <= data[rWT_COUNT];
              end
        FIFO: begin
                rTRG_ACK   <= 1'b0;
                rTRG_LOCK  <= 1'b1;
                rFLAG_WAIT <= 3'b000;
                rWT_VALID  <= 1'b1;
                rWT_EVENT  <= 1'b0;
                rDATA_BUS  <= data[rWT_COUNT];
                if ( wt_ready == 1'b1 ) begin
                  rWT_COUNT <= rWT_COUNT + 1;
                end
                else begin
                  rWT_COUNT <= rWT_COUNT;
                end
              end
        FLAG: begin
                rTRG_ACK   <= 1'b0;
                rTRG_LOCK  <= 1'b1;
                rWT_COUNT  <= 6'b000000;
                rFLAG_WAIT <= rFLAG_WAIT + 1;
                rWT_VALID  <= 1'b0;
                rWT_EVENT  <= 1'b1;
                rDATA_BUS  <= data[rWT_COUNT];
              end
        default: begin
                   rTRG_ACK   <= 1'b0;
                   rTRG_LOCK  <= 1'b0;
                   rWT_COUNT  <= 6'b000000;
                   rFLAG_WAIT <= 3'b0;
                   rWT_VALID  <= 1'b0;
                   rWT_EVENT  <= 1'b0;
                   rDATA_BUS  <= data[rWT_COUNT];
                 end
        endcase
      end
    end

    // ------------------------
    // [1] EventID (Locked Ack)
    // ------------------------
    // Scaler32 lck_scaler_inst (
    // .aclk   ( axis_aclk  ),
    // .aresetn( axis_aresetn  ),
    // .enable ( scl_enable ),
    // .sin    ( rTRG_ACK   ),
    // .sout   ( lck_scaler )
    // );
    event_id event_id_inst (
    .aclk        ( axis_aclk    ),
    .aresetn     ( axis_aresetn ),
    .trg_ack     ( trg_ack      ),
    .register_1  ( register_1   ),
    .busy        ( id_lock      ),
    .evt_id_latch( evt_id_latch ),
    .evt_id_data ( evt_id_data  ),
    .evt_id      ( lck_scaler   )
    );
        
    // -------------------
    // [6] GPS PPS Scaler
    // -------------------
    Scaler32 gps_scaler_inst (
    .aclk   ( axis_aclk    ),
    .aresetn( axis_aresetn ),
    .enable ( scl_enable   ),
    .sin    ( gps_pps      ),
    .sout   ( gps_scaler   )
    );

    // ------------------------
    // [7] Coincidence Pattern
    // ------------------------
    Delay32 coi_pat_inst (
    .aclk   ( axis_aclk     ),
    .aresetn( axis_aresetn  ),
    .sin    ( coi_pat       ),
    .sout   ( coi_pat_delay )
    );
    
    // ----------------
    // [8] Hit Pattern
    // ----------------
    Delay32 hit_pat_inst (
    .aclk   ( axis_aclk     ),
    .aresetn( axis_aresetn  ),
    .sin    ( hit_pat       ),
    .sout   ( hit_pat_delay )
    );
    
    // ------------------------------------
    // [9] Trigger Ack Scaler (No Locked)
    // ------------------------------------
    Scaler32 ack_scaler_inst (
    .aclk   ( axis_aclk    ),
    .aresetn( axis_aresetn ),
    .enable ( scl_enable   ),
    .sin    ( trg_hit      ),
    .sout   ( ack_scaler   )
    );

    // -----------------------
    // [10] <LSB> Switch Ack Scaler
    // -----------------------
    Scaler16 swt_scaler_inst (
    .aclk   ( axis_aclk    ),
    .aresetn( axis_aresetn ),
    .enable ( scl_enable   ),
    .sin    ( swt_ack      ),
    .sout   ( swt_scaler   )
    );

    // -------------------------
    // [10] <MSB> External Ack Scaler
    // -------------------------
    Scaler16 ext_scaler_inst (
    .aclk   ( axis_aclk    ),
    .aresetn( axis_aresetn ),
    .enable ( scl_enable   ),
    .sin    ( ext_ack      ),
    .sout   ( ext_scaler   )
    );

    // -------------------------
    // [11] <LSB> LPC Lemo Ack Scaler
    // -------------------------
    Scaler16 lpc_scaler_inst (
    .aclk   ( axis_aclk    ),
    .aresetn( axis_aresetn ),
    .enable ( scl_enable   ),
    .sin    ( lpc_lemo_ack ),
    .sout   ( lpc_scaler   )
    );

    // -------------------------
    // [11] <MSB> HPC Lemo Ack Scaler
    // -------------------------
    Scaler16 hpc_scaler_inst (
    .aclk   ( axis_aclk    ),
    .aresetn( axis_aresetn ),
    .enable ( scl_enable   ),
    .sin    ( hpc_lemo_ack ),
    .sout   ( hpc_scaler   )
    );

    // -------------------------------------
    // [12] <LSB> Coincidence Mode 0 Scaler 
    // -------------------------------------
    Scaler16 cm_scaler_0_inst (
    .aclk   ( axis_aclk     ),
    .aresetn( axis_aresetn  ),
    .enable ( scl_enable    ),
    .sin    ( mode_hit [ 0] ),
    .sout   ( cm_scaler[ 0] )
    );
 
    // -------------------------------------
    // [13] <LSB> Coincidence Mode 1 Scaler 
    // -------------------------------------
    Scaler16 cm_scaler_1_inst (
    .aclk   ( axis_aclk     ),
    .aresetn( axis_aresetn  ),
    .enable ( scl_enable    ),
    .sin    ( mode_hit [ 1] ),
    .sout   ( cm_scaler[ 1] )
    );

    // -------------------------------------
    // [14] <LSB> Coincidence Mode 2 Scaler 
    // -------------------------------------
    Scaler16 cm_scaler_2_inst (
    .aclk   ( axis_aclk     ),
    .aresetn( axis_aresetn  ),
    .enable ( scl_enable    ),
    .sin    ( mode_hit [ 2] ),
    .sout   ( cm_scaler[ 2] )
    );
 
    // -------------------------------------
    // [15] <LSB> Coincidence Mode 3 Scaler 
    // -------------------------------------
    Scaler16 cm_scaler_3_inst (
    .aclk   ( axis_aclk     ),
    .aresetn( axis_aresetn  ),
    .enable ( scl_enable    ),
    .sin    ( mode_hit [ 3] ),
    .sout   ( cm_scaler[ 3] )
    );
        
    // -------------------------------------
    // [16] <LSB> Coincidence Mode 4 Scaler 
    // -------------------------------------
    Scaler16 cm_scaler_4_inst (
    .aclk   ( axis_aclk     ),
    .aresetn( axis_aresetn  ),
    .enable ( scl_enable    ),
    .sin    ( mode_hit [ 4] ),
    .sout   ( cm_scaler[ 4] )
    );
 
    // -------------------------------------
    // [17] <LSB> Coincidence Mode 5 Scaler 
    // -------------------------------------
    Scaler16 cm_scaler_5_inst (
    .aclk   ( axis_aclk     ),
    .aresetn( axis_aresetn  ),
    .enable ( scl_enable    ),
    .sin    ( mode_hit [ 5] ),
    .sout   ( cm_scaler[ 5] )
    );

    // -------------------------------------
    // [18] <LSB> Coincidence Mode 6 Scaler 
    // -------------------------------------
    Scaler16 cm_scaler_6_inst (
    .aclk   ( axis_aclk     ),
    .aresetn( axis_aresetn  ),
    .enable ( scl_enable    ),
    .sin    ( mode_hit [ 6] ),
    .sout   ( cm_scaler[ 6] )
    );
 
    // -------------------------------------
    // [19] <LSB> Coincidence Mode 7 Scaler 
    // -------------------------------------
    Scaler16 cm_scaler_7_inst (
    .aclk   ( axis_aclk     ),
    .aresetn( axis_aresetn  ),
    .enable ( scl_enable    ),
    .sin    ( mode_hit [ 7] ),
    .sout   ( cm_scaler[ 7] )
    );
        
    // -------------------------------------
    // [20] <LSB> Coincidence Mode 8 Scaler 
    // -------------------------------------
    Scaler16 cm_scaler_8_inst (
    .aclk   ( axis_aclk     ),
    .aresetn( axis_aresetn  ),
    .enable ( scl_enable    ),
    .sin    ( mode_hit [ 8] ),
    .sout   ( cm_scaler[ 8] )
    );
 
    // -------------------------------------
    // [21] <LSB> Coincidence Mode 9 Scaler 
    // -------------------------------------
    Scaler16 cm_scaler_9_inst (
    .aclk   ( axis_aclk     ),
    .aresetn( axis_aresetn  ),
    .enable ( scl_enable    ),
    .sin    ( mode_hit [ 9] ),
    .sout   ( cm_scaler[ 9] )
    );

    // -------------------------------------
    // [22] <LSB> Coincidence Mode A Scaler 
    // -------------------------------------
    Scaler16 cm_scaler_a_inst (
    .aclk   ( axis_aclk     ),
    .aresetn( axis_aresetn  ),
    .enable ( scl_enable    ),
    .sin    ( mode_hit [10] ),
    .sout   ( cm_scaler[10] )
    );
 
    // -------------------------------------
    // [23] <LSB> Coincidence Mode B Scaler 
    // -------------------------------------
    Scaler16 cm_scaler_b_inst (
    .aclk   ( axis_aclk     ),
    .aresetn( axis_aresetn  ),
    .enable ( scl_enable    ),
    .sin    ( mode_hit [11] ),
    .sout   ( cm_scaler[11] )
    );
        
    // -------------------------------------
    // [24] <LSB> Coincidence Mode C Scaler 
    // -------------------------------------
    Scaler16 cm_scaler_c_inst (
    .aclk   ( axis_aclk     ),
    .aresetn( axis_aresetn  ),
    .enable ( scl_enable    ),
    .sin    ( mode_hit [12] ),
    .sout   ( cm_scaler[12] )
    );
 
    // -------------------------------------
    // [25] <LSB> Coincidence Mode D Scaler 
    // -------------------------------------
    Scaler16 cm_scaler_d_inst (
    .aclk   ( axis_aclk     ),
    .aresetn( axis_aresetn  ),
    .enable ( scl_enable    ),
    .sin    ( mode_hit [13] ),
    .sout   ( cm_scaler[13] )
    );

    // -------------------------------------
    // [26] <LSB> Coincidence Mode E Scaler 
    // -------------------------------------
    Scaler16 cm_scaler_e_inst (
    .aclk   ( axis_aclk     ),
    .aresetn( axis_aresetn  ),
    .enable ( scl_enable    ),
    .sin    ( mode_hit [14] ),
    .sout   ( cm_scaler[14] )
    );
 
    // -------------------------------------
    // [27] <LSB> Coincidence Mode F Scaler 
    // -------------------------------------
    Scaler16 cm_scaler_f_inst (
    .aclk   ( axis_aclk     ),
    .aresetn( axis_aresetn  ),
    .enable ( scl_enable    ),
    .sin    ( mode_hit [15] ),
    .sout   ( cm_scaler[15] )
    );
        
    // ---------------------------------------
    // [12] <MSB> Coincidence Mode 0 CD Scaler 
    // ---------------------------------------
    Scaler16 cd_scaler_0_inst (
    .aclk   ( axis_aclk     ),
    .aresetn( axis_aresetn  ),
    .enable ( scl_enable    ),
    .sin    ( mode_ack [ 0] ),
    .sout   ( cd_scaler[ 0] )
    );
 
    // ---------------------------------------
    // [13] <BSB> Coincidence Mode 1 CD Scaler 
    // ---------------------------------------
    Scaler16 cd_scaler_1_inst (
    .aclk   ( axis_aclk     ),
    .aresetn( axis_aresetn  ),
    .enable ( scl_enable    ),
    .sin    ( mode_ack [ 1] ),
    .sout   ( cd_scaler[ 1] )
    );

    // ---------------------------------------
    // [14] <MSB> Coincidence Mode 2 CD Scaler 
    // ---------------------------------------
    Scaler16 cd_scaler_2_inst (
    .aclk   ( axis_aclk     ),
    .aresetn( axis_aresetn  ),
    .enable ( scl_enable    ),
    .sin    ( mode_ack [ 2] ),
    .sout   ( cd_scaler[ 2] )
    );
 
    // ---------------------------------------
    // [15] <MSB> Coincidence Mode 3 CD Scaler 
    // ---------------------------------------
    Scaler16 cd_scaler_3_inst (
    .aclk   ( axis_aclk     ),
    .aresetn( axis_aresetn  ),
    .enable ( scl_enable    ),
    .sin    ( mode_ack [ 3] ),
    .sout   ( cd_scaler[ 3] )
    );
        
    // ---------------------------------------
    // [16] <MSB> Coincidence Mode 4 CD Scaler 
    // ---------------------------------------
    Scaler16 cd_scaler_4_inst (
    .aclk   ( axis_aclk     ),
    .aresetn( axis_aresetn  ),
    .enable ( scl_enable    ),
    .sin    ( mode_ack [ 4] ),
    .sout   ( cd_scaler[ 4] )
    );
 
    // ---------------------------------------
    // [17] <MSB> Coincidence Mode 5 CD Scaler 
    // ---------------------------------------
    Scaler16 cd_scaler_5_inst (
    .aclk   ( axis_aclk     ),
    .aresetn( axis_aresetn  ),
    .enable ( scl_enable    ),
    .sin    ( mode_ack [ 5] ),
    .sout   ( cd_scaler[ 5] )
    );

    // ---------------------------------------
    // [18] <MSB> Coincidence Mode 6 CD Scaler 
    // ---------------------------------------
    Scaler16 cd_scaler_6_inst (
    .aclk   ( axis_aclk     ),
    .aresetn( axis_aresetn  ),
    .enable ( scl_enable    ),
    .sin    ( mode_ack [ 6] ),
    .sout   ( cd_scaler[ 6] )
    );
 
    // ---------------------------------------
    // [19] <MSB> Coincidence Mode 7 CD Scaler 
    // ---------------------------------------
    Scaler16 cd_scaler_7_inst (
    .aclk   ( axis_aclk     ),
    .aresetn( axis_aresetn  ),
    .enable ( scl_enable    ),
    .sin    ( mode_ack [ 7] ),
    .sout   ( cd_scaler[ 7] )
    );
        
    // ---------------------------------------
    // [20] <MSB> Coincidence Mode 8 CD Scaler 
    // ---------------------------------------
    Scaler16 cd_scaler_8_inst (
    .aclk   ( axis_aclk     ),
    .aresetn( axis_aresetn  ),
    .enable ( scl_enable    ),
    .sin    ( mode_ack [ 8] ),
    .sout   ( cd_scaler[ 8] )
    );
 
    // ---------------------------------------
    // [21] <MSB> Coincidence Mode 9 CD Scaler 
    // ---------------------------------------
    Scaler16 cd_scaler_9_inst (
    .aclk   ( axis_aclk     ),
    .aresetn( axis_aresetn  ),
    .enable ( scl_enable    ),
    .sin    ( mode_ack [ 9] ),
    .sout   ( cd_scaler[ 9] )
    );

    // ---------------------------------------
    // [22] <MSB> Coincidence Mode A CD Scaler 
    // ---------------------------------------
    Scaler16 cd_scaler_a_inst (
    .aclk   ( axis_aclk     ),
    .aresetn( axis_aresetn  ),
    .enable ( scl_enable    ),
    .sin    ( mode_ack [10] ),
    .sout   ( cd_scaler[10] )
    );
 
    // ---------------------------------------
    // [23] <MSB> Coincidence Mode B CD Scaler 
    // ---------------------------------------
    Scaler16 cd_scaler_b_inst (
    .aclk   ( axis_aclk     ),
    .aresetn( axis_aresetn  ),
    .enable ( scl_enable    ),
    .sin    ( mode_ack [11] ),
    .sout   ( cd_scaler[11] )
    );
        
    // ---------------------------------------
    // [24] <MSB> Coincidence Mode C CD Scaler 
    // ---------------------------------------
    Scaler16 cd_scaler_c_inst (
    .aclk   ( axis_aclk     ),
    .aresetn( axis_aresetn  ),
    .enable ( scl_enable    ),
    .sin    ( mode_ack [12] ),
    .sout   ( cd_scaler[12] )
    );
 
    // ---------------------------------------
    // [25] <MSB> Coincidence Mode D CD Scaler 
    // ---------------------------------------
    Scaler16 cd_scaler_d_inst (
    .aclk   ( axis_aclk     ),
    .aresetn( axis_aresetn  ),
    .enable ( scl_enable    ),
    .sin    ( mode_ack [13] ),
    .sout   ( cd_scaler[13] )
    );

    // ---------------------------------------
    // [26] <MSB> Coincidence Mode E CD Scaler 
    // ---------------------------------------
    Scaler16 cd_scaler_e_inst (
    .aclk   ( axis_aclk     ),
    .aresetn( axis_aresetn  ),
    .enable ( scl_enable    ),
    .sin    ( mode_ack [14] ),
    .sout   ( cd_scaler[14] )
    );
 
    // ---------------------------------------
    // [27] <MSB> Coincidence Mode F CD Scaler 
    // ---------------------------------------
    Scaler16 cd_scaler_f_inst (
    .aclk   ( axis_aclk     ),
    .aresetn( axis_aresetn  ),
    .enable ( scl_enable    ),
    .sin    ( mode_ack [15] ),
    .sout   ( cd_scaler[15] )
    );
        
    // ----------------
    // [44] ACD Scaler
    // ----------------
    Scaler32 acd_scaler_inst (
    .aclk   ( axis_aclk    ),
    .aresetn( axis_aresetn ),
    .enable ( scl_enable   ),
    .sin    ( acd_hit_coin ),
    .sout   ( acd_scaler   )
    );
    
    // -------------------------------
    // [45] Si-Tracker One Hit Scaler 
    // -------------------------------
    Scaler32 sit_one_scaler_inst (
    .aclk   ( axis_aclk      ),
    .aresetn( axis_aresetn   ),
    .enable ( scl_enable     ),
    .sin    ( sit_one_coin   ),
    .sout   ( sit_one_scaler )
    );
    
    // -------------------------------
    // [46] Si-Tracker And Hit Scaler
    // -------------------------------
    Scaler32 sit_and_scaler_inst (
    .aclk   ( axis_aclk      ),
    .aresetn( axis_aresetn   ),
    .enable ( scl_enable     ),
    .sin    ( sit_and_coin   ),
    .sout   ( sit_and_scaler )
    );
       
    // -------------------------------
    // [47] Si-Tracker Two Hit Scaler
    // -------------------------------
    Scaler32 sit_two_scaler_inst (
    .aclk   ( axis_aclk      ),
    .aresetn( axis_aresetn   ),
    .enable ( scl_enable     ),
    .sin    ( sit_two_coin   ),
    .sout   ( sit_two_scaler )
    );
    
    // --------------------
    // [48] CZT Hit Scaler
    // --------------------
    Scaler32 czt_scaler_inst (
    .aclk   ( axis_aclk    ),
    .aresetn( axis_aresetn ),
    .enable ( scl_enable   ),
    .sin    ( czt_hit_coin ),
    .sout   ( czt_scaler   )
    );
    
    // ------------------------
    // [49] CsI 1st Hit Scaler
    // ------------------------
    Scaler32 csi_1st_scaler_inst (
    .aclk   ( axis_aclk      ),
    .aresetn( axis_aresetn   ),
    .enable ( scl_enable     ),
    .sin    ( csi_hit_1st    ),
    .sout   ( csi_1st_scaler )
    );
    
    // ------------------------
    // [50] CsI 2nd Hit Scaler
    // ------------------------
    Scaler32 csi_2nd_scaler_inst (
    .aclk   ( axis_aclk      ),
    .aresetn( axis_aresetn   ),
    .enable ( scl_enable     ),
    .sin    ( csi_hit_2nd    ),
    .sout   ( csi_2nd_scaler )
    );
    
    // -------------------------------
    // [51] <LSB> Si-Tracker X 0 HIt Scaler
    // -------------------------------
    Scaler16 six_0_scaler_inst (
    .aclk   ( axis_aclk      ),
    .aresetn( axis_aresetn   ),
    .enable ( scl_enable     ),
    .sin    ( sit_hit_x [ 0] ),
    .sout   ( six_scaler[ 0] )
    );
    
    // -------------------------------
    // [52] <LSB> Si-Tracker X 1 HIt Scaler
    // -------------------------------
    Scaler16 six_1_scaler_inst (
    .aclk   ( axis_aclk      ),
    .aresetn( axis_aresetn   ),
    .enable ( scl_enable     ),
    .sin    ( sit_hit_x [ 1] ),
    .sout   ( six_scaler[ 1] )
    );
    
    // -------------------------------
    // [53] <LSB> Si-Tracker X 2 HIt Scaler
    // -------------------------------
    Scaler16 six_2_scaler_inst (
    .aclk   ( axis_aclk      ),
    .aresetn( axis_aresetn   ),
    .enable ( scl_enable     ),
    .sin    ( sit_hit_x [ 2] ),
    .sout   ( six_scaler[ 2] )
    );
    
    // -------------------------------
    // [54] <LSB> Si-Tracker X 3 HIt Scaler
    // -------------------------------
    Scaler16 six_3_scaler_inst (
    .aclk   ( axis_aclk      ),
    .aresetn( axis_aresetn   ),
    .enable ( scl_enable     ),
    .sin    ( sit_hit_x [ 3] ),
    .sout   ( six_scaler[ 3] )
    );
    
    // -------------------------------
    // [55] <LSB> Si-Tracker X 4 HIt Scaler
    // -------------------------------
    Scaler16 six_4_scaler_inst (
    .aclk   ( axis_aclk      ),
    .aresetn( axis_aresetn   ),
    .enable ( scl_enable     ),
    .sin    ( sit_hit_x [ 4] ),
    .sout   ( six_scaler[ 4] )
    );
    
    // -------------------------------
    // [56] <LSB> Si-Tracker X 5 HIt Scaler
    // -------------------------------
    Scaler16 six_5_scaler_inst (
    .aclk   ( axis_aclk      ),
    .aresetn( axis_aresetn   ),
    .enable ( scl_enable     ),
    .sin    ( sit_hit_x [ 5] ),
    .sout   ( six_scaler[ 5] )
    );
    
    // -------------------------------
    // [57] <LSB> Si-Tracker X 6 HIt Scaler
    // -------------------------------
    Scaler16 six_6_scaler_inst (
    .aclk   ( axis_aclk      ),
    .aresetn( axis_aresetn   ),
    .enable ( scl_enable     ),
    .sin    ( sit_hit_x [ 6] ),
    .sout   ( six_scaler[ 6] )
    );
    
    // -------------------------------
    // [58] <LSB> Si-Tracker X 7 HIt Scaler
    // -------------------------------
    Scaler16 six_7_scaler_inst (
    .aclk   ( axis_aclk      ),
    .aresetn( axis_aresetn   ),
    .enable ( scl_enable     ),
    .sin    ( sit_hit_x [ 7] ),
    .sout   ( six_scaler[ 7] )
    );
    
    // -------------------------------
    // [59] <LSB> Si-Tracker X 8 HIt Scaler
    // -------------------------------
    Scaler16 six_8_scaler_inst (
    .aclk   ( axis_aclk      ),
    .aresetn( axis_aresetn   ),
    .enable ( scl_enable     ),
    .sin    ( sit_hit_x [ 8] ),
    .sout   ( six_scaler[ 8] )
    );
    
    // -------------------------------
    // [60] <LSB> Si-Tracker X 9 HIt Scaler
    // -------------------------------
    Scaler16 six_9_scaler_inst (
    .aclk   ( axis_aclk      ),
    .aresetn( axis_aresetn   ),
    .enable ( scl_enable     ),
    .sin    ( sit_hit_x [ 9] ),
    .sout   ( six_scaler[ 9] )
    );
    
    // -------------------------------
    // [61] <LSB> Si-Tracker X A HIt Scaler
    // -------------------------------
    Scaler16 six_a_scaler_inst (
    .aclk   ( axis_aclk      ),
    .aresetn( axis_aresetn   ),
    .enable ( scl_enable     ),
    .sin    ( sit_hit_x [10] ),
    .sout   ( six_scaler[10] )
    );
    
    // -------------------------------
    // [62] <LSB> Si-Tracker X B HIt Scaler
    // -------------------------------
    Scaler16 six_b_scaler_inst (
    .aclk   ( axis_aclk      ),
    .aresetn( axis_aresetn   ),
    .enable ( scl_enable     ),
    .sin    ( sit_hit_x [11] ),
    .sout   ( six_scaler[11] )
    );
    
    // -------------------------------
    // [51] <MSB> Si-Tracker Y 0 HIt Scaler
    // -------------------------------
    Scaler16 siy_0_scaler_inst (
    .aclk   ( axis_aclk      ),
    .aresetn( axis_aresetn   ),
    .enable ( scl_enable     ),
    .sin    ( sit_hit_y [ 0] ),
    .sout   ( siy_scaler[ 0] )
    );
    
    // -------------------------------
    // [52] <MSB> Si-Tracker Y 1 HIt Scaler
    // -------------------------------
    Scaler16 siy_1_scaler_inst (
    .aclk   ( axis_aclk      ),
    .aresetn( axis_aresetn   ),
    .enable ( scl_enable     ),
    .sin    ( sit_hit_y [ 1] ),
    .sout   ( siy_scaler[ 1] )
    );
    
    // -------------------------------
    // [53] <MSB> Si-Tracker Y 2 HIt Scaler
    // -------------------------------
    Scaler16 siy_2_scaler_inst (
    .aclk   ( axis_aclk      ),
    .aresetn( axis_aresetn   ),
    .enable ( scl_enable     ),
    .sin    ( sit_hit_y [ 2] ),
    .sout   ( siy_scaler[ 2] )
    );
    
    // -------------------------------
    // [54] <MSB> Si-Tracker Y 3 HIt Scaler
    // -------------------------------
    Scaler16 siy_3_scaler_inst (
    .aclk   ( axis_aclk      ),
    .aresetn( axis_aresetn   ),
    .enable ( scl_enable     ),
    .sin    ( sit_hit_y [ 3] ),
    .sout   ( siy_scaler[ 3] )
    );
    
    // -------------------------------
    // [55] <MSB> Si-Tracker Y 4 HIt Scaler
    // -------------------------------
    Scaler16 siy_4_scaler_inst (
    .aclk   ( axis_aclk      ),
    .aresetn( axis_aresetn   ),
    .enable ( scl_enable     ),
    .sin    ( sit_hit_y [ 4] ),
    .sout   ( siy_scaler[ 4] )
    );
    
    // -------------------------------
    // [56] <MSB> Si-Tracker Y 5 HIt Scaler
    // -------------------------------
    Scaler16 siy_5_scaler_inst (
    .aclk   ( axis_aclk      ),
    .aresetn( axis_aresetn   ),
    .enable ( scl_enable     ),
    .sin    ( sit_hit_y [ 5] ),
    .sout   ( siy_scaler[ 5] )
    );
    
    // -------------------------------
    // [57] <MSB> Si-Tracker Y 6 HIt Scaler
    // -------------------------------
    Scaler16 siy_6_scaler_inst (
    .aclk   ( axis_aclk      ),
    .aresetn( axis_aresetn   ),
    .enable ( scl_enable     ),
    .sin    ( sit_hit_y [ 6] ),
    .sout   ( siy_scaler[ 6] )
    );
    
    // -------------------------------
    // [58] <MSB> Si-Tracker Y 7 HIt Scaler
    // -------------------------------
    Scaler16 siy_7_scaler_inst (
    .aclk   ( axis_aclk      ),
    .aresetn( axis_aresetn   ),
    .enable ( scl_enable     ),
    .sin    ( sit_hit_y [ 7] ),
    .sout   ( siy_scaler[ 7] )
    );
    
    // -------------------------------
    // [59] <MSB> Si-Tracker Y 8 HIt Scaler
    // -------------------------------
    Scaler16 siy_8_scaler_inst (
    .aclk   ( axis_aclk      ),
    .aresetn( axis_aresetn   ),
    .enable ( scl_enable     ),
    .sin    ( sit_hit_y [ 8] ),
    .sout   ( siy_scaler[ 8] )
    );
    
    // -------------------------------
    // [60] <MSB> Si-Tracker Y 9 HIt Scaler
    // -------------------------------
    Scaler16 siy_9_scaler_inst (
    .aclk   ( axis_aclk      ),
    .aresetn( axis_aresetn   ),
    .enable ( scl_enable     ),
    .sin    ( sit_hit_y [ 9] ),
    .sout   ( siy_scaler[ 9] )
    );
    
    // -------------------------------
    // [61] <MSB> Si-Tracker Y A HIt Scaler
    // -------------------------------
    Scaler16 siy_a_scaler_inst (
    .aclk   ( axis_aclk      ),
    .aresetn( axis_aresetn   ),
    .enable ( scl_enable     ),
    .sin    ( sit_hit_y [10] ),
    .sout   ( siy_scaler[10] )
    );
    
    // -------------------------------
    // [62] <MSB> Si-Tracker Y B HIt Scaler
    // -------------------------------
    Scaler16 siy_b_scaler_inst (
    .aclk   ( axis_aclk      ),
    .aresetn( axis_aresetn   ),
    .enable ( scl_enable     ),
    .sin    ( sit_hit_y [11] ),
    .sout   ( siy_scaler[11] )
    );


    // ----------------------------------
    // Live Time Counters
    // ----------------------------------    
    always @( posedge axis_aclk )
    begin
      rTIM_SECOND  <= 32'h00000000;           // [xx]
      rTIM_MICROS  <= 32'h00000000;           // [xx]
      if ( axis_aresetn == 1'b0 ) begin
        rRAW_GPS     <= 32'h00000000;         // [ 2]
        rRAW_CLK     <= 32'h00000000;         // [ 3]
        rRAW_CLK_ENA <= 32'h00000000;         // [ 4]
        rRAW_CLK_LIV <= 32'h00000000;         // [ 5]
        rRAW_CLK_ACD <= 32'h00000000;         // [28]
        rRAW_CLK_CZT <= 32'h00000000;         // [29]
        rRAW_CLK_CS1 <= 32'h00000000;         // [30]
        rRAW_CLK_CS2 <= 32'h00000000;         // [31]
        rRAW_CLK_SI0 <= 32'h00000000;         // [32]
        rRAW_CLK_SI1 <= 32'h00000000;         // [33]
        rRAW_CLK_SI2 <= 32'h00000000;         // [34]
        rRAW_CLK_SI3 <= 32'h00000000;         // [35]
        rRAW_CLK_SI4 <= 32'h00000000;         // [36]
        rRAW_CLK_SI5 <= 32'h00000000;         // [37]
        rRAW_CLK_SI6 <= 32'h00000000;         // [38]
        rRAW_CLK_SI7 <= 32'h00000000;         // [39]
        rRAW_CLK_SI8 <= 32'h00000000;         // [40]
        rRAW_CLK_SI9 <= 32'h00000000;         // [41]
        rRAW_CLK_SIA <= 32'h00000000;         // [42]
        rRAW_CLK_SIB <= 32'h00000000;         // [43]
      end
      else begin
        // --------------------------------
        // [2] Raw Clock Counter @ GPS PPS
        // --------------------------------
        if ( gps_pps == 1'b1 ) begin
          rRAW_GPS <= rRAW_CLK;
        end
        else begin
          rRAW_GPS <= rRAW_GPS;
        end
        
        // ----------------------
        // [3] Raw Clock Counter
        // ----------------------
        rRAW_CLK <= rRAW_CLK + 1'b1;

        if ( trg_enable == 1'b1 ) begin
        // -----------------------------
        // [4] Raw Clock Enable Counter
        // -----------------------------
          rRAW_CLK_ENA <= rRAW_CLK_ENA + 1'b1;
          
        // ----------------------------
        // [5] Raw Clock Live Counter
        // ----------------------------
          if ( trg_lock == 1'b0 ) begin
            rRAW_CLK_LIV <= rRAW_CLK_LIV + 1'b1;
          end
          else begin
            rRAW_CLK_LIV <= rRAW_CLK_LIV;
          end
          
        // ------------------------------------
        // [28] Raw Clock Live Counter for ACD
        // ------------------------------------
          if ( det_busy[0] == 1'b0 ) begin
            rRAW_CLK_ACD <= rRAW_CLK_ACD + 1'b1;
          end
          else begin
            rRAW_CLK_ACD <= rRAW_CLK_ACD;
          end
          
        // ------------------------------------
        // [29] Raw Clock Live Counter for CZT
        // ------------------------------------
          if ( det_busy[1] == 1'b0 ) begin
            rRAW_CLK_CZT <= rRAW_CLK_CZT + 1'b1;
          end
          else begin
            rRAW_CLK_CZT <= rRAW_CLK_CZT;
          end
          
        // ------------------------------------------
        // [30] Raw Clock Live Counter for CsI (1st)
        // ------------------------------------------
          if ( det_busy[2] == 1'b0 ) begin
            rRAW_CLK_CS1 <= rRAW_CLK_CS1 + 1'b1;
          end
          else begin
            rRAW_CLK_CS1 <= rRAW_CLK_CS1;
          end
          
        // ------------------------------------------
        // [31] Raw Clock Live Counter for CsI (2nd)
        // ------------------------------------------
          if ( det_busy[3] == 1'b0 ) begin
            rRAW_CLK_CS2 <= rRAW_CLK_CS2 + 1'b1;
          end
          else begin
            rRAW_CLK_CS2 <= rRAW_CLK_CS2;
          end
          
        // -------------------------------------
        // [32] Raw Clock Live Counter for Si-0
        // -------------------------------------
          if ( det_busy[4] == 1'b0 ) begin
            rRAW_CLK_SI0 <= rRAW_CLK_SI0 + 1'b1;
          end
          else begin
            rRAW_CLK_SI0 <= rRAW_CLK_SI0;
          end
          
        // -------------------------------------
        // [33] Raw Clock Live Counter for Si-1
        // -------------------------------------
          if ( det_busy[5] == 1'b0 ) begin
            rRAW_CLK_SI1 <= rRAW_CLK_SI1 + 1'b1;
          end
          else begin
            rRAW_CLK_SI1 <= rRAW_CLK_SI1;
          end
          
        // -------------------------------------
        // [34] Raw Clock Live Counter for Si-2
        // -------------------------------------
          if ( det_busy[6] == 1'b0 ) begin
            rRAW_CLK_SI2 <= rRAW_CLK_SI2 + 1'b1;
          end
          else begin
            rRAW_CLK_SI2 <= rRAW_CLK_SI2;
          end
          
        // -------------------------------------
        // [35] Raw Clock Live Counter for Si-3
        // -------------------------------------
          if ( det_busy[7] == 1'b0 ) begin
            rRAW_CLK_SI3 <= rRAW_CLK_SI3 + 1'b1;
          end
          else begin
            rRAW_CLK_SI3 <= rRAW_CLK_SI3;
          end            

        // -------------------------------------
        // [36] Raw Clock Live Counter for Si-4
        // -------------------------------------
          if ( det_busy[8] == 1'b0 ) begin
            rRAW_CLK_SI4 <= rRAW_CLK_SI4 + 1'b1;
          end
          else begin
            rRAW_CLK_SI4 <= rRAW_CLK_SI4;
          end
          
        // -------------------------------------
        // [37] Raw Clock Live Counter for Si-5
        // -------------------------------------
          if ( det_busy[9] == 1'b0 ) begin
            rRAW_CLK_SI5 <= rRAW_CLK_SI5 + 1'b1;
          end
          else begin
            rRAW_CLK_SI5 <= rRAW_CLK_SI5;
          end

        // -------------------------------------
        // [38] Raw Clock Live Counter for Si-6
        // -------------------------------------
          if ( det_busy[10] == 1'b0 ) begin
            rRAW_CLK_SI6 <= rRAW_CLK_SI6 + 1'b1;
          end
          else begin
            rRAW_CLK_SI6 <= rRAW_CLK_SI6;
          end

        // -------------------------------------
        // [39] Raw Clock Live Counter for Si-7
        // -------------------------------------
          if ( det_busy[11] == 1'b0 ) begin
            rRAW_CLK_SI7 <= rRAW_CLK_SI7 + 1'b1;
          end
          else begin
            rRAW_CLK_SI7 <= rRAW_CLK_SI7;
          end
          
        // -------------------------------------
        // [40] Raw Clock Live Counter for Si-8
        // -------------------------------------
          if ( det_busy[12] == 1'b0 ) begin
            rRAW_CLK_SI8 <= rRAW_CLK_SI8 + 1'b1;
          end
          else begin
            rRAW_CLK_SI8 <= rRAW_CLK_SI8;
          end

        // -------------------------------------
        // [41] Raw Clock Live Counter for Si-9
        // -------------------------------------
          if ( det_busy[13] == 1'b0 ) begin
            rRAW_CLK_SI9 <= rRAW_CLK_SI9 + 1'b1;
          end
          else begin
            rRAW_CLK_SI9 <= rRAW_CLK_SI9;
          end
          
        // -------------------------------------
        // [42] Raw Clock Live Counter for Si-A
        // -------------------------------------
          if ( det_busy[14] == 1'b0 ) begin
            rRAW_CLK_SIA <= rRAW_CLK_SIA + 1'b1;
          end
          else begin
            rRAW_CLK_SIB <= rRAW_CLK_SIB;
          end
          
        // --------------------------------------
        // [43] Raw Clock Live Counter for rSi-B
        // --------------------------------------
          if ( det_busy[15] == 1'b0 ) begin
            rRAW_CLK_SIB <= rRAW_CLK_SIB + 1'b1;
          end
          else begin
            rRAW_CLK_SIB <= rRAW_CLK_SIB;
          end
          
        end
        else begin
          rRAW_CLK_ENA <= rRAW_CLK_ENA;             // [ 4]
          rRAW_CLK_LIV <= rRAW_CLK_LIV;             // [ 5]
        end
      end
    end

    always @( posedge axis_aclk )
    begin
      if ( axis_aresetn == 1'b0 ) begin
        data[0] <= 32'haaaaaaaa;
        for ( i=1; i<63; i=i+1) begin
          data[i] <= 32'h00000000;
        end
        data[63] <= 32'hffffffff;
      end
      else begin
        if ( rTRG_ACK == 1'b1 ) begin
          data[ 0] <= data[0];
          data[ 1] <= lck_scaler;               // [ 1]
          data[ 2] <= rRAW_GPS;                 // [ 2]
          data[ 3] <= rRAW_CLK;                 // [ 3]
          data[ 4] <= rRAW_CLK_ENA;             // [ 4]
          data[ 5] <= rRAW_CLK_LIV;             // [ 5]
          data[ 6] <= gps_scaler;               // [ 6]
          data[ 7] <= coi_pat_delay;            // [ 7]
          data[ 8] <= hit_pat_delay;            // [ 8]
          // data[ 9] <= rTIM_SECOND;              // [xx]
          // data[10] <= rTIM_MICROS;              // [xx]
          data[ 9] <= ack_scaler;               // [ 9]
          data[10][15: 0] <= swt_scaler;        // [10] LSB
          data[10][31:16] <= ext_scaler;        // [10] MSB
          data[11][15: 0] <= lpc_scaler;        // [11] LSB
          data[11][31:16] <= hpc_scaler;        // [11] MSB
          data[12][15: 0] <= cm_scaler[ 0];     // [12] LSB
          data[12][31:16] <= cd_scaler[ 0];     // [12] MSB
          data[13][15: 0] <= cm_scaler[ 1];     // [13] LSB
          data[13][31:16] <= cd_scaler[ 1];     // [13] MSB
          data[14][15: 0] <= cm_scaler[ 2];     // [14] LSB
          data[14][31:16] <= cd_scaler[ 2];     // [14] MSB
          data[15][15: 0] <= cm_scaler[ 3];     // [15] LSB
          data[15][31:16] <= cd_scaler[ 3];     // [15] MSB
          data[16][15: 0] <= cm_scaler[ 4];     // [16] LSB
          data[16][31:16] <= cd_scaler[ 4];     // [16] MSB
          data[17][15: 0] <= cm_scaler[ 5];     // [17] LSB
          data[17][31:16] <= cd_scaler[ 5];     // [17] MSB
          data[18][15: 0] <= cm_scaler[ 6];     // [18] LSB
          data[18][31:16] <= cd_scaler[ 6];     // [18] MSB
          data[19][15: 0] <= cm_scaler[ 7];     // [19] LSB
          data[19][31:16] <= cd_scaler[ 7];     // [19] MSB
          data[20][15: 0] <= cm_scaler[ 8];     // [20] LSB
          data[20][31:16] <= cd_scaler[ 8];     // [20] MSB
          data[21][15: 0] <= cm_scaler[ 9];     // [21] LSB
          data[21][31:16] <= cd_scaler[ 9];     // [21] MSB
          data[22][15: 0] <= cm_scaler[10];     // [22] LSB
          data[22][31:16] <= cd_scaler[10];     // [22] MSB
          data[23][15: 0] <= cm_scaler[11];     // [23] LSB
          data[23][31:16] <= cd_scaler[11];     // [23] MSB
          data[24][15: 0] <= cm_scaler[12];     // [24] LSB
          data[24][31:16] <= cd_scaler[12];     // [24] MSB
          data[25][15: 0] <= cm_scaler[13];     // [25] LSB
          data[25][31:16] <= cd_scaler[13];     // [25] MSB
          data[26][15: 0] <= cm_scaler[14];     // [26] LSB
          data[26][31:16] <= cd_scaler[14];     // [26] MSB
          data[27][15: 0] <= cm_scaler[15];     // [27] LSB
          data[27][31:16] <= cd_scaler[15];     // [27] MSB
          data[28]        <= rRAW_CLK_ACD;      // [28]
          data[29]        <= rRAW_CLK_CZT;      // [29]
          data[30]        <= rRAW_CLK_CS1;      // [30]
          data[31]        <= rRAW_CLK_CS2;      // [31]
          data[32]        <= rRAW_CLK_SI0;      // [32]
          data[33]        <= rRAW_CLK_SI1;      // [33]
          data[34]        <= rRAW_CLK_SI2;      // [34]
          data[35]        <= rRAW_CLK_SI3;      // [35]
          data[36]        <= rRAW_CLK_SI4;      // [36]
          data[37]        <= rRAW_CLK_SI5;      // [37]
          data[38]        <= rRAW_CLK_SI6;      // [38]
          data[39]        <= rRAW_CLK_SI7;      // [39]
          data[40]        <= rRAW_CLK_SI8;      // [40]
          data[41]        <= rRAW_CLK_SI9;      // [41]
          data[42]        <= rRAW_CLK_SIA;      // [42]
          data[43]        <= rRAW_CLK_SIB;      // [43]
          data[44]        <= acd_scaler;        // [44]
          data[45]        <= sit_one_scaler;    // [45]
          data[46]        <= sit_and_scaler;    // [46]
          data[47]        <= sit_two_scaler;    // [47]
          data[48]        <= czt_scaler;        // [48]
          data[49]        <= csi_1st_scaler;    // [49]
          data[50]        <= csi_2nd_scaler;    // [50]
          data[51][15: 0] <= six_scaler[ 0];    // [51] LSB
          data[51][31:16] <= siy_scaler[ 0];    // [51] MSB
          data[52][15: 0] <= six_scaler[ 1];    // [52] LSB
          data[52][31:16] <= siy_scaler[ 1];    // [52] MSB
          data[53][15: 0] <= six_scaler[ 2];    // [53] LSB
          data[53][31:16] <= siy_scaler[ 2];    // [53] MSB
          data[54][15: 0] <= six_scaler[ 3];    // [54] LSB
          data[54][31:16] <= siy_scaler[ 3];    // [54] MSB
          data[55][15: 0] <= six_scaler[ 4];    // [55] LSB
          data[55][31:16] <= siy_scaler[ 4];    // [55] MSB
          data[56][15: 0] <= six_scaler[ 5];    // [56] LSB
          data[56][31:16] <= siy_scaler[ 5];    // [56] MSB
          data[57][15: 0] <= six_scaler[ 6];    // [57] LSB
          data[57][31:16] <= siy_scaler[ 6];    // [57] MSB
          data[58][15: 0] <= six_scaler[ 7];    // [58] LSB
          data[58][31:16] <= siy_scaler[ 7];    // [58] MSB
          data[59][15: 0] <= six_scaler[ 8];    // [59] LSB
          data[59][31:16] <= siy_scaler[ 8];    // [59] MSB
          data[60][15: 0] <= six_scaler[ 9];    // [60] LSB
          data[60][31:16] <= siy_scaler[ 9];    // [60] MSB
          data[61][15: 0] <= six_scaler[10];    // [61] LSB
          data[61][31:16] <= siy_scaler[10];    // [61] MSB
          data[62][15: 0] <= six_scaler[11];    // [62] LSB
          data[62][31:16] <= siy_scaler[11];    // [62] MSB
          data[63]        <= data[63];
        end
        else begin
          for ( i=0; i<=63; i=i+1 ) begin
            data[i] <= data[i];
          end          
        end
      end
    end
    
endmodule
