`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/06/2019 12:03:14 PM
// Design Name: 
// Module Name: dummy_fee
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module dummy_fee(
    input  wire aclk,
    input  wire aresetn,
    input  wire trg_ack,
    input  wire trg_lock,
    output wire fee_hit,
    output wire fee_ready,
    output wire fee_busy,
    output wire fee_spare
    );
    
    reg rFEE_HIT;
    reg rFEE_READY;
    reg rFEE_BUSY;
    reg rFEE_SPARE;
    
    reg [ 3:0] rTRG_WIDTH;
    reg [15:0] rBUSY_WIDTH;
    
    assign fee_hit   = rFEE_HIT;
    assign fee_ready = rFEE_READY;
    assign fee_busy  = rFEE_BUSY;
    assign fee_spare = rFEE_SPARE;
    
    localparam IDLE=5'b00001, LOCK=5'b00010, TRIG=5'b00100, BUSY=5'b01000, REDY=5'b10000;
    reg [4:0] state;
    
    always @( posedge aclk )
    begin
      if ( aresetn == 1'b0 ) begin
        state <= IDLE;
      end
      else begin
        case (state)
        IDLE: if ( trg_ack == 1'b1 ) begin
                state <= LOCK;
              end
              else begin
                state <= IDLE;
              end
        LOCK: if ( trg_lock == 1'b1 ) begin
                state <= LOCK;
              end
              else begin
                state <= TRIG;
              end
        TRIG: if ( rTRG_WIDTH == 4'b0111 ) begin
                state <= BUSY;
              end
              else begin
                state <= TRIG;
              end
        BUSY: if ( rBUSY_WIDTH == 16'hFFFF ) begin
                state <= REDY;
              end
              else begin
                state <= BUSY;
              end
        REDY: begin
                state <= IDLE;
              end 
        default: begin
                   state <= IDLE;
                 end      
        endcase
      end
    end
    
    always @( posedge aclk )
    begin
      if ( aresetn == 1'b0 ) begin
        rFEE_HIT    <= 1'b0;
        rFEE_READY  <= 1'b0;
        rFEE_BUSY   <= 1'b0;
        rFEE_SPARE  <= 1'b0;
        rTRG_WIDTH  <= 4'b0000;
        rBUSY_WIDTH <= 16'h0000;
      end
      else begin
        case (state)
        IDLE: begin
                rFEE_HIT    <= 1'b0;
                rFEE_READY  <= 1'b0;
                rFEE_BUSY   <= 1'b0;
                rFEE_SPARE  <= 1'b0;
                rTRG_WIDTH  <= 4'b0000;
                rBUSY_WIDTH <= 16'h0000;
              end
        LOCK: begin
                rFEE_HIT    <= 1'b0;
                if ( rBUSY_WIDTH == 16'h00FF ) begin
                  rFEE_READY <= 1'b1;
                end
                else begin
                  rFEE_READY <= 1'b0;
                end
                rFEE_BUSY   <= 1'b0;
                rFEE_SPARE  <= 1'b0;
                rTRG_WIDTH  <= 4'b0000;
                rBUSY_WIDTH <= rBUSY_WIDTH + 1'b1;
              end
        TRIG: begin
                rFEE_HIT    <= 1'b1;
                rFEE_READY  <= 1'b0;
                rFEE_BUSY   <= 1'b0;
                rFEE_SPARE  <= 1'b1;
                rTRG_WIDTH  <= rTRG_WIDTH + 1'b1;
                rBUSY_WIDTH <= 16'h0000;
              end
        BUSY: begin
                rFEE_HIT    <= 1'b0;
                rFEE_READY  <= 1'b0;
                rFEE_BUSY   <= 1'b1;
                rFEE_SPARE  <= 1'b0;
                rTRG_WIDTH  <= 4'b0000;
                rBUSY_WIDTH <= rBUSY_WIDTH + 1'b1;
              end
        REDY: begin
                rFEE_HIT    <= 1'b0;
                rFEE_READY  <= 1'b1;
                rFEE_BUSY   <= 1'b0;
                rTRG_WIDTH  <= 4'b0000;
                rBUSY_WIDTH <= 16'h0000;
              end      
        default: begin
                   rFEE_HIT    <= 1'b0;
                   rFEE_READY  <= 1'b0;
                   rFEE_BUSY   <= 1'b0;
                   rFEE_SPARE  <= 1'b0;
                   rTRG_WIDTH  <= 4'b0000;
                   rBUSY_WIDTH <= 16'h0000;
                 end
        endcase
      end
    end
    
endmodule
