`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/22/2021 10:48:34 AM
// Design Name: 
// Module Name: FallingEdge
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module FallingEdge(
    input axis_aresetn,
    input axis_aclk,
    input signal_in,
    output detected
    );
    
    reg [2:0] rFED;
    assign detected = rFED[2];
    
    always @( posedge axis_aclk )
    begin
        if ( axis_aresetn == 1'b0 ) begin
          rFED[0] <= 1'b0;
          rFED[1] <= 1'b0;
          rFED[2] <= 1'b0;
        end
        else begin
            if ( signal_in == 1'b1 ) begin
                rFED[0] <= 1'b1;
            end
            else begin
                rFED[0] <= 1'b0;
            end
            rFED[1] <= rFED[0];
            if ( rFED[0] == 1'b0 && rFED[1] == 1'b1 ) begin
                rFED[2] <= 1'b1;
            end
            else begin
                rFED[2] <= 1'b0;
            end
        end
    end
    
endmodule
