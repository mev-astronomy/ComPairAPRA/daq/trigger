`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/28/2019 01:58:41 PM
// Design Name: 
// Module Name: mode_ack_gen
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module mode_ack_gen(
    input  wire aclk,
    input  wire aresetn,
    input  wire enable,
    input  wire strb,
    input  wire coin,
    input  wire [7:0] coin_cd,
    output wire mode_hit,
    output wire mode_ack
    );
    
    reg rSTRB_FLG;
    reg rMODE_HIT;
    reg rMODE_ACK;
    reg [7:0] rMODE_CNT;

    assign mode_hit = rMODE_HIT;
    assign mode_ack = rMODE_ACK;
            
    always @( posedge aclk )
    begin
      if ( aresetn == 1'b0 ) begin
        rSTRB_FLG <= 1'b0;
        rMODE_HIT <= 1'b0;
        rMODE_ACK <= 1'b0;
        rMODE_CNT <= 8'h00;
      end
      else begin
        if ( enable == 1'b1 ) begin
          if ( strb == 1'b1 ) begin
            if ( rSTRB_FLG == 1'b0 ) begin
              rSTRB_FLG <= 1'b1;
              if ( coin == 1'b1 ) begin
                rMODE_HIT <= 1'b1;
                if ( rMODE_CNT == coin_cd - 1'b1 ) begin
                  rMODE_ACK <= 1'b1;
                  rMODE_CNT <= 8'h00;
                end
                else begin
                  rMODE_ACK <= 1'b0;
                  rMODE_CNT <= rMODE_CNT + 1'b1;
                end
              end
              else begin
                rMODE_HIT <= 1'b0;
                rMODE_ACK <= 1'b0;
                rMODE_CNT <= rMODE_CNT;
              end
            end
            else begin
              rSTRB_FLG <= 1'b1;
              rMODE_HIT <= 1'b0;
              rMODE_ACK <= 1'b0;
              rMODE_CNT <= rMODE_CNT;
            end
          end
          else begin
            rSTRB_FLG <= 1'b0;
            rMODE_HIT <= 1'b0;
            rMODE_ACK <= 1'b0;
            rMODE_CNT <= rMODE_CNT;
          end
        end
        else begin
          rSTRB_FLG <= 1'b0;
          rMODE_HIT <= 1'b0;
          rMODE_ACK <= 1'b0;
          rMODE_CNT <= rMODE_CNT;
        end
      end
    end

endmodule
