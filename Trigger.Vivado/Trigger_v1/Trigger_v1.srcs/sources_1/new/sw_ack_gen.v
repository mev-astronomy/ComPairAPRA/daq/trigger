`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/20/2019 11:35:03 AM
// Design Name: 
// Module Name: sw_ack_gen
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module sw_ack_gen(
    input  wire axis_aresetn,
    input  wire axis_aclk,
    input  wire sw_reset,
    input  wire sw_trigger,
    output wire sw_resetn,
    output wire sw_ack
    );

    reg rSW_RESETn;    
    reg rSW_ACK;
    reg rSW_FLAG;    

    assign sw_resetn = rSW_RESETn;
    assign sw_ack    = rSW_ACK;
    
    always @( posedge axis_aclk )
    begin
      if ( axis_aresetn == 1'b0 )
        begin
          rSW_RESETn <= 1'b1;
          rSW_ACK    <= 1'b0;
          rSW_FLAG   <= 1'b0;
        end
      else
        begin
          if ( sw_reset == 1'b1 )
            begin
              rSW_RESETn <= 1'b0;
            end
          else
            begin
              rSW_RESETn <= 1'b1;
            end
          if ( sw_trigger == 1'b1 )
            begin
              if ( rSW_FLAG == 1'b0 )
                begin
                  rSW_ACK  <= 1'b1;
                  rSW_FLAG <= 1'b1;
                end
              else
                begin
                  rSW_ACK  <= 1'b0;
                  rSW_FLAG <= 1'b1;
                end
            end
          else
            begin
              rSW_ACK  <= 1'b0;
              rSW_FLAG <= 1'b0;
            end
        end
    end
    
endmodule
