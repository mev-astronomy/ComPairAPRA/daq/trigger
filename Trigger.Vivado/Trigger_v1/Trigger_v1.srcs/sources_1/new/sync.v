`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/22/2021 09:57:43 AM
// Design Name: 
// Module Name: sync
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module sync(
    input axis_aresetn,
    input axis_aclk,
    input unsynced_in,
    output synced_out
    );
    
    reg rSYNCED_OUT;
    assign synced_out = rSYNCED_OUT;
    
    always @( posedge axis_aclk )
    begin
        if ( axis_aresetn == 1'b0 ) begin
            rSYNCED_OUT <= 1'b0;
        end
        else begin
            if ( unsynced_in == 1'b1 ) begin
                rSYNCED_OUT <= 1'b1;
            end
            else begin
                rSYNCED_OUT <= 1'b0;
            end
        end
    end
endmodule
