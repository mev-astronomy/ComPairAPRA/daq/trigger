`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/20/2019 03:52:37 PM
// Design Name: 
// Module Name: register_pkt_length
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module register_pkt_length(
    input  wire [31:0] register_f,
    output wire [ 9:0] pkt_length
    );
    assign pkt_length = register_f[9:0];
endmodule
