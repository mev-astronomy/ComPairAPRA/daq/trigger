`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/26/2019 03:26:00 PM
// Design Name: 
// Module Name: gps_pps_mimic
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module gps_pps_mimic(
    input  wire axis_aresetn,
    input  wire axis_aclk,
    output wire gps_pps
    );
    
    reg rGPS_PPS;
    reg [31:0] rGPS_COUNT;
    
    assign gps_pps = rGPS_PPS;

    always @( posedge axis_aclk )
    begin
      if ( axis_aresetn == 1'b0 )
        begin
          rGPS_PPS <= 1'b0;
          rGPS_COUNT <= 32'h00000000;
        end
      else
        begin
          if ( rGPS_COUNT == 49999999 )
            begin
              rGPS_PPS <= 1'b1;
              rGPS_COUNT <= 32'h00000000;
            end
          else
            begin
              rGPS_PPS <= 1'b0;
              rGPS_COUNT <= rGPS_COUNT + 1'b1;
            end
        end
    end
    
endmodule
