`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/06/2019 11:22:14 AM
// Design Name: 
// Module Name: Delay32
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Delay32(
    input  wire aclk,
    input  wire aresetn,
    input  wire [31:0] sin,
    output wire [31:0] sout
    );
    
    reg [31:0] rTEMP1;
    reg [31:0] rTEMP2;
    
    assign sout = rTEMP2;

    always @( posedge aclk )
    begin
      if ( aresetn == 1'b0 ) begin
        rTEMP1 <= 32'h00000000;
        rTEMP2 <= 32'h00000000;
      end
      else begin
        rTEMP1 <= sin;
        rTEMP2 <= rTEMP1;
      end
    end
    
endmodule
