`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/20/2019 12:34:29 PM
// Design Name: 
// Module Name: register_ctrl
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module register_ctrl(
    input  wire axis_aresetn,
    input  wire axis_aclk,
    input  wire sw_resetn,
    input  wire [31:0] register_1,
    output wire aux_resetn,
    output wire trg_resetn,
    output wire trg_enable,
    output wire csi_init10
    );
    
    reg rTRG_ENABLE;
    reg rCSI_INIT10;
    reg [ 3:0] rCSI_COUNT;
    reg [ 3:0] rCSI_HIGH;
    reg [15:0] rCSI_LOW;
    
    assign aux_resetn = !register_1[31];
    assign trg_resetn = !register_1[30] && sw_resetn;
//    assign trg_enable =  register_1[ 0];
    assign trg_enable = rTRG_ENABLE;
    assign csi_init10 = rCSI_INIT10;
    
    localparam IDLE=5'b00001, INIT=5'b00010, TINI=5'b00100, CONT=5'b01000, ENBL=5'b10000;
    reg [4:0] state;
    
    always @ ( posedge axis_aclk ) 
    begin
      if ( trg_resetn == 1'b0 ) begin
        state <= IDLE;
      end
      else begin
        case (state)
        IDLE: if ( register_1[0] == 1'b1 ) begin
                state <= INIT;
              end
              else begin
                state <= IDLE;
              end
        INIT: if ( rCSI_HIGH == 4'b0100 ) begin
                state <= TINI;
              end
              else begin
                state <= INIT;
              end
        TINI: if ( rCSI_LOW == 16'h1381 ) begin
                state <= CONT;
              end
              else begin
                state <= TINI;
              end
        CONT: if ( rCSI_COUNT == 4'b1001 ) begin
                state <= ENBL;
              end
              else begin
                state <= INIT;
              end
        ENBL: if ( register_1[0] == 1'b0 ) begin
                state <= IDLE;
              end
              else begin
                state <= ENBL;
              end
        default: begin
                   state <= IDLE;
                 end
        endcase
      end
    end
      
    always @( posedge axis_aclk )
    begin
      if ( trg_resetn == 1'b0 ) begin
        rTRG_ENABLE <=  1'b0;
        rCSI_INIT10 <=  1'b0;
        rCSI_COUNT  <=  4'b0000;
        rCSI_HIGH   <=  4'b0000;
        rCSI_LOW    <= 16'h0000;
      end
      else begin
        case (state)
        IDLE: begin
                rTRG_ENABLE <=  1'b0;
                rCSI_INIT10 <=  1'b0;
                rCSI_COUNT  <=  4'b0000;
                rCSI_HIGH   <=  4'b0000;
                rCSI_LOW    <= 16'h0000;
              end
        INIT: begin
                rTRG_ENABLE <=  1'b0;
                rCSI_INIT10 <=  1'b1;
                rCSI_COUNT  <= rCSI_COUNT;
                rCSI_HIGH   <= rCSI_HIGH + 1'b1;
                rCSI_LOW    <= 16'h0000;
              end
        TINI: begin
                rTRG_ENABLE <=  1'b0;
                rCSI_INIT10 <=  1'b0;
                rCSI_COUNT  <= rCSI_COUNT;
                rCSI_HIGH   <=  4'b0000;
                rCSI_LOW    <= rCSI_LOW + 1'b1;
              end
        CONT: begin
                rTRG_ENABLE <=  1'b0;
                rCSI_INIT10 <=  1'b0;
                rCSI_COUNT  <= rCSI_COUNT + 1'b1;
                rCSI_HIGH   <=  4'b0000;
                rCSI_LOW    <= 16'h0000;
              end
        ENBL: begin
                rTRG_ENABLE <=  1'b1;
                rCSI_INIT10 <=  1'b0;
                rCSI_COUNT  <=  4'b0000;
                rCSI_HIGH   <=  4'b0000;
                rCSI_LOW    <= 16'h0000;
              end
        default: begin
                   rTRG_ENABLE <=  1'b0;
                   rCSI_INIT10 <=  1'b0;
                   rCSI_COUNT  <=  4'b0000;
                   rCSI_HIGH   <=  4'b0000;
                   rCSI_LOW    <= 16'h0000;
                 end
        endcase
      end
    end
    
endmodule
