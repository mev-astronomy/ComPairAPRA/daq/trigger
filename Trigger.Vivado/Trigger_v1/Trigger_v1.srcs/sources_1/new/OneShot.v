`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/27/2019 02:40:55 PM
// Design Name: 
// Module Name: OneShot
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module OneShot(
    input  wire aclk,
    input  wire aresetn,
    input  wire sin,
    input  wire [7:0] width,
    output wire sout
    );
    
    reg rPULSE;
    reg rFLAG;
    reg [7:0] rWIDTH;
    reg rSYNC_IN;
    
    assign sout = rPULSE;
    
    always @( posedge aclk )
    begin
      if ( aresetn == 1'b0 ) begin
        rSYNC_IN <= 1'b0;
      end
      else begin
        if ( sin == 1'b1 ) begin
          rSYNC_IN <= 1'b1;
        end
        else begin
          rSYNC_IN <= 1'b0;
        end
      end
    end 
       
    always @( posedge aclk )
    begin
      if ( aresetn == 1'b0 ) begin
        rPULSE <= 1'b0;
        rFLAG  <= 1'b0;
        rWIDTH <= 8'h00;
      end
      else begin
        if ( rSYNC_IN == 1'b1 ) begin
          if ( rFLAG == 1'b0 ) begin
            rPULSE <= 1'b1;
            rFLAG  <= 1'b1;
            rWIDTH <= width;
          end
          else begin
            rFLAG <= 1'b1;
            if ( rWIDTH > 0 ) begin
              rPULSE <= 1'b1;
              rWIDTH <= rWIDTH - 1'b1;
            end
            else begin
              rPULSE <= 1'b0;
              rWIDTH <= rWIDTH;
            end
          end
        end
        else begin
          if ( rWIDTH > 0 ) begin
            rPULSE <= 1'b1;
            rFLAG  <= 1'b1;
            rWIDTH <= rWIDTH - 1'b1;
          end
          else begin
            rPULSE <= 1'b0;
            rFLAG  <= 1'b0;
            rWIDTH <= rWIDTH;
          end
        end
      end
    end
    
endmodule
