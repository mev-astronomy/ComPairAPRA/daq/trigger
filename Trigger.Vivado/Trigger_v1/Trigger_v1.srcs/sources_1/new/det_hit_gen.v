`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/20/2019 01:52:07 PM
// Design Name: 
// Module Name: det_hit_gen
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

// -- 2019Jul26
//      Si-Tracker: lpc: lpc[3:0], mpc: hpc[3:0], hpc: hpc[7:4] 
//      ACD: lpc[4]
//      CZT: lpc[5]
//      CsI 1st: lpc[6]
//      CsI 2nd: lpc[7] --2019Aug27--
// 2019Jul26 --

module det_hit_gen(
    input  wire axis_aresetn,
    input  wire axis_aclk,
    input  wire trg_resetn,
    input  wire trg_ack,
    input  wire [ 7:0] lpc_fee_hit,
    input  wire [ 7:0] lpc_fee_ready,
    input  wire [ 7:0] lpc_fee_busy,
    input  wire [ 7:0] lpc_fee_spare,
    input  wire [ 7:0] hpc_fee_hit,
    input  wire [ 7:0] hpc_fee_ready,
    input  wire [ 7:0] hpc_fee_busy,
    input  wire [ 7:0] hpc_fee_spare,
    input  wire [ 7:0] lpc_fee_hit_ie,
    input  wire [ 7:0] lpc_fee_ready_ie,
    input  wire [ 7:0] lpc_fee_busy_ie,
    input  wire [ 7:0] lpc_fee_spare_ie,
    input  wire [ 7:0] hpc_fee_hit_ie,
    input  wire [ 7:0] hpc_fee_ready_ie,
    input  wire [ 7:0] hpc_fee_busy_ie,
    input  wire [ 7:0] hpc_fee_spare_ie,
    input  wire [31:0] register_1,
    input  wire [31:0] register_7,
    output wire sit_hit_or,
    output wire [11:0] sit_hit_x,
    output wire [11:0] sit_hit_y,
    output wire csi_hit_1st,
    output wire csi_hit_2nd,
    output wire acd_hit_coin,
    output wire sit_one_coin,
    output wire sit_and_coin,
    output wire sit_two_coin,
    output wire czt_hit_coin,
    output wire csi_hit_coin,
    output wire acd_hit_lock,
    output wire sit_hit_lock,
    output wire czt_hit_lock,
    output wire csi_hit_lock,
    output wire [15:0] det_busy,
    output wire gps_pps
    );
    
    reg rACD_HIT_LOCK;
    reg rSIT_HIT_LOCK;
    reg rCZT_HIT_LOCK;
    reg rCSI_HIT_LOCK;
    
    reg rCSI_1ST_LOCK;
    reg rCSI_2ND_LOCK;
    
    reg [3:0] rSIT_LPC_LOCK;
    reg [3:0] rSIT_MPC_LOCK;
    reg [3:0] rSIT_HPC_LOCK;

    assign det_busy[0] = rACD_HIT_LOCK;
    assign det_busy[1] = rCZT_HIT_LOCK;
    assign det_busy[2] = rCSI_1ST_LOCK;
    assign det_busy[3] = rCSI_2ND_LOCK;
    assign det_busy[ 7: 4] = rSIT_LPC_LOCK;
    assign det_busy[11: 8] = rSIT_MPC_LOCK;
    assign det_busy[15:12] = rSIT_HPC_LOCK;

    reg rGPS_PPS;
    reg rGPS_PPS_FLAG;
    
    wire [3:0] sit_hit_x_lpc;
    wire [3:0] sit_hit_x_mpc;
    wire [3:0] sit_hit_x_hpc;
    
    wire [3:0] sit_hit_y_lpc;
    wire [3:0] sit_hit_y_mpc;
    wire [3:0] sit_hit_y_hpc;
    
    wire [2:0] sit_or_sel;
    wire gps_pps_sel;
    
    assign sit_or_sel = register_1[7:5];
    assign gps_pps_sel = register_1[24];
                       
    wire [7:0] acd_width;
    wire [7:0] sit_width;
    wire [7:0] czt_width;
    wire [7:0] csi_width;
    
    assign acd_width = register_7[ 7: 0];
    assign sit_width = register_7[15: 8];
    assign czt_width = register_7[23:16];
//    assign czt_width = 8'h7c;
    assign csi_width = register_7[31:24];
    
    // ACD: lpc[4]
    wire acd_hit   = lpc_fee_hit  [4] & lpc_fee_hit_ie  [4];
    wire acd_ready = lpc_fee_ready[4] & lpc_fee_ready_ie[4];
    wire acd_busy  = lpc_fee_busy [4] & lpc_fee_busy_ie [4];
    wire acd_ready_ie = lpc_fee_ready_ie[4];
    
    // CZT: lpc[5]
    wire czt_hit   = lpc_fee_hit  [5] & lpc_fee_hit_ie  [5];
    wire czt_ready = lpc_fee_ready[5] & lpc_fee_ready_ie[5];
    wire czt_busy  = lpc_fee_busy [5] & lpc_fee_busy_ie [5];
    wire czt_ready_ie = lpc_fee_ready_ie[5];
    
    // CsI 1st: lpc[6]
    wire cs1_hit   = lpc_fee_hit  [6] & lpc_fee_hit_ie  [6];
    wire cs1_ready = lpc_fee_ready[6] & lpc_fee_ready_ie[6];
    wire cs1_busy  = lpc_fee_busy [6] & lpc_fee_busy_ie [6];
    wire cs1_ready_ie = lpc_fee_ready_ie[6];
    
    // CsI 2nd: lpc[7]
    wire cs2_hit   = lpc_fee_hit  [7] & lpc_fee_hit_ie  [7];
    wire cs2_ready = lpc_fee_ready[7] & lpc_fee_ready_ie[7];
    wire cs2_busy  = lpc_fee_busy [7] & lpc_fee_busy_ie [7];
    wire cs2_ready_ie = lpc_fee_ready_ie[7];

    wire csi_hit  = cs1_hit  | cs2_hit;
    wire csi_busy = cs1_busy | cs2_busy;

    // GPS PPS Input
    wire gps_pps_in = lpc_fee_spare[6] & lpc_fee_spare_ie[6];
                
    // Si-Tracker: lpc: lpc[3:0], mpc: hpc[3:0], hpc: hpc[7:4] 
    wire [3:0] si_x_lpc     = lpc_fee_hit  [3:0] & lpc_fee_hit_ie  [3:0];
    wire [3:0] si_x_mpc     = hpc_fee_hit  [3:0] & hpc_fee_hit_ie  [3:0];
    wire [3:0] si_x_hpc     = hpc_fee_hit  [7:4] & hpc_fee_hit_ie  [7:4];

    wire [3:0] si_ready_lpc = lpc_fee_ready[3:0] & lpc_fee_ready_ie[3:0];
    wire [3:0] si_ready_mpc = hpc_fee_ready[3:0] & hpc_fee_ready_ie[3:0];
    wire [3:0] si_ready_hpc = hpc_fee_ready[7:4] & hpc_fee_ready_ie[7:4];
    
    wire [3:0] si_busy_lpc  = lpc_fee_busy [3:0] & lpc_fee_busy_ie [3:0];
    wire [3:0] si_busy_mpc  = hpc_fee_busy [3:0] & hpc_fee_busy_ie [3:0];
    wire [3:0] si_busy_hpc  = hpc_fee_busy [7:4] & hpc_fee_busy_ie [7:4];
    
    wire [3:0] si_y_lpc     = lpc_fee_spare[3:0] & lpc_fee_spare_ie[3:0];   
    wire [3:0] si_y_mpc     = hpc_fee_spare[3:0] & hpc_fee_spare_ie[3:0];
    wire [3:0] si_y_hpc     = hpc_fee_spare[7:4] & hpc_fee_spare_ie[7:4];

    wire [3:0] si_ready_lpc_ie = lpc_fee_ready_ie[3:0];
    wire [3:0] si_ready_mpc_ie = hpc_fee_ready_ie[3:0];
    wire [3:0] si_ready_hpc_ie = hpc_fee_ready_ie[7:4];
    
    wire sit_busy = si_busy_lpc || si_busy_mpc || si_busy_hpc;
   
    wire [3:0] si_lpc_or = si_x_lpc | si_y_lpc;
    wire [3:0] si_mpc_or = si_x_mpc | si_y_mpc;
    wire [3:0] si_hpc_or = si_x_hpc | si_y_hpc;
    
    wire [3:0] si_lpc_an = si_x_lpc & si_y_lpc;
    wire [3:0] si_mpc_an = si_x_mpc & si_y_mpc;
    wire [3:0] si_hpc_an = si_x_hpc & si_y_hpc;

    // si_hit_one    
    wire si_lpc_one = si_lpc_or[0] | si_lpc_or[1] | si_lpc_or[2] | si_lpc_or[3];
    wire si_mpc_one = si_mpc_or[0] | si_mpc_or[1] | si_mpc_or[2] | si_mpc_or[3];
    wire si_hpc_one = si_hpc_or[0] | si_hpc_or[1] | si_hpc_or[2] | si_hpc_or[3];

    wire si_hit_one = si_lpc_one | si_mpc_one | si_hpc_one;
    
    // si_hit_and
    wire si_lpc_and = si_lpc_an[0] | si_lpc_an[1] | si_lpc_an[2] | si_lpc_an[3];
    wire si_mpc_and = si_mpc_an[0] | si_mpc_an[1] | si_mpc_an[2] | si_mpc_an[3];
    wire si_hpc_and = si_hpc_an[0] | si_hpc_an[1] | si_hpc_an[2] | si_hpc_an[3];

    wire si_hit_and = si_lpc_and | si_mpc_and | si_hpc_and;

    // si_hit_two
    wire si_lpc_two_1 = (si_lpc_or[0] & si_lpc_or[1]) | (si_lpc_or[2] & si_lpc_or[3]);
    wire si_mpc_two_1 = (si_mpc_or[0] & si_mpc_or[1]) | (si_mpc_or[2] & si_mpc_or[3]);
    wire si_hpc_two_1 = (si_hpc_or[0] & si_hpc_or[1]) | (si_hpc_or[2] & si_hpc_or[3]);
    
    wire si_lpc_two_2 = (si_lpc_or[0] | si_lpc_or[1]) & (si_lpc_or[2] | si_lpc_or[3]);
    wire si_mpc_two_2 = (si_mpc_or[0] | si_mpc_or[1]) & (si_mpc_or[2] | si_mpc_or[3]);
    wire si_hpc_two_2 = (si_hpc_or[0] | si_hpc_or[1]) & (si_hpc_or[2] | si_hpc_or[3]);
    
    wire si_hit_two = (si_lpc_one & si_mpc_one) | (si_lpc_one & si_hpc_one) | (si_mpc_one & si_hpc_one)
                    | si_lpc_two_1 | si_mpc_two_1 | si_hpc_two_1
                    | si_lpc_two_2 | si_mpc_two_2 | si_hpc_two_2;

    // assign sit_hit_or = si_lpc_one | si_mpc_one | si_hpc_one;
    assign sit_hit_or = (si_hit_one & sit_or_sel[0]) | (si_hit_and & sit_or_sel[1]) | (si_hit_two & sit_or_sel[2]);

    assign sit_hit_x[ 3:0] = sit_hit_x_lpc[3:0];
    assign sit_hit_x[ 7:4] = sit_hit_x_mpc[3:0];
    assign sit_hit_x[11:8] = sit_hit_x_hpc[3:0];

    assign sit_hit_y[ 3:0] = sit_hit_y_lpc[3:0];
    assign sit_hit_y[ 7:4] = sit_hit_y_mpc[3:0];
    assign sit_hit_y[11:8] = sit_hit_y_hpc[3:0];

    assign acd_hit_lock = rACD_HIT_LOCK;
    assign sit_hit_lock = rSIT_HIT_LOCK;
    assign czt_hit_lock = rCZT_HIT_LOCK;
    assign csi_hit_lock = rCSI_HIT_LOCK;

    wire gps_pps_int;
    assign gps_pps = (rGPS_PPS & !gps_pps_sel) | (gps_pps_int & gps_pps_sel);
 
    always @( posedge axis_aclk )
    begin
      if ( axis_aresetn == 1'b0 ) begin
        rGPS_PPS      <= 1'b0;
        rGPS_PPS_FLAG <= 1'b0;
      end
      else begin
        if ( gps_pps_in == 1'b1 ) begin
          if ( rGPS_PPS_FLAG == 1'b0 ) begin
            rGPS_PPS      <= 1'b1;
            rGPS_PPS_FLAG <= 1'b1;
          end
          else begin
            rGPS_PPS      <= 1'b0;
            rGPS_PPS_FLAG <= 1'b1;
          end
        end
        else begin
          rGPS_PPS      <= 1'b0;
          rGPS_PPS_FLAG <= 1'b0;
        end
      end
    end


    // GPS PPS MIMIC
    gps_pps_mimic gps_pps_clk (
    .axis_aresetn( axis_aresetn ),
    .axis_aclk   ( axis_aclk    ),
    .gps_pps     ( gps_pps_int  )
    );
    
    // SiT X HIT
    OneShot sit_x_lpc_0 (
    .aclk   ( axis_aclk        ),
    .aresetn( axis_aresetn     ),
    .sin    ( si_x_lpc[0]      ),
    .width  ( sit_width        ),
    .sout   ( sit_hit_x_lpc[0] )
    );
    
    OneShot sit_x_lpc_1 (
    .aclk   ( axis_aclk        ),
    .aresetn( axis_aresetn     ),
    .sin    ( si_x_lpc[1]      ),
    .width  ( sit_width        ),
    .sout   ( sit_hit_x_lpc[1] )    
    );

    OneShot sit_x_lpc_2 (
    .aclk   ( axis_aclk        ),
    .aresetn( axis_aresetn     ),
    .sin    ( si_x_lpc[2]      ),
    .width  ( sit_width        ),
    .sout   ( sit_hit_x_lpc[2] )
    );
    
    OneShot sit_x_lpc_3 (
    .aclk   ( axis_aclk        ),
    .aresetn( axis_aresetn     ),
    .sin    ( si_x_lpc[3]      ),
    .width  ( sit_width        ),
    .sout   ( sit_hit_x_lpc[3] )    
    );

    OneShot sit_x_mpc_0 (
    .aclk   ( axis_aclk        ),
    .aresetn( axis_aresetn     ),
    .sin    ( si_x_mpc[0]      ),
    .width  ( sit_width        ),
    .sout   ( sit_hit_x_mpc[0] )
    );
    
    OneShot sit_x_mpc_1 (
    .aclk   ( axis_aclk        ),
    .aresetn( axis_aresetn     ),
    .sin    ( si_x_mpc[1]      ),
    .width  ( sit_width        ),
    .sout   ( sit_hit_x_mpc[1] )    
    );

    OneShot sit_x_mpc_2 (
    .aclk   ( axis_aclk        ),
    .aresetn( axis_aresetn     ),
    .sin    ( si_x_mpc[2]      ),
    .width  ( sit_width        ),
    .sout   ( sit_hit_x_mpc[2] )
    );
    
    OneShot sit_x_mpc_3 (
    .aclk   ( axis_aclk        ),
    .aresetn( axis_aresetn     ),
    .sin    ( si_x_mpc[3]      ),
    .width  ( sit_width        ),
    .sout   ( sit_hit_x_mpc[3] )    
    );

    OneShot sit_x_hpc_0 (
    .aclk   ( axis_aclk        ),
    .aresetn( axis_aresetn     ),
    .sin    ( si_x_hpc[0]      ),
    .width  ( sit_width        ),
    .sout   ( sit_hit_x_hpc[0] )
    );
    
    OneShot sit_x_hpc_1 (
    .aclk   ( axis_aclk        ),
    .aresetn( axis_aresetn     ),
    .sin    ( si_x_hpc[1]      ),
    .width  ( sit_width        ),
    .sout   ( sit_hit_x_hpc[1] )    
    );

    OneShot sit_x_hpc_2 (
    .aclk   ( axis_aclk        ),
    .aresetn( axis_aresetn     ),
    .sin    ( si_x_hpc[2]      ),
    .width  ( sit_width        ),
    .sout   ( sit_hit_x_hpc[2] )
    );
    
    OneShot sit_x_hpc_3 (
    .aclk   ( axis_aclk        ),
    .aresetn( axis_aresetn     ),
    .sin    ( si_x_hpc[3]      ),
    .width  ( sit_width        ),
    .sout   ( sit_hit_x_hpc[3] )    
    );

    // SiT Y HIT
    OneShot sit_y_lpc_0 (
    .aclk   ( axis_aclk        ),
    .aresetn( axis_aresetn     ),
    .sin    ( si_y_lpc[0]      ),
    .width  ( sit_width        ),
    .sout   ( sit_hit_y_lpc[0] )
    );
    
    OneShot sit_y_lpc_1 (
    .aclk   ( axis_aclk        ),
    .aresetn( axis_aresetn     ),
    .sin    ( si_y_lpc[1]      ),
    .width  ( sit_width        ),
    .sout   ( sit_hit_y_lpc[1] )    
    );

    OneShot sit_y_lpc_2 (
    .aclk   ( axis_aclk        ),
    .aresetn( axis_aresetn     ),
    .sin    ( si_y_lpc[2]      ),
    .width  ( sit_width        ),
    .sout   ( sit_hit_y_lpc[2] )
    );
    
    OneShot sit_y_lpc_3 (
    .aclk   ( axis_aclk        ),
    .aresetn( axis_aresetn     ),
    .sin    ( si_y_lpc[3]      ),
    .width  ( sit_width        ),
    .sout   ( sit_hit_y_lpc[3] )    
    );

    OneShot sit_y_mpc_0 (
    .aclk   ( axis_aclk        ),
    .aresetn( axis_aresetn     ),
    .sin    ( si_y_mpc[0]      ),
    .width  ( sit_width        ),
    .sout   ( sit_hit_y_mpc[0] )
    );
    
    OneShot sit_y_mpc_1 (
    .aclk   ( axis_aclk        ),
    .aresetn( axis_aresetn     ),
    .sin    ( si_y_mpc[1]      ),
    .width  ( sit_width        ),
    .sout   ( sit_hit_y_mpc[1] )    
    );

    OneShot sit_y_mpc_2 (
    .aclk   ( axis_aclk        ),
    .aresetn( axis_aresetn     ),
    .sin    ( si_y_mpc[2]      ),
    .width  ( sit_width        ),
    .sout   ( sit_hit_y_mpc[2] )
    );
    
    OneShot sit_y_mpc_3 (
    .aclk   ( axis_aclk        ),
    .aresetn( axis_aresetn     ),
    .sin    ( si_y_mpc[3]      ),
    .width  ( sit_width        ),
    .sout   ( sit_hit_y_mpc[3] )    
    );

    OneShot sit_y_hpc_0 (
    .aclk   ( axis_aclk        ),
    .aresetn( axis_aresetn     ),
    .sin    ( si_y_hpc[0]      ),
    .width  ( sit_width        ),
    .sout   ( sit_hit_y_hpc[0] )
    );
    
    OneShot sit_y_hpc_1 (
    .aclk   ( axis_aclk        ),
    .aresetn( axis_aresetn     ),
    .sin    ( si_y_hpc[1]      ),
    .width  ( sit_width        ),
    .sout   ( sit_hit_y_hpc[1] )    
    );

    OneShot sit_y_hpc_2 (
    .aclk   ( axis_aclk        ),
    .aresetn( axis_aresetn     ),
    .sin    ( si_y_hpc[2]      ),
    .width  ( sit_width        ),
    .sout   ( sit_hit_y_hpc[2] )
    );
    
    OneShot sit_y_hpc_3 (
    .aclk   ( axis_aclk        ),
    .aresetn( axis_aresetn     ),
    .sin    ( si_y_hpc[3]      ),
    .width  ( sit_width        ),
    .sout   ( sit_hit_y_hpc[3] )    
    );

    // CsI 1st HIT
    OneShot csi_1st_hit (
    .aclk   ( axis_aclk        ),
    .aresetn( axis_aresetn     ),
    .sin    ( cs1_hit          ),
    .width  ( csi_width        ),
    .sout   ( csi_hit_1st      )
    );
    
    // CsI 2nd HIT
    OneShot csi_2nd_hit (
    .aclk   ( axis_aclk        ),
    .aresetn( axis_aresetn     ),
    .sin    ( cs2_hit          ),
    .width  ( csi_width        ),
    .sout   ( csi_hit_2nd      )
    );
    
    // ACD HIT COIN
    OneShot acd_pulse (
    .aclk   ( axis_aclk        ),
    .aresetn( axis_aresetn     ),
    .sin    ( acd_hit          ),
    .width  ( acd_width        ),
    .sout   ( acd_hit_coin     )        
    );
           
    // SiT HIT ONE
    OneShot sit_one_pulse (
    .aclk   ( axis_aclk        ),
    .aresetn( axis_aresetn     ),
    .sin    ( si_hit_one       ),
    .width  ( sit_width        ),
    .sout   ( sit_one_coin     )        
    );
   
    // SiT HIT AND
    OneShot sit_and_pulse (
    .aclk   ( axis_aclk        ),
    .aresetn( axis_aresetn     ),
    .sin    ( si_hit_and       ),
    .width  ( sit_width        ),
    .sout   ( sit_and_coin     )        
    );

    // SiT HIT TWO
    OneShot sit_two_pulse (
    .aclk   ( axis_aclk        ),
    .aresetn( axis_aresetn     ),
    .sin    ( si_hit_two       ),
    .width  ( sit_width        ),
    .sout   ( sit_two_coin     )        
    );

    // SiT HIT COIN
    wire sit_hit_coin;
    OneShot sit_hit_pulse (
    .aclk   ( axis_aclk        ),
    .aresetn( axis_aresetn     ),
    .sin    ( sit_hit_or       ),
    .width  ( sit_width        ),
    .sout   ( sit_hit_coin     )
    );
    
    // CZT HIT COIN    
    OneShot czt_pulse (
    .aclk   ( axis_aclk        ),
    .aresetn( axis_aresetn     ),
    .sin    ( czt_hit          ),
    .width  ( czt_width        ),
    .sout   ( czt_hit_coin     )        
    );

    // CsI HIT COIN
    OneShot csi_pulse (
    .aclk   ( axis_aclk        ),
    .aresetn( axis_aresetn     ),
    .sin    ( csi_hit          ),
    .width  ( csi_width        ),
    .sout   ( csi_hit_coin     )        
    );

    // ACD_Falling_EDGE_Detection
    wire acd_fed;
    FallingEdge acd_hit_fed (
    .axis_aresetn ( axis_aresetn ),
    .axis_aclk    ( axis_aclk    ),
    .signal_in    ( acd_hit_coin ),
    .detected     ( acd_fed      )
    );
    
    // ACD HIT LOCK
    always @( posedge axis_aclk )
    begin
      if ( axis_aresetn == 1'b0 || trg_resetn == 1'b0 ) begin
        rACD_HIT_LOCK <= 1'b0;
      end
      else begin
        if ( acd_fed == 1'b1 || trg_ack == 1'b1 ) begin
          rACD_HIT_LOCK <= acd_ready_ie;
        end
        else begin
          rACD_HIT_LOCK <= ( rACD_HIT_LOCK & ~(acd_ready) );
        end
      end
    end

    // SiT_Falling_EDGE_Detection
    wire sit_fed;
    FallingEdge sit_hit_fed (
    .axis_aresetn ( axis_aresetn ),
    .axis_aclk    ( axis_aclk    ),
    .signal_in    ( sit_hit_coin ),
    .detected     ( sit_fed      )
    );
    
    // SiT HIT LOCK
    always @( posedge axis_aclk )
    begin
      if ( axis_aresetn == 1'b0 || trg_resetn == 1'b0 ) begin
        rSIT_HIT_LOCK <= 1'b0;
        rSIT_LPC_LOCK <= 4'b0000;
        rSIT_MPC_LOCK <= 4'b0000;
        rSIT_HPC_LOCK <= 4'b0000;
      end
      else begin
        if ( sit_fed == 1'b1 | trg_ack == 1'b1 ) begin
          rSIT_HIT_LOCK <= 1'b1;
          rSIT_LPC_LOCK <= (4'b1111 & si_ready_lpc_ie);
          rSIT_MPC_LOCK <= (4'b1111 & si_ready_mpc_ie);
          rSIT_HPC_LOCK <= (4'b1111 & si_ready_hpc_ie);
        end
        else begin
          rSIT_HIT_LOCK <= (rSIT_LPC_LOCK || rSIT_MPC_LOCK || rSIT_HPC_LOCK );
          rSIT_LPC_LOCK <= (rSIT_LPC_LOCK & ~(si_ready_lpc));
          rSIT_MPC_LOCK <= (rSIT_MPC_LOCK & ~(si_ready_mpc));
          rSIT_HPC_LOCK <= (rSIT_HPC_LOCK & ~(si_ready_hpc));
        end
      end
    end

    // CZT_Falling_EDGE_Detection
    wire czt_fed;
    FallingEdge czt_hit_fed (
    .axis_aresetn ( axis_aresetn ),
    .axis_aclk    ( axis_aclk    ),
    .signal_in    ( czt_hit_coin ),
    .detected     ( czt_fed      )
    );
    
    // CZT HIT LOCK
    always @( posedge axis_aclk )
    begin
      if ( axis_aresetn == 1'b0 || trg_resetn == 1'b0 ) begin
        rCZT_HIT_LOCK <= 1'b0;
      end
      else begin
        if ( czt_fed == 1'b1 || trg_ack == 1'b1 ) begin
          rCZT_HIT_LOCK <= czt_ready_ie;
        end
        else begin
          rCZT_HIT_LOCK <= ( rCZT_HIT_LOCK & ~(czt_ready) );
        end
      end
    end

    // CsI_Falling_EDGE_Detection
    wire csi_fed;
    FallingEdge csi_hit_fed (
    .axis_aresetn ( axis_aresetn ),
    .axis_aclk    ( axis_aclk    ),
    .signal_in    ( csi_hit_coin ),
    .detected     ( csi_fed      )
    );

    // CsI HIT LOCK
    always @( posedge axis_aclk )
    begin
      if ( axis_aresetn == 1'b0 || trg_resetn == 1'b0 ) begin
        rCSI_HIT_LOCK <= 1'b0;
        rCSI_1ST_LOCK <= 1'b0;
        rCSI_2ND_LOCK <= 1'b0;
      end
      else begin
        if ( csi_fed == 1'b1 || trg_ack == 1'b1 ) begin
          rCSI_HIT_LOCK <= 1'b1;
          rCSI_1ST_LOCK <= cs1_ready_ie;
          rCSI_2ND_LOCK <= cs2_ready_ie;
        end
        else begin
          rCSI_HIT_LOCK <= rCSI_1ST_LOCK | rCSI_2ND_LOCK;
          rCSI_1ST_LOCK <= ( rCSI_1ST_LOCK & ~(cs1_ready) );
          rCSI_2ND_LOCK <= ( rCSI_2ND_LOCK & ~(cs2_ready) );
        end
      end
    end
        
endmodule
