`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/20/2019 12:46:57 PM
// Design Name: 
// Module Name: lemo_ack_gen
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module lemo_ack_gen(
    input  wire axis_aresetn,
    input  wire axis_aclk,
    input  wire trg_resetn,
    input  wire [ 1:0] lpc_lemo_in, // suppose to be active high
    input  wire [ 1:0] hpc_lemo_in, // suppose to be active high
    input  wire [31:0] register_e,
    output wire lpc_lemo_ack,
    output wire lpc_lemo_lock,
    output wire hpc_lemo_ack,
    output wire hpc_lemo_lock
    );
    
    wire [1:0] lpc_lemo_ie = register_e[1:0];
    wire [1:0] hpc_lemo_ie = register_e[5:4];

    reg rLPC_LEMO_ACK;
    reg rLPC_LEMO_FLAG;
    reg rLPC_LEMO_LOCK;
    
    reg rHPC_LEMO_ACK;
    reg rHPC_LEMO_FLAG;
    reg rHPC_LEMO_LOCK;
    
    assign lpc_lemo_ack  = rLPC_LEMO_ACK;
    assign lpc_lemo_lock = rLPC_LEMO_LOCK;
    
    assign hpc_lemo_ack  = rHPC_LEMO_ACK;
    assign hpc_lemo_lock = rHPC_LEMO_LOCK;
    
    always @( posedge axis_aclk )
    begin
      if ( axis_aresetn == 1'b0 || trg_resetn == 1'b0 ) begin
        rLPC_LEMO_ACK  <= 1'b0;
        rLPC_LEMO_FLAG <= 1'b0;
        rLPC_LEMO_LOCK <= 1'b0;
      end
      else begin
        if ( lpc_lemo_in[0] == 1'b1 && lpc_lemo_ie[0] == 1'b1 ) begin
          if ( rLPC_LEMO_FLAG == 1'b0 ) begin
            rLPC_LEMO_ACK  <= 1'b1;
            rLPC_LEMO_FLAG <= 1'b1;
            rLPC_LEMO_LOCK <= 1'b0;
          end
          else begin
            rLPC_LEMO_ACK  <= 1'b0;
            rLPC_LEMO_FLAG <= 1'b1;
            rLPC_LEMO_LOCK <= 1'b1;
          end
        end
        else begin
          rLPC_LEMO_ACK  <= 1'b0;
          rLPC_LEMO_FLAG <= 1'b0;
          if ( lpc_lemo_in[1] == 1'b1 || lpc_lemo_ie[1] == 1'b0 ) begin
             rLPC_LEMO_LOCK <= 1'b0;
          end            
          else begin
            rLPC_LEMO_LOCK <= rLPC_LEMO_LOCK;
          end
        end
      end
    end

    always @( posedge axis_aclk )
    begin
      if ( axis_aresetn == 1'b0 || trg_resetn == 1'b0 ) begin
        rHPC_LEMO_ACK  <= 1'b0;
        rHPC_LEMO_FLAG <= 1'b0;
        rHPC_LEMO_LOCK <= 1'b0;
      end
      else begin
        if ( hpc_lemo_in[0] == 1'b1 && hpc_lemo_ie[0] == 1'b1 ) begin
          if ( rHPC_LEMO_FLAG == 1'b0 ) begin
            rHPC_LEMO_ACK  <= 1'b1;
            rHPC_LEMO_FLAG <= 1'b1;
            rHPC_LEMO_LOCK <= 1'b0;
          end
          else begin
            rHPC_LEMO_ACK  <= 1'b0;
            rHPC_LEMO_FLAG <= 1'b1;
            rHPC_LEMO_LOCK <= 1'b1;
          end
        end
        else begin
          rHPC_LEMO_ACK  <= 1'b0;
          rHPC_LEMO_FLAG <= 1'b0;
          if ( hpc_lemo_in[1] == 1'b1 || hpc_lemo_ie[1] == 1'b0 ) begin
            rHPC_LEMO_LOCK <= 1'b0;
          end            
          else begin
            rHPC_LEMO_LOCK <= rHPC_LEMO_LOCK;
          end
        end
      end
    end
    
endmodule
