`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/20/2019 02:19:16 PM
// Design Name: 
// Module Name: fmc_out_gen
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module fmc_out_gen(
    input  wire axis_aresetn,
    input  wire axis_aclk,
    input  wire trg_ack,
    input  wire evt_id_latch,
    input  wire evt_id_data,
    input  wire trg_enable,
    input  wire sit_hit_or,
    input  wire acd_hit_coin,
    input  wire sit_one_coin,
    input  wire sit_and_coin,
    input  wire sit_two_coin,
    input  wire czt_hit_coin,
    input  wire csi_hit_coin,
    input  wire csi_hit_1st,
    input  wire csi_hit_2nd,
    input  wire acd_hit_lock,
    input  wire sit_hit_lock,
    input  wire czt_hit_lock,
    input  wire csi_hit_lock,
    input  wire lpc_lemo_lock,
    input  wire hpc_lemo_lock,
    input  wire trg_lock,
    input  wire gps_pps,
    input  wire npcs0,
    input  wire [ 7:0] lpc_trg_ack_oe,
    input  wire [ 7:0] lpc_evt_id_latch_oe,
    input  wire [ 7:0] lpc_evt_id_data_oe,
    input  wire [ 7:0] lpc_trg_ena_oe,
    input  wire [ 7:0] hpc_trg_ack_oe,
    input  wire [ 7:0] hpc_evt_id_latch_oe,
    input  wire [ 7:0] hpc_evt_id_data_oe,
    input  wire [ 7:0] hpc_trg_ena_oe,
    input  wire [ 7:0] lpc_fee_hit,
    input  wire [ 7:0] lpc_fee_ready,
    input  wire [ 7:0] lpc_fee_busy,
    input  wire [ 7:0] lpc_fee_spare,
    input  wire [ 7:0] hpc_fee_hit,
    input  wire [ 7:0] hpc_fee_ready,
    input  wire [ 7:0] hpc_fee_busy,
    input  wire [ 7:0] hpc_fee_spare,
    input  wire [31:0] register_c,
    input  wire [31:0] register_d,
    input  wire [31:0] register_e,
    output wire [ 7:0] lpc_trg_ack,
    output wire [ 7:0] lpc_evt_id_latch,
    output wire [ 7:0] lpc_evt_id_data,
    output wire [ 7:0] lpc_trg_ena,
    output wire [ 7:0] hpc_trg_ack,
    output wire [ 7:0] hpc_evt_id_latch,
    output wire [ 7:0] hpc_evt_id_data,
    output wire [ 7:0] hpc_trg_ena,
    output wire [ 1:0] lpc_lemo_out,
    output wire [ 1:0] hpc_lemo_out
    );

    wire dummy_hit;
    wire dummy_ready;
    wire dummy_busy;
    wire dummy_spare;
    
    wire [15:0] fmc_trg_ack;
    wire [15:0] fmc_evt_id_latch;
    wire [15:0] fmc_evt_id_data;
    wire [15:0] fmc_trg_ena;
    wire [15:0] fmc_fee_hit;
    wire [15:0] fmc_fee_ready;
    wire [15:0] fmc_fee_busy;
    wire [15:0] fmc_fee_spare;
    
    wire [3:0] ch0_conn_lpc;
    wire [7:0] ch0_chan_lpc;
    wire [3:0] ch1_conn_lpc;
    wire [7:0] ch1_chan_lpc;
    
    wire [3:0] ch0_conn_hpc;
    wire [7:0] ch0_chan_hpc;
    wire [3:0] ch1_conn_hpc;
    wire [7:0] ch1_chan_hpc;

    wire [1:0] lpc_lemo_oe;
    wire [1:0] hpc_lemo_oe;
    
    reg  [1:0] rLPC_LEMO_OUT;
    reg  [1:0] rHPC_LEMO_OUT;
        
    assign ch0_conn_lpc = register_c[ 3: 0];
    assign ch0_chan_lpc = register_c[15: 8];
    assign ch1_conn_lpc = register_c[19:16];
    assign ch1_chan_lpc = register_c[31:24];

    assign ch0_conn_hpc = register_d[ 3: 0];
    assign ch0_chan_hpc = register_d[15: 8];
    assign ch1_conn_hpc = register_d[19:16];
    assign ch1_chan_hpc = register_d[31:24];

    assign lpc_lemo_oe = register_e[3:2];
    assign hpc_lemo_oe = register_e[7:6];
    
    // lemo_out
    assign lpc_lemo_out = rLPC_LEMO_OUT & lpc_lemo_oe;
    assign hpc_lemo_out = rHPC_LEMO_OUT & hpc_lemo_oe;
    
    // lpc_trg_ack[7:0]
    assign lpc_trg_ack[0] = trg_ack && lpc_trg_ack_oe[0];
    assign lpc_trg_ack[1] = trg_ack && lpc_trg_ack_oe[1];
    assign lpc_trg_ack[2] = trg_ack && lpc_trg_ack_oe[2];
    assign lpc_trg_ack[3] = trg_ack && lpc_trg_ack_oe[3];
    assign lpc_trg_ack[4] = trg_ack && lpc_trg_ack_oe[4];
    assign lpc_trg_ack[5] = trg_ack && lpc_trg_ack_oe[5];
//    assign lpc_trg_ack[6] = trg_ack && lpc_trg_ack_oe[6];
    assign lpc_trg_ack[6] = ( trg_ack || trg_enable ) && lpc_trg_ack_oe[6];
//    assign lpc_trg_ack[7] = trg_ack && lpc_trg_ack_oe[7];
    assign lpc_trg_ack[7] = dummy_hit && lpc_trg_ack_oe[7];
      
    // lpc_evt_id_latch[7:0]
    assign lpc_evt_id_latch[0] = evt_id_latch && lpc_evt_id_latch_oe[0];
    assign lpc_evt_id_latch[1] = evt_id_latch && lpc_evt_id_latch_oe[1];
    assign lpc_evt_id_latch[2] = evt_id_latch && lpc_evt_id_latch_oe[2];
    assign lpc_evt_id_latch[3] = evt_id_latch && lpc_evt_id_latch_oe[3];
    assign lpc_evt_id_latch[4] = evt_id_latch && lpc_evt_id_latch_oe[4];
    assign lpc_evt_id_latch[5] = evt_id_latch && lpc_evt_id_latch_oe[5];
    assign lpc_evt_id_latch[6] = evt_id_latch && lpc_evt_id_latch_oe[6];
//    assign lpc_evt_id_latch[7] = evt_id_latch && lpc_evt_id_latch_oe[7];
    assign lpc_evt_id_latch[7] = dummy_ready  && lpc_evt_id_latch_oe[7];
      
    // lpc_evt_id_data[7:0]
    assign lpc_evt_id_data[0] = evt_id_data && lpc_evt_id_data_oe[0];
    assign lpc_evt_id_data[1] = evt_id_data && lpc_evt_id_data_oe[1];
    assign lpc_evt_id_data[2] = evt_id_data && lpc_evt_id_data_oe[2];
    assign lpc_evt_id_data[3] = evt_id_data && lpc_evt_id_data_oe[3];
    assign lpc_evt_id_data[4] = evt_id_data && lpc_evt_id_data_oe[4];
    assign lpc_evt_id_data[5] = evt_id_data && lpc_evt_id_data_oe[5];
    assign lpc_evt_id_data[6] = evt_id_data && lpc_evt_id_data_oe[6];
//    assign lpc_evt_id_data[7] = evt_id_data && lpc_evt_id_data_oe[7];
    assign lpc_evt_id_data[7] = dummy_busy  && lpc_evt_id_data_oe[7];

    // lpc_trg_ena[7:0]
    assign lpc_trg_ena[0] = sit_hit_or && lpc_trg_ena_oe[0];
    assign lpc_trg_ena[1] = sit_hit_or && lpc_trg_ena_oe[1];
    assign lpc_trg_ena[2] = sit_hit_or && lpc_trg_ena_oe[2];
    assign lpc_trg_ena[3] = sit_hit_or && lpc_trg_ena_oe[3];
    assign lpc_trg_ena[4] = trg_enable && lpc_trg_ena_oe[4];
//    assign lpc_trg_ena[5] = trg_enable && lpc_trg_ena_oe[5];
    assign lpc_trg_ena[5] = gps_pps    && lpc_trg_ena_oe[5];
    assign lpc_trg_ena[6] = npcs0      && lpc_trg_ena_oe[6];
//    assign lpc_trg_ena[7] = npcs0      && lpc_trg_ena_oe[7];
//    assign lpc_trg_ena[7] = dummy_spare && lpc_trg_ena_oe[7];
    assign lpc_trg_ena[7] = gps_pps    && lpc_trg_ena_oe[7];
    
    // hpc_trg_ack[7:0]
    assign hpc_trg_ack[0] = trg_ack && hpc_trg_ack_oe[0];
    assign hpc_trg_ack[1] = trg_ack && hpc_trg_ack_oe[1];
    assign hpc_trg_ack[2] = trg_ack && hpc_trg_ack_oe[2];
    assign hpc_trg_ack[3] = trg_ack && hpc_trg_ack_oe[3];
    assign hpc_trg_ack[4] = trg_ack && hpc_trg_ack_oe[4];
    assign hpc_trg_ack[5] = trg_ack && hpc_trg_ack_oe[5];
    assign hpc_trg_ack[6] = trg_ack && hpc_trg_ack_oe[6];
    assign hpc_trg_ack[7] = trg_ack && hpc_trg_ack_oe[7];
    
    // hpc_evt_id_latch[7:0]
    assign hpc_evt_id_latch[0] = evt_id_latch && hpc_evt_id_latch_oe[0];
    assign hpc_evt_id_latch[1] = evt_id_latch && hpc_evt_id_latch_oe[1];
    assign hpc_evt_id_latch[2] = evt_id_latch && hpc_evt_id_latch_oe[2];
    assign hpc_evt_id_latch[3] = evt_id_latch && hpc_evt_id_latch_oe[3];
    assign hpc_evt_id_latch[4] = evt_id_latch && hpc_evt_id_latch_oe[4];
    assign hpc_evt_id_latch[5] = evt_id_latch && hpc_evt_id_latch_oe[5];
    assign hpc_evt_id_latch[6] = evt_id_latch && hpc_evt_id_latch_oe[6];
    assign hpc_evt_id_latch[7] = evt_id_latch && hpc_evt_id_latch_oe[7];
    
    // hpc_evt_id_data[7:0]
    assign hpc_evt_id_data[0] = evt_id_data && hpc_evt_id_data_oe[0];
    assign hpc_evt_id_data[1] = evt_id_data && hpc_evt_id_data_oe[1];
    assign hpc_evt_id_data[2] = evt_id_data && hpc_evt_id_data_oe[2];
    assign hpc_evt_id_data[3] = evt_id_data && hpc_evt_id_data_oe[3];
    assign hpc_evt_id_data[4] = evt_id_data && hpc_evt_id_data_oe[4];
    assign hpc_evt_id_data[5] = evt_id_data && hpc_evt_id_data_oe[5];
    assign hpc_evt_id_data[6] = evt_id_data && hpc_evt_id_data_oe[6];
    assign hpc_evt_id_data[7] = evt_id_data && hpc_evt_id_data_oe[7];

    // hpc_trg_ena[7:0]
    assign hpc_trg_ena[0] = sit_hit_or && hpc_trg_ena_oe[0];
    assign hpc_trg_ena[1] = sit_hit_or && hpc_trg_ena_oe[1];
    assign hpc_trg_ena[2] = sit_hit_or && hpc_trg_ena_oe[2];
    assign hpc_trg_ena[3] = sit_hit_or && hpc_trg_ena_oe[3];
    assign hpc_trg_ena[4] = sit_hit_or && hpc_trg_ena_oe[4];
    assign hpc_trg_ena[5] = sit_hit_or && hpc_trg_ena_oe[5];
    assign hpc_trg_ena[6] = sit_hit_or && hpc_trg_ena_oe[6];
    assign hpc_trg_ena[7] = sit_hit_or && hpc_trg_ena_oe[7];

    assign fmc_trg_ack     [ 7:0] = lpc_trg_ack     [7:0];
    assign fmc_evt_id_latch[ 7:0] = lpc_evt_id_latch[7:0];
    assign fmc_evt_id_data [ 7:0] = lpc_evt_id_data [7:0];
    assign fmc_trg_ena     [ 7:0] = lpc_trg_ena     [7:0];
    assign fmc_fee_hit     [ 7:0] = lpc_fee_hit     [7:0];
    assign fmc_fee_ready   [ 7:0] = lpc_fee_ready   [7:0];
    assign fmc_fee_busy    [ 7:0] = lpc_fee_busy    [7:0];
    assign fmc_fee_spare   [ 7:0] = lpc_fee_spare   [7:0];

    assign fmc_trg_ack     [15:8] = hpc_trg_ack     [7:0];
    assign fmc_evt_id_latch[15:8] = hpc_evt_id_latch[7:0];
    assign fmc_evt_id_data [15:8] = hpc_evt_id_data [7:0];
    assign fmc_trg_ena     [15:8] = hpc_trg_ena     [7:0];
    assign fmc_fee_hit     [15:8] = hpc_fee_hit     [7:0];
    assign fmc_fee_ready   [15:8] = hpc_fee_ready   [7:0];
    assign fmc_fee_busy    [15:8] = hpc_fee_busy    [7:0];
    assign fmc_fee_spare   [15:8] = hpc_fee_spare   [7:0];

    // dummy_fee
    dummy_fee dummy_fee_inst (
    .aclk     ( axis_aclk    ),
    .aresetn  ( axis_aresetn ),
    .trg_ack  ( trg_ack      ),
    .trg_lock ( trg_lock     ),
    .fee_hit  ( dummy_hit    ),
    .fee_ready( dummy_ready  ),
    .fee_busy ( dummy_busy   ),
    .fee_spare( dummy_spare  )
    );

    always @( posedge axis_aclk )
    begin
      if ( axis_aresetn == 1'b0 ) begin
        rLPC_LEMO_OUT[0] <= 1'b0;
        rLPC_LEMO_OUT[1] <= 1'b0;
        rHPC_LEMO_OUT[0] <= 1'b0;
        rHPC_LEMO_OUT[1] <= 1'b0;
      end
      else begin
        // lpc_lemo_out[0]
        case (ch0_chan_lpc)
        8'b00000000 : rLPC_LEMO_OUT[0] <= trg_lock;
        8'b00000001 : rLPC_LEMO_OUT[0] <= fmc_trg_ack     [ch0_conn_lpc];
        8'b00000010 : rLPC_LEMO_OUT[0] <= fmc_evt_id_latch[ch0_conn_lpc];
        8'b00000011 : rLPC_LEMO_OUT[0] <= fmc_evt_id_data [ch0_conn_lpc];
        8'b00000100 : rLPC_LEMO_OUT[0] <= fmc_trg_ena     [ch0_conn_lpc];
        8'b00000101 : rLPC_LEMO_OUT[0] <= fmc_fee_hit     [ch0_conn_lpc];
        8'b00000110 : rLPC_LEMO_OUT[0] <= fmc_fee_ready   [ch0_conn_lpc];
        8'b00000111 : rLPC_LEMO_OUT[0] <= fmc_fee_busy    [ch0_conn_lpc];
        8'b00001000 : rLPC_LEMO_OUT[0] <= fmc_fee_spare   [ch0_conn_lpc];
        8'b00001001 : rLPC_LEMO_OUT[0] <= acd_hit_coin;
        8'b00001010 : rLPC_LEMO_OUT[0] <= sit_one_coin;
        8'b00001011 : rLPC_LEMO_OUT[0] <= sit_and_coin;
        8'b00001100 : rLPC_LEMO_OUT[0] <= sit_two_coin;
        8'b00001101 : rLPC_LEMO_OUT[0] <= czt_hit_coin;
        8'b00001110 : rLPC_LEMO_OUT[0] <= csi_hit_coin;
        8'b00001111 : rLPC_LEMO_OUT[0] <= csi_hit_1st;
        8'b00010000 : rLPC_LEMO_OUT[0] <= csi_hit_2nd;
        8'b00010001 : rLPC_LEMO_OUT[0] <= acd_hit_lock;
        8'b00010010 : rLPC_LEMO_OUT[0] <= sit_hit_lock;
        8'b00010011 : rLPC_LEMO_OUT[0] <= czt_hit_lock;
        8'b00010100 : rLPC_LEMO_OUT[0] <= csi_hit_lock;
        8'b00010101 : rLPC_LEMO_OUT[0] <= lpc_lemo_lock;
        8'b00010110 : rLPC_LEMO_OUT[0] <= hpc_lemo_lock;
        8'b00010111 : rLPC_LEMO_OUT[0] <= 1'b0;
        default:      rLPC_LEMO_OUT[0] <= trg_lock;
        endcase
        
        // lpc_lemo_out[1]
        case (ch1_chan_lpc)
        8'b00000000 : rLPC_LEMO_OUT[1] <= trg_lock;
        8'b00000001 : rLPC_LEMO_OUT[1] <= fmc_trg_ack     [ch1_conn_lpc];
        8'b00000010 : rLPC_LEMO_OUT[1] <= fmc_evt_id_latch[ch1_conn_lpc];
        8'b00000011 : rLPC_LEMO_OUT[1] <= fmc_evt_id_data [ch1_conn_lpc];
        8'b00000100 : rLPC_LEMO_OUT[1] <= fmc_trg_ena     [ch1_conn_lpc];
        8'b00000101 : rLPC_LEMO_OUT[1] <= fmc_fee_hit     [ch1_conn_lpc];
        8'b00000110 : rLPC_LEMO_OUT[1] <= fmc_fee_ready   [ch1_conn_lpc];
        8'b00000111 : rLPC_LEMO_OUT[1] <= fmc_fee_busy    [ch1_conn_lpc];
        8'b00001000 : rLPC_LEMO_OUT[1] <= fmc_fee_spare   [ch1_conn_lpc];
        8'b00001001 : rLPC_LEMO_OUT[1] <= acd_hit_coin;
        8'b00001010 : rLPC_LEMO_OUT[1] <= sit_one_coin;
        8'b00001011 : rLPC_LEMO_OUT[1] <= sit_and_coin;
        8'b00001100 : rLPC_LEMO_OUT[1] <= sit_two_coin;
        8'b00001101 : rLPC_LEMO_OUT[1] <= czt_hit_coin;
        8'b00001110 : rLPC_LEMO_OUT[1] <= csi_hit_coin;
        8'b00001111 : rLPC_LEMO_OUT[1] <= csi_hit_1st;
        8'b00010000 : rLPC_LEMO_OUT[1] <= csi_hit_2nd;
        8'b00010001 : rLPC_LEMO_OUT[1] <= acd_hit_lock;
        8'b00010010 : rLPC_LEMO_OUT[1] <= sit_hit_lock;
        8'b00010011 : rLPC_LEMO_OUT[1] <= czt_hit_lock;
        8'b00010100 : rLPC_LEMO_OUT[1] <= csi_hit_lock;
        8'b00010101 : rLPC_LEMO_OUT[1] <= lpc_lemo_lock;
        8'b00010110 : rLPC_LEMO_OUT[1] <= hpc_lemo_lock;
        8'b00010111 : rLPC_LEMO_OUT[1] <= 1'b0;
        default:      rLPC_LEMO_OUT[1] <= trg_lock;
        endcase

        // hpc_lemo_out[0]
        case (ch0_chan_hpc)
        8'b00000000 : rHPC_LEMO_OUT[0] <= trg_lock;
        8'b00000001 : rHPC_LEMO_OUT[0] <= fmc_trg_ack     [ch0_conn_hpc];
        8'b00000010 : rHPC_LEMO_OUT[0] <= fmc_evt_id_latch[ch0_conn_hpc];
        8'b00000011 : rHPC_LEMO_OUT[0] <= fmc_evt_id_data [ch0_conn_hpc];
        8'b00000100 : rHPC_LEMO_OUT[0] <= fmc_trg_ena     [ch0_conn_hpc];
        8'b00000101 : rHPC_LEMO_OUT[0] <= fmc_fee_hit     [ch0_conn_hpc];
        8'b00000110 : rHPC_LEMO_OUT[0] <= fmc_fee_ready   [ch0_conn_hpc];
        8'b00000111 : rHPC_LEMO_OUT[0] <= fmc_fee_busy    [ch0_conn_hpc];
        8'b00001000 : rHPC_LEMO_OUT[0] <= fmc_fee_spare   [ch0_conn_hpc];
        8'b00001001 : rHPC_LEMO_OUT[0] <= acd_hit_coin;
        8'b00001010 : rHPC_LEMO_OUT[0] <= sit_one_coin;
        8'b00001011 : rHPC_LEMO_OUT[0] <= sit_and_coin;
        8'b00001100 : rHPC_LEMO_OUT[0] <= sit_two_coin;
        8'b00001101 : rHPC_LEMO_OUT[0] <= czt_hit_coin;
        8'b00001110 : rHPC_LEMO_OUT[0] <= csi_hit_coin;
        8'b00001111 : rHPC_LEMO_OUT[0] <= csi_hit_1st;
        8'b00010000 : rHPC_LEMO_OUT[0] <= csi_hit_2nd;
        8'b00010001 : rHPC_LEMO_OUT[0] <= acd_hit_lock;
        8'b00010010 : rHPC_LEMO_OUT[0] <= sit_hit_lock;
        8'b00010011 : rHPC_LEMO_OUT[0] <= czt_hit_lock;
        8'b00010100 : rHPC_LEMO_OUT[0] <= csi_hit_lock;
        8'b00010101 : rHPC_LEMO_OUT[0] <= lpc_lemo_lock;
        8'b00010110 : rHPC_LEMO_OUT[0] <= hpc_lemo_lock;
        8'b00010111 : rHPC_LEMO_OUT[0] <= 1'b0;
        default:      rHPC_LEMO_OUT[0] <= trg_lock;
        endcase

        // hpc_lemo_out[1]
        case (ch1_chan_hpc)
        8'b00000000 : rHPC_LEMO_OUT[1] <= trg_lock;
        8'b00000001 : rHPC_LEMO_OUT[1] <= fmc_trg_ack     [ch1_conn_hpc];
        8'b00000010 : rHPC_LEMO_OUT[1] <= fmc_evt_id_latch[ch1_conn_hpc];
        8'b00000011 : rHPC_LEMO_OUT[1] <= fmc_evt_id_data [ch1_conn_hpc];
        8'b00000100 : rHPC_LEMO_OUT[1] <= fmc_trg_ena     [ch1_conn_hpc];
        8'b00000101 : rHPC_LEMO_OUT[1] <= fmc_fee_hit     [ch1_conn_hpc];
        8'b00000110 : rHPC_LEMO_OUT[1] <= fmc_fee_ready   [ch1_conn_hpc];
        8'b00000111 : rHPC_LEMO_OUT[1] <= fmc_fee_busy    [ch1_conn_hpc];
        8'b00001000 : rHPC_LEMO_OUT[1] <= fmc_fee_spare   [ch1_conn_hpc];
        8'b00001001 : rHPC_LEMO_OUT[1] <= acd_hit_coin;
        8'b00001010 : rHPC_LEMO_OUT[1] <= sit_one_coin;
        8'b00001011 : rHPC_LEMO_OUT[1] <= sit_and_coin;
        8'b00001100 : rHPC_LEMO_OUT[1] <= sit_two_coin;
        8'b00001101 : rHPC_LEMO_OUT[1] <= czt_hit_coin;
        8'b00001110 : rHPC_LEMO_OUT[1] <= csi_hit_coin;
        8'b00001111 : rHPC_LEMO_OUT[1] <= csi_hit_1st;
        8'b00010000 : rHPC_LEMO_OUT[1] <= csi_hit_2nd;
        8'b00010001 : rHPC_LEMO_OUT[1] <= acd_hit_lock;
        8'b00010010 : rHPC_LEMO_OUT[1] <= sit_hit_lock;
        8'b00010011 : rHPC_LEMO_OUT[1] <= czt_hit_lock;
        8'b00010100 : rHPC_LEMO_OUT[1] <= csi_hit_lock;
        8'b00010101 : rHPC_LEMO_OUT[1] <= lpc_lemo_lock;
        8'b00010110 : rHPC_LEMO_OUT[1] <= hpc_lemo_lock;
        8'b00010111 : rHPC_LEMO_OUT[1] <= 1'b0;
        default:      rHPC_LEMO_OUT[1] <= trg_lock;
        endcase
      end
    end
       
endmodule
