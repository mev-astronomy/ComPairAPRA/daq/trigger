`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08/28/2019 12:02:05 PM
// Design Name: 
// Module Name: event_id
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//
// -- 2021Jun18
//     n_delay = register_1[15:8] 
// --> n_delay = register_1[15:8]+1'b1
// 2021Jun18 --
//
//////////////////////////////////////////////////////////////////////////////////


module event_id(
    input  wire aclk,
    input  wire aresetn,
    input  wire trg_ack,
    input  wire [31:0] register_1,
    output wire busy,
    output wire evt_id_latch,
    output wire evt_id_data,
    output wire [31:0] evt_id
    );

    // n_delay == ack_width in trg_ack_gen    
    wire [7:0] n_delay;
    wire [7:0] n_width;
    
    assign n_delay = register_1[15: 8] + 1'b1;
    assign n_width = register_1[23:16];
        
    localparam IDLE=5'b00001, INIT=5'b00010, TRIG=5'b00100, GIRT= 5'b01000, INCR=5'b10000;
    reg [4:0] state;
    
    reg rBUSY;
    reg rID_LATCH;
    reg rID_DATA;
    reg [31:0] rEVT_ID;
    
    reg [4:0] rNUM_CLK;
    
    reg [7:0] rDELAY;
    reg [7:0] rWIDTH;
    
    assign busy         = rBUSY;
    assign evt_id_latch = rID_LATCH;
    assign evt_id_data  = rID_DATA;
    assign evt_id       = rEVT_ID;
    
    always @( posedge aclk )
    begin
      if ( aresetn == 1'b0 ) begin
        state <= IDLE;
      end
      else begin
        case (state)
        IDLE: begin
                if ( trg_ack == 1'b1 ) begin
                  state <= INIT;
                end
                else begin
                  state <= IDLE;
                end
              end
        INIT: begin
                if ( rDELAY == n_delay ) begin
                  state <= TRIG;
                end
                else begin
                  state <= INIT;
                end
              end
        TRIG: begin
                if ( rWIDTH == n_width ) begin
                  if ( rNUM_CLK == 5'b11111 ) begin
                    state <= INCR;
                  end
                  else begin
                    state <= GIRT;
                  end
                end
                else begin
                  state <= TRIG;
                end
              end
        GIRT: begin
                if ( rWIDTH == n_width ) begin
                  state <= TRIG;
                end
                else begin
                  state <= GIRT;
                end
              end
        INCR: begin
                if ( rWIDTH == n_width ) begin
                  state <= IDLE;
                end
                else begin
                  state <= INCR;
                end
              end
        default: begin
                   state <= IDLE;
                 end
        endcase
      end
    end
        
    always @( posedge aclk )
    begin
      if ( aresetn == 1'b0 ) begin
        rDELAY    <= 8'b00000000;
        rWIDTH    <= 8'b00000000;
        rBUSY     <= 1'b0;
        rID_LATCH <= 1'b0;
        rID_DATA  <= 1'b0;
        rEVT_ID   <= 32'h00000000;
        rNUM_CLK  <= 5'b00000;
      end
      else begin
        case (state)
        IDLE: begin
                rDELAY    <= 8'b00000000;
                rWIDTH    <= 8'b00000000;
                rBUSY     <= 1'b0;
                rID_LATCH <= 1'b0;
                rID_DATA  <= 1'b0;
                rEVT_ID   <= rEVT_ID;
                rNUM_CLK  <= 5'b00000;
              end
        INIT: begin
                rWIDTH <= 8'b00000000;
                if ( rDELAY == n_delay ) begin
                  rDELAY    <= 8'b00000000;
                  rBUSY     <= 1'b1;
                  rID_LATCH <= 1'b0;
                  rID_DATA  <= 1'b1;
                  rEVT_ID   <= rEVT_ID;
                  rNUM_CLK  <= 5'b00000;
                end
                else begin
                  rDELAY    <= rDELAY + 1'b1;
                  rBUSY     <= rBUSY;
                  rID_LATCH <= rID_LATCH;
                  rID_DATA  <= rID_DATA;
                  rEVT_ID   <= rEVT_ID;
                  rNUM_CLK  <= rNUM_CLK;
                end
              end
        TRIG: begin
                rDELAY <= 8'b00000000;
                if ( rWIDTH == n_width ) begin
                  rWIDTH    <= 8'b00000000;
                  rBUSY     <= 1'b1;
                  rID_LATCH <= 1'b1;
                  rID_DATA  <= rID_DATA;
                  rEVT_ID   <= rEVT_ID;
                  rNUM_CLK  <= rNUM_CLK;
                end
                else begin
                  rWIDTH    <= rWIDTH + 1'b1;
                  rBUSY     <= rBUSY;
                  rID_LATCH <= rID_LATCH;
                  rID_DATA  <= rID_DATA;
                  rEVT_ID   <= rEVT_ID;
                  rNUM_CLK  <= rNUM_CLK;
                end
              end
        GIRT: begin
                rDELAY <= 8'b00000000;
                if ( rWIDTH == n_width ) begin
                  rWIDTH    <= 8'b00000000;
                  rBUSY     <= 1'b1;
                  rID_LATCH <= 1'b0;
                  rID_DATA  <= rEVT_ID[30-rNUM_CLK];
                  rEVT_ID   <= rEVT_ID;
                  rNUM_CLK  <= rNUM_CLK+1'b1;
                end
                else begin
                  rWIDTH    <= rWIDTH + 1'b1;
                  rBUSY     <= rBUSY;
                  rID_LATCH <= rID_LATCH;
                  rID_DATA  <= rID_DATA;
                  rEVT_ID   <= rEVT_ID;
                  rNUM_CLK  <= rNUM_CLK;
                end
              end
        INCR: begin
                rDELAY <= 8'b00000000;
                if ( rWIDTH == n_width ) begin
                  rWIDTH    <= 8'b00000000;
                  rBUSY     <= 1'b1;
                  rID_LATCH <= 1'b0;
                  rID_DATA  <= 1'b0;
                  rEVT_ID   <= rEVT_ID + 1'b1;
                  rNUM_CLK  <= 5'b00000;
                end
                else begin
                  rWIDTH    <= rWIDTH + 1'b1;
                  rBUSY     <= rBUSY;
                  rID_LATCH <= rID_LATCH;
                  rID_DATA  <= rID_DATA;
                  rEVT_ID   <= rEVT_ID;
                  rNUM_CLK  <= rNUM_CLK;
                end
              end
        default: begin
                   rDELAY    <= 8'b00000000;
                   rWIDTH    <= 8'b00000000;
                   rBUSY     <= 1'b0;
                   rID_LATCH <= 1'b0;
                   rID_DATA  <= 1'b0;
                   rEVT_ID   <= 32'h00000000;
                   rNUM_CLK  <= 5'b00000;
                 end
        endcase
      end
    end
    
endmodule
