`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/26/2019 02:05:15 PM
// Design Name: 
// Module Name: external_trigger
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module external_trigger(
    input  wire axis_aresetn,
    input  wire axis_aclk,
    input  wire [31:0] register_2,
    output wire ext_ack
    );

    reg rEXT_FLAG;
    reg [31:0] rEXT_COAR;
    reg [ 7:0] rEXT_FINE;
    reg [ 7:0] rEXT_OUT;

    wire [ 8:0] ext_mask = ( register_2[7:0] == 8'b00000000 ) ? 8'b00000000 : register_2[7:0] + register_2[7:0] - 1'b1;
    
    assign ext_ack = ( ( rEXT_FLAG == 1'b1 ) && ( rEXT_OUT > 8'b00000000 ) ); 
    
    always @( posedge axis_aclk )
    begin
      if ( axis_aresetn == 1'b0 ) begin
        rEXT_FLAG <= 1'b0;
        rEXT_FINE <= 8'b00000000;
      end 
      else begin
        rEXT_FINE <= rEXT_FINE + 1'b1;          
        if ( ext_mask && ( ( rEXT_FINE & ext_mask ) == ext_mask ) ) begin
          rEXT_FLAG <= 1'b1;
        end
        else begin
          rEXT_FLAG <= 1'b0;
        end
      end
    end
 
    always @( posedge axis_aclk )
    begin
      if ( axis_aresetn == 1'b0 ) begin
        rEXT_COAR <= 32'h00000000;
      end
      else begin
        if ( rEXT_FLAG == 1'b1 ) begin
          rEXT_COAR <= rEXT_COAR + 1'b1;
        end
        else begin
          rEXT_COAR <= rEXT_COAR;
        end
      end
    end

    always @( posedge axis_aclk )
    begin
      if ( axis_aresetn == 1'b0 ) begin
        rEXT_OUT <= 8'b00000000;
      end
      else begin
        if ( ( rEXT_COAR[ 3:0] ==  4'hf ) && register_2[8] ) begin
          rEXT_OUT[0] <= 1'b1;
        end
        else begin
          rEXT_OUT[0] <= 1'b0;
        end
        if ( ( rEXT_COAR[ 7:0] ==  8'hff ) && register_2[9] ) begin
          rEXT_OUT[1] <= 1'b1;
        end
        else begin
          rEXT_OUT[1] <= 1'b0;
        end
        if ( ( rEXT_COAR[11:0] == 12'hfff ) && register_2[10] ) begin
          rEXT_OUT[2] <= 1'b1;
        end 
        else begin
          rEXT_OUT[2] <= 1'b0;
        end
        if ( ( rEXT_COAR[15:0] == 16'hffff ) && register_2[11] ) begin
          rEXT_OUT[3] <= 1'b1;
        end
        else begin
          rEXT_OUT[3] <= 1'b0;
        end
        if ( ( rEXT_COAR[19:0] == 20'hfffff ) && register_2[12] ) begin
          rEXT_OUT[4] <= 1'b1;
        end
        else begin
          rEXT_OUT[4] <= 1'b0;
        end
        if ( ( rEXT_COAR[23:0] == 24'hffffff ) && register_2[13] ) begin
          rEXT_OUT[5] <= 1'b1;
        end
        else begin
          rEXT_OUT[5] <= 1'b0;
        end
        if ( ( rEXT_COAR[27:0] == 28'hfffffff ) && register_2[14] ) begin
          rEXT_OUT[6] <= 1'b1;
        end
        else begin
          rEXT_OUT[6] <= 1'b0;
        end
        if ( ( rEXT_COAR[31:0] == 32'hffffffff ) && register_2[15] ) begin
          rEXT_OUT[7] <= 1'b1;
        end
        else begin
          rEXT_OUT[7] <= 1'b0;
        end
      end
    end
   
endmodule
