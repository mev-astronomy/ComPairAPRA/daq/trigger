`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/28/2019 10:30:58 AM
// Design Name: 
// Module Name: evt_ack_gen
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

// -- 2019Jul28
//  c_mode_0:  acd &  sit_one &  czt &  csi 
//  c_mode_1:  acd &  sit_and &  czt &  csi
//  c_mode_2:  acd &  sit_two &  czt &  csi
//  c_mode_3: !acd &  sit_one &  czt &  csi
//  c_mode_4: !acd &  sit_and &  czt &  csi
//  c_mode_5: !acd &  sit_two &  czt &  csi
//  c_mode_6: !acd &  sit_one &  czt & !csi
//  c_mode_7: !acd &  sit_and &  czt & !csi
//  c_mode_8: !acd &  sit_two &  czt & !csi
//  c_mode_9: !acd &  sit_one & !czt &  csi
//  c_mode_a: !acd &  sit_and & !czt &  csi
//  c_mode_b: !acd &  sit_two & !czt &  csi
//  c_mode_c: !acd & !sit_one &  czt &  csi
//  c_mode_d: !acd &  sit_one & !czt & !csi
//  c_mode_e: !acd & !sit_one &  czt & !csi
//  c_mode_f: !acd & !sit_one & !czt &  csi
// 2019Jul28 --

// -- 2021Dec02
//  c_mode_0: !acd &  sit_one & !czt & !csi 
//  c_mode_1: !acd &  sit_and & !czt & !csi
//  c_mode_2: !acd &  sit_two & !czt & !csi
//  c_mode_3: !acd &  sit_one &  czt &  csi
//  c_mode_4: !acd &  sit_and &  czt &  csi
//  c_mode_5: !acd &  sit_two &  czt &  csi
//  c_mode_6: !acd &  sit_one &  czt & !csi
//  c_mode_7: !acd &  sit_and &  czt & !csi
//  c_mode_8: !acd &  sit_two &  czt & !csi
//  c_mode_9: !acd &  sit_one & !czt &  csi
//  c_mode_a: !acd &  sit_and & !czt &  csi
//  c_mode_b: !acd &  sit_two & !czt &  csi
//  c_mode_c: !acd & !sit_one &  czt &  csi
//  c_mode_d: !acd &  sit_one & !czt & !csi
//  c_mode_e: !acd & !sit_one &  czt & !csi
//  c_mode_f: !acd & !sit_one & !czt &  csi
// 2021Dec02 --

module evt_ack_gen(
    input  wire axis_aresetn,
    input  wire axis_aclk,
    input  wire acd_hit_coin,
    input  wire sit_one_coin,
    input  wire sit_and_coin,
    input  wire sit_two_coin,
    input  wire czt_hit_coin,
    input  wire csi_hit_coin,
    input  wire [31:0] register_2,
    input  wire [31:0] register_3,
    input  wire [31:0] register_4,
    input  wire [31:0] register_5,
    input  wire [31:0] register_6,
    input  wire [31:0] register_7,
    output wire evt_ack,
    output wire [15:0] mode_hit,
    output wire [15:0] mode_ack
    );

    integer i;
    
    wire [7:0] iMargin = 8'b00010101; // = 21 (dec)
    
    wire [15:0] c_mode = register_2[31:16];
    
    wire [7:0] cm_0_cd = register_3[ 7: 0];
    wire [7:0] cm_1_cd = register_3[15: 8];
    wire [7:0] cm_2_cd = register_3[23:16];
    wire [7:0] cm_3_cd = register_3[31:24];

    wire [7:0] cm_4_cd = register_4[ 7: 0];
    wire [7:0] cm_5_cd = register_4[15: 8];
    wire [7:0] cm_6_cd = register_4[23:16];
    wire [7:0] cm_7_cd = register_4[31:24];

    wire [7:0] cm_8_cd = register_5[ 7: 0];
    wire [7:0] cm_9_cd = register_5[15: 8];
    wire [7:0] cm_a_cd = register_5[23:16];
    wire [7:0] cm_b_cd = register_5[31:24];

    wire [7:0] cm_c_cd = register_6[ 7: 0];
    wire [7:0] cm_d_cd = register_6[15: 8];
    wire [7:0] cm_e_cd = register_6[23:16];
    wire [7:0] cm_f_cd = register_6[31:24];

    wire [7:0] acd_width = register_7[ 7: 0];
    wire [7:0] sit_width = register_7[15: 8];
    wire [7:0] czt_width = register_7[23:16];
    wire [7:0] csi_width = register_7[31:24];

//    wire coin_mode_0 =  acd_hit_coin &  sit_one_coin &  czt_hit_coin &  csi_hit_coin;
//    wire coin_mode_1 =  acd_hit_coin &  sit_and_coin &  czt_hit_coin &  csi_hit_coin;
//    wire coin_mode_2 =  acd_hit_coin &  sit_two_coin &  czt_hit_coin &  csi_hit_coin;
    wire coin_mode_0 = !acd_hit_coin &  sit_one_coin & !czt_hit_coin & !csi_hit_coin;
    wire coin_mode_1 = !acd_hit_coin &  sit_and_coin & !czt_hit_coin & !csi_hit_coin;
    wire coin_mode_2 = !acd_hit_coin &  sit_two_coin & !czt_hit_coin & !csi_hit_coin;
    wire coin_mode_3 = !acd_hit_coin &  sit_one_coin &  czt_hit_coin &  csi_hit_coin;
    wire coin_mode_4 = !acd_hit_coin &  sit_and_coin &  czt_hit_coin &  csi_hit_coin;
    wire coin_mode_5 = !acd_hit_coin &  sit_two_coin &  czt_hit_coin &  csi_hit_coin;
    wire coin_mode_6 = !acd_hit_coin &  sit_one_coin &  czt_hit_coin & !csi_hit_coin;
    wire coin_mode_7 = !acd_hit_coin &  sit_and_coin &  czt_hit_coin & !csi_hit_coin;
    wire coin_mode_8 = !acd_hit_coin &  sit_two_coin &  czt_hit_coin & !csi_hit_coin;
    wire coin_mode_9 = !acd_hit_coin &  sit_one_coin & !czt_hit_coin &  csi_hit_coin;
    wire coin_mode_a = !acd_hit_coin &  sit_and_coin & !czt_hit_coin &  csi_hit_coin;
    wire coin_mode_b = !acd_hit_coin &  sit_two_coin & !czt_hit_coin &  csi_hit_coin;
    wire coin_mode_c = !acd_hit_coin & !sit_one_coin &  czt_hit_coin &  csi_hit_coin;
    wire coin_mode_d = !acd_hit_coin &  sit_one_coin & !czt_hit_coin & !csi_hit_coin;
    wire coin_mode_e = !acd_hit_coin & !sit_one_coin &  czt_hit_coin & !csi_hit_coin;
    wire coin_mode_f = !acd_hit_coin & !sit_one_coin & !czt_hit_coin &  csi_hit_coin;
    
    // 3: CsI, 2: CZT, 1: SiT, 0: ACD
    reg [3:0] rWINDOW;

    reg rCOIN_STRB;
    reg rCOIN_FLAG;
    reg [7:0] rCOIN_WIDTH;
    
    assign evt_ack = ( mode_ack > 0 );
        
    mode_ack_gen mode_0 (
    .aclk     ( axis_aclk    ),
    .aresetn  ( axis_aresetn ),
    .enable   ( c_mode[0]    ),
    .strb     ( rCOIN_STRB   ),
    .coin     ( coin_mode_0  ),
    .coin_cd  ( cm_0_cd      ),
    .mode_hit ( mode_hit[0]  ),
    .mode_ack ( mode_ack[0]  )
    );

    mode_ack_gen mode_1 (
    .aclk     ( axis_aclk    ),
    .aresetn  ( axis_aresetn ),
    .enable   ( c_mode[1]    ),
    .strb     ( rCOIN_STRB   ),
    .coin     ( coin_mode_1  ),
    .coin_cd  ( cm_1_cd      ),
    .mode_hit ( mode_hit[1]  ),
    .mode_ack ( mode_ack[1]  )
    );
    
    mode_ack_gen mode_2 (
    .aclk     ( axis_aclk    ),
    .aresetn  ( axis_aresetn ),
    .enable   ( c_mode[2]    ),
    .strb     ( rCOIN_STRB   ),
    .coin     ( coin_mode_2  ),
    .coin_cd  ( cm_2_cd      ),
    .mode_hit ( mode_hit[2]  ),
    .mode_ack ( mode_ack[2]  )
    );

    mode_ack_gen mode_3 (
    .aclk     ( axis_aclk    ),
    .aresetn  ( axis_aresetn ),
    .enable   ( c_mode[3]    ),
    .strb     ( rCOIN_STRB   ),
    .coin     ( coin_mode_3  ),
    .coin_cd  ( cm_3_cd      ),
    .mode_hit ( mode_hit[3]  ),
    .mode_ack ( mode_ack[3]  )
    );

     mode_ack_gen mode_4 (
    .aclk     ( axis_aclk    ),
    .aresetn  ( axis_aresetn ),
    .enable   ( c_mode[4]    ),
    .strb     ( rCOIN_STRB   ),
    .coin     ( coin_mode_4  ),
    .coin_cd  ( cm_4_cd      ),
    .mode_hit ( mode_hit[4]  ),
    .mode_ack ( mode_ack[4]  )
    );

    mode_ack_gen mode_5 (
    .aclk     ( axis_aclk    ),
    .aresetn  ( axis_aresetn ),
    .enable   ( c_mode[5]    ),
    .strb     ( rCOIN_STRB   ),
    .coin     ( coin_mode_5  ),
    .coin_cd  ( cm_5_cd      ),
    .mode_hit ( mode_hit[5]  ),
    .mode_ack ( mode_ack[5]  )
    );
    
    mode_ack_gen mode_6 (
    .aclk     ( axis_aclk    ),
    .aresetn  ( axis_aresetn ),
    .enable   ( c_mode[6]    ),
    .strb     ( rCOIN_STRB   ),
    .coin     ( coin_mode_6  ),
    .coin_cd  ( cm_6_cd      ),
    .mode_hit ( mode_hit[6]  ),
    .mode_ack ( mode_ack[6]  )
    );

    mode_ack_gen mode_7 (
    .aclk     ( axis_aclk    ),
    .aresetn  ( axis_aresetn ),
    .enable   ( c_mode[7]    ),
    .strb     ( rCOIN_STRB   ),
    .coin     ( coin_mode_7  ),
    .coin_cd  ( cm_7_cd      ),
    .mode_hit ( mode_hit[7]  ),
    .mode_ack ( mode_ack[7]  )
    );

    mode_ack_gen mode_8 (
    .aclk     ( axis_aclk    ),
    .aresetn  ( axis_aresetn ),
    .enable   ( c_mode[8]    ),
    .strb     ( rCOIN_STRB   ),
    .coin     ( coin_mode_8  ),
    .coin_cd  ( cm_8_cd      ),
    .mode_hit ( mode_hit[8]  ),
    .mode_ack ( mode_ack[8]  )
    );

    mode_ack_gen mode_9 (
    .aclk     ( axis_aclk    ),
    .aresetn  ( axis_aresetn ),
    .enable   ( c_mode[9]    ),
    .strb     ( rCOIN_STRB   ),
    .coin     ( coin_mode_9  ),
    .coin_cd  ( cm_9_cd      ),
    .mode_hit ( mode_hit[9]  ),
    .mode_ack ( mode_ack[9]  )
    );
    
    mode_ack_gen mode_a (
    .aclk     ( axis_aclk    ),
    .aresetn  ( axis_aresetn ),
    .enable   ( c_mode[10]   ),
    .strb     ( rCOIN_STRB   ),
    .coin     ( coin_mode_a  ),
    .coin_cd  ( cm_a_cd      ),
    .mode_hit ( mode_hit[10] ),
    .mode_ack ( mode_ack[10] )
    );

    mode_ack_gen mode_b (
    .aclk     ( axis_aclk    ),
    .aresetn  ( axis_aresetn ),
    .enable   ( c_mode[11]   ),
    .strb     ( rCOIN_STRB   ),
    .coin     ( coin_mode_b  ),
    .coin_cd  ( cm_b_cd      ),
    .mode_hit ( mode_hit[11] ),
    .mode_ack ( mode_ack[11] )
    );

     mode_ack_gen mode_c (
    .aclk     ( axis_aclk    ),
    .aresetn  ( axis_aresetn ),
    .enable   ( c_mode[12]   ),
    .strb     ( rCOIN_STRB   ),
    .coin     ( coin_mode_c  ),
    .coin_cd  ( cm_c_cd      ),
    .mode_hit ( mode_hit[12] ),
    .mode_ack ( mode_ack[12] )
    );

    mode_ack_gen mode_d (
    .aclk     ( axis_aclk    ),
    .aresetn  ( axis_aresetn ),
    .enable   ( c_mode[13]   ),
    .strb     ( rCOIN_STRB   ),
    .coin     ( coin_mode_d  ),
    .coin_cd  ( cm_d_cd      ),
    .mode_hit ( mode_hit[13] ),
    .mode_ack ( mode_ack[13] )
    );

    mode_ack_gen mode_e (
    .aclk     ( axis_aclk    ),
    .aresetn  ( axis_aresetn ),
    .enable   ( c_mode[14]   ),
    .strb     ( rCOIN_STRB   ),
    .coin     ( coin_mode_e  ),
    .coin_cd  ( cm_e_cd      ),
    .mode_hit ( mode_hit[14] ),
    .mode_ack ( mode_ack[14] )
    );

     mode_ack_gen mode_f (
    .aclk     ( axis_aclk    ),
    .aresetn  ( axis_aresetn ),
    .enable   ( c_mode[15]   ),
    .strb     ( rCOIN_STRB   ),
    .coin     ( coin_mode_f  ),
    .coin_cd  ( cm_f_cd      ),
    .mode_hit ( mode_hit[15] ),
    .mode_ack ( mode_ack[15] )
    );
                   
    always @( posedge axis_aclk )
    begin
      if ( axis_aresetn == 1'b0 ) begin
        rWINDOW     <= 4'b0000;
        rCOIN_STRB  <= 1'b0;
        rCOIN_FLAG  <= 1'b0;
        rCOIN_WIDTH <= 8'h00;
      end
      else begin
        if ( rCOIN_FLAG == 1'b0 ) begin
          if ( czt_hit_coin == 1'b1 ) begin
            rWINDOW     <= 4'b0100;
            rCOIN_STRB  <= 1'b0;
            rCOIN_FLAG  <= 1'b1;
            rCOIN_WIDTH <= 8'b00000001;
          end
          else begin
            if ( sit_one_coin == 1'b1 ) begin
              rWINDOW     <= 4'b0010;
              rCOIN_STRB  <= 1'b0;
              rCOIN_FLAG  <= 1'b1;
              rCOIN_WIDTH <= sit_width;
            end
            else begin
              if ( csi_hit_coin == 1'b1 ) begin
                rWINDOW     <= 4'b1000;
                rCOIN_STRB  <= 1'b0;
                rCOIN_FLAG  <= 1'b1;
                rCOIN_WIDTH <= csi_width;
              end
              else begin
                if ( acd_hit_coin == 1'b1 ) begin
                  rWINDOW     <= 4'b0001;
                  rCOIN_STRB  <= 1'b0;
                  rCOIN_FLAG  <= 1'b1;
                  rCOIN_WIDTH <= acd_width;
                end
                else begin
                  rWINDOW     <= 4'b0000;
                  rCOIN_STRB  <= 1'b0;
                  rCOIN_FLAG  <= 1'b0;
                  rCOIN_WIDTH <= 8'h00;
                end
              end
            end
          end
        end
        else begin
          if ( rCOIN_WIDTH > iMargin ) begin
            rCOIN_STRB  <= 1'b0;
            rCOIN_FLAG  <= 1'b1;
            rCOIN_WIDTH <= rCOIN_WIDTH - 1'b1;
          end
          else begin
            rCOIN_STRB  <= 1'b1;
            rCOIN_WIDTH <= 8'h00;
            case (rWINDOW)
            4'b0001: if ( acd_hit_coin == 1'b1 ) begin
                       rCOIN_FLAG <= 1'b1;                      
                     end
                     else begin
                       rCOIN_FLAG <= 1'b0;
                     end
            4'b0010: if ( sit_one_coin == 1'b1 ) begin
                       rCOIN_FLAG <= 1'b1;
                     end
                     else begin
                       rCOIN_FLAG <= 1'b0;
                     end
            4'b0100: if ( czt_hit_coin == 1'b1 ) begin
                       rCOIN_FLAG <= 1'b1;
                     end
                     else begin
                       rCOIN_FLAG <= 1'b0;
                     end
            4'b1000: if ( csi_hit_coin == 1'b1 ) begin
                       rCOIN_FLAG <= 1'b1;
                     end
                     else begin
                       rCOIN_FLAG <= 1'b0;
                     end
            default: begin
                       rCOIN_FLAG <= 1'b0;
                     end
            endcase
          end
        end
      end
    end
    
endmodule
