`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/20/2019 11:57:51 AM
// Design Name: 
// Module Name: register_fmc
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module register_fmc(
    input  wire [31:0] register_3210,
    input  wire [31:0] register_7654,
    output wire [ 7:0] trg_ack_oe,
    output wire [ 7:0] evt_id_latch_oe,
    output wire [ 7:0] evt_id_data_oe,
    output wire [ 7:0] trg_ena_oe,    
    output wire [ 7:0] fee_hit_ie,
    output wire [ 7:0] fee_ready_ie,
    output wire [ 7:0] fee_busy_ie,
    output wire [ 7:0] fee_spare_ie
    );
    
    // connector[0]
    assign trg_ack_oe     [0] = register_3210[ 0];
    assign evt_id_latch_oe[0] = register_3210[ 1];
    assign evt_id_data_oe [0] = register_3210[ 2];
    assign trg_ena_oe     [0] = register_3210[ 3];
    assign fee_hit_ie     [0] = register_3210[ 4];
    assign fee_ready_ie   [0] = register_3210[ 5];
    assign fee_busy_ie    [0] = register_3210[ 6];
    assign fee_spare_ie   [0] = register_3210[ 7];

    // connector[1]
    assign trg_ack_oe     [1] = register_3210[ 8];
    assign evt_id_latch_oe[1] = register_3210[ 9];
    assign evt_id_data_oe [1] = register_3210[10];
    assign trg_ena_oe     [1] = register_3210[11];
    assign fee_hit_ie     [1] = register_3210[12];
    assign fee_ready_ie   [1] = register_3210[13];
    assign fee_busy_ie    [1] = register_3210[14];
    assign fee_spare_ie   [1] = register_3210[15];

    // connector[2]
    assign trg_ack_oe     [2] = register_3210[16];
    assign evt_id_latch_oe[2] = register_3210[17];
    assign evt_id_data_oe [2] = register_3210[18];
    assign trg_ena_oe     [2] = register_3210[19];
    assign fee_hit_ie     [2] = register_3210[20];
    assign fee_ready_ie   [2] = register_3210[21];
    assign fee_busy_ie    [2] = register_3210[22];
    assign fee_spare_ie   [2] = register_3210[23];

    // connector[3]
    assign trg_ack_oe     [3] = register_3210[24];
    assign evt_id_latch_oe[3] = register_3210[25];
    assign evt_id_data_oe [3] = register_3210[26];
    assign trg_ena_oe     [3] = register_3210[27];
    assign fee_hit_ie     [3] = register_3210[28];
    assign fee_ready_ie   [3] = register_3210[29];
    assign fee_busy_ie    [3] = register_3210[30];
    assign fee_spare_ie   [3] = register_3210[31];

    // connector[4]
    assign trg_ack_oe     [4] = register_7654[ 0];
    assign evt_id_latch_oe[4] = register_7654[ 1];
    assign evt_id_data_oe [4] = register_7654[ 2];
    assign trg_ena_oe     [4] = register_7654[ 3];
    assign fee_hit_ie     [4] = register_7654[ 4];
    assign fee_ready_ie   [4] = register_7654[ 5];
    assign fee_busy_ie    [4] = register_7654[ 6];
    assign fee_spare_ie   [4] = register_7654[ 7];

    // connector[5]
    assign trg_ack_oe     [5] = register_7654[ 8];
    assign evt_id_latch_oe[5] = register_7654[ 9];
    assign evt_id_data_oe [5] = register_7654[10];
    assign trg_ena_oe     [5] = register_7654[11];
    assign fee_hit_ie     [5] = register_7654[12];
    assign fee_ready_ie   [5] = register_7654[13];
    assign fee_busy_ie    [5] = register_7654[14];
    assign fee_spare_ie   [5] = register_7654[15];

    // connector[6]
    assign trg_ack_oe     [6] = register_7654[16];
    assign evt_id_latch_oe[6] = register_7654[17];
    assign evt_id_data_oe [6] = register_7654[18];
    assign trg_ena_oe     [6] = register_7654[19];
    assign fee_hit_ie     [6] = register_7654[20];
    assign fee_ready_ie   [6] = register_7654[21];
    assign fee_busy_ie    [6] = register_7654[22];
    assign fee_spare_ie   [6] = register_7654[23];

    // connector[7]
    assign trg_ack_oe     [7] = register_7654[24];
    assign evt_id_latch_oe[7] = register_7654[25];
    assign evt_id_data_oe [7] = register_7654[26];
    assign trg_ena_oe     [7] = register_7654[27];
    assign fee_hit_ie     [7] = register_7654[28];
    assign fee_ready_ie   [7] = register_7654[29];
    assign fee_busy_ie    [7] = register_7654[30];
    assign fee_spare_ie   [7] = register_7654[31];

endmodule
