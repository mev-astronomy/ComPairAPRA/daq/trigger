//Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2016.1 (win64) Build 1538259 Fri Apr  8 15:45:27 MDT 2016
//Date        : Thu Feb 03 10:08:31 2022
//Host        : gs66-adept-dell running 64-bit major release  (build 9200)
//Command     : generate_target Trigger_wrapper.bd
//Design      : Trigger_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module Trigger_wrapper
   (DDR_addr,
    DDR_ba,
    DDR_cas_n,
    DDR_ck_n,
    DDR_ck_p,
    DDR_cke,
    DDR_cs_n,
    DDR_dm,
    DDR_dq,
    DDR_dqs_n,
    DDR_dqs_p,
    DDR_odt,
    DDR_ras_n,
    DDR_reset_n,
    DDR_we_n,
    FIXED_IO_ddr_vrn,
    FIXED_IO_ddr_vrp,
    FIXED_IO_mio,
    FIXED_IO_ps_clk,
    FIXED_IO_ps_porb,
    FIXED_IO_ps_srstb,
    hpc_event_id_data,
    hpc_event_id_latch,
    hpc_fee_busy,
    hpc_fee_hit,
    hpc_fee_ready,
    hpc_fee_spare,
    hpc_lemo_in,
    hpc_lemo_out,
    hpc_trig_ack,
    hpc_trig_ena,
    led_out,
    lpc_event_id_data,
    lpc_event_id_latch,
    lpc_fee_busy,
    lpc_fee_hit,
    lpc_fee_ready,
    lpc_fee_spare,
    lpc_lemo_in,
    lpc_lemo_out,
    lpc_trig_ack,
    lpc_trig_ena,
    sw_reset,
    sw_trigger);
  inout [14:0]DDR_addr;
  inout [2:0]DDR_ba;
  inout DDR_cas_n;
  inout DDR_ck_n;
  inout DDR_ck_p;
  inout DDR_cke;
  inout DDR_cs_n;
  inout [3:0]DDR_dm;
  inout [31:0]DDR_dq;
  inout [3:0]DDR_dqs_n;
  inout [3:0]DDR_dqs_p;
  inout DDR_odt;
  inout DDR_ras_n;
  inout DDR_reset_n;
  inout DDR_we_n;
  inout FIXED_IO_ddr_vrn;
  inout FIXED_IO_ddr_vrp;
  inout [53:0]FIXED_IO_mio;
  inout FIXED_IO_ps_clk;
  inout FIXED_IO_ps_porb;
  inout FIXED_IO_ps_srstb;
  output [7:0]hpc_event_id_data;
  output [7:0]hpc_event_id_latch;
  input [7:0]hpc_fee_busy;
  input [7:0]hpc_fee_hit;
  input [7:0]hpc_fee_ready;
  input [7:0]hpc_fee_spare;
  input [1:0]hpc_lemo_in;
  output [1:0]hpc_lemo_out;
  output [7:0]hpc_trig_ack;
  output [7:0]hpc_trig_ena;
  output [3:0]led_out;
  output [7:0]lpc_event_id_data;
  output [7:0]lpc_event_id_latch;
  input [7:0]lpc_fee_busy;
  input [7:0]lpc_fee_hit;
  input [7:0]lpc_fee_ready;
  input [7:0]lpc_fee_spare;
  input [1:0]lpc_lemo_in;
  output [1:0]lpc_lemo_out;
  output [7:0]lpc_trig_ack;
  output [7:0]lpc_trig_ena;
  input sw_reset;
  input sw_trigger;

  wire [14:0]DDR_addr;
  wire [2:0]DDR_ba;
  wire DDR_cas_n;
  wire DDR_ck_n;
  wire DDR_ck_p;
  wire DDR_cke;
  wire DDR_cs_n;
  wire [3:0]DDR_dm;
  wire [31:0]DDR_dq;
  wire [3:0]DDR_dqs_n;
  wire [3:0]DDR_dqs_p;
  wire DDR_odt;
  wire DDR_ras_n;
  wire DDR_reset_n;
  wire DDR_we_n;
  wire FIXED_IO_ddr_vrn;
  wire FIXED_IO_ddr_vrp;
  wire [53:0]FIXED_IO_mio;
  wire FIXED_IO_ps_clk;
  wire FIXED_IO_ps_porb;
  wire FIXED_IO_ps_srstb;
  wire [7:0]hpc_event_id_data;
  wire [7:0]hpc_event_id_latch;
  wire [7:0]hpc_fee_busy;
  wire [7:0]hpc_fee_hit;
  wire [7:0]hpc_fee_ready;
  wire [7:0]hpc_fee_spare;
  wire [1:0]hpc_lemo_in;
  wire [1:0]hpc_lemo_out;
  wire [7:0]hpc_trig_ack;
  wire [7:0]hpc_trig_ena;
  wire [3:0]led_out;
  wire [7:0]lpc_event_id_data;
  wire [7:0]lpc_event_id_latch;
  wire [7:0]lpc_fee_busy;
  wire [7:0]lpc_fee_hit;
  wire [7:0]lpc_fee_ready;
  wire [7:0]lpc_fee_spare;
  wire [1:0]lpc_lemo_in;
  wire [1:0]lpc_lemo_out;
  wire [7:0]lpc_trig_ack;
  wire [7:0]lpc_trig_ena;
  wire sw_reset;
  wire sw_trigger;

  Trigger Trigger_i
       (.DDR_addr(DDR_addr),
        .DDR_ba(DDR_ba),
        .DDR_cas_n(DDR_cas_n),
        .DDR_ck_n(DDR_ck_n),
        .DDR_ck_p(DDR_ck_p),
        .DDR_cke(DDR_cke),
        .DDR_cs_n(DDR_cs_n),
        .DDR_dm(DDR_dm),
        .DDR_dq(DDR_dq),
        .DDR_dqs_n(DDR_dqs_n),
        .DDR_dqs_p(DDR_dqs_p),
        .DDR_odt(DDR_odt),
        .DDR_ras_n(DDR_ras_n),
        .DDR_reset_n(DDR_reset_n),
        .DDR_we_n(DDR_we_n),
        .FIXED_IO_ddr_vrn(FIXED_IO_ddr_vrn),
        .FIXED_IO_ddr_vrp(FIXED_IO_ddr_vrp),
        .FIXED_IO_mio(FIXED_IO_mio),
        .FIXED_IO_ps_clk(FIXED_IO_ps_clk),
        .FIXED_IO_ps_porb(FIXED_IO_ps_porb),
        .FIXED_IO_ps_srstb(FIXED_IO_ps_srstb),
        .hpc_event_id_data(hpc_event_id_data),
        .hpc_event_id_latch(hpc_event_id_latch),
        .hpc_fee_busy(hpc_fee_busy),
        .hpc_fee_hit(hpc_fee_hit),
        .hpc_fee_ready(hpc_fee_ready),
        .hpc_fee_spare(hpc_fee_spare),
        .hpc_lemo_in(hpc_lemo_in),
        .hpc_lemo_out(hpc_lemo_out),
        .hpc_trig_ack(hpc_trig_ack),
        .hpc_trig_ena(hpc_trig_ena),
        .led_out(led_out),
        .lpc_event_id_data(lpc_event_id_data),
        .lpc_event_id_latch(lpc_event_id_latch),
        .lpc_fee_busy(lpc_fee_busy),
        .lpc_fee_hit(lpc_fee_hit),
        .lpc_fee_ready(lpc_fee_ready),
        .lpc_fee_spare(lpc_fee_spare),
        .lpc_lemo_in(lpc_lemo_in),
        .lpc_lemo_out(lpc_lemo_out),
        .lpc_trig_ack(lpc_trig_ack),
        .lpc_trig_ena(lpc_trig_ena),
        .sw_reset(sw_reset),
        .sw_trigger(sw_trigger));
endmodule
