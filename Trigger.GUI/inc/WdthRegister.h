#ifndef _WDTH_REGISTER_H
#define _WDTH_REGISTER_H

#include "BaseRegister.h"
#include "RegNumberEntry.h"

class TGNumberEntry;
class TGString;

class WdthRegister : public BaseRegister {
 public:
  
 protected:
  RegNumberEntry *fEntry;
  
 public:
  WdthRegister( const TGWindow *p, TGString *title, UInt_t options );
  virtual ~WdthRegister();

  virtual void Init( void );

  ClassDef( WdthRegister, 0 )
};

#endif
