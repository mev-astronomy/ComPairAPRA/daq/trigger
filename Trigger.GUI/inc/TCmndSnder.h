#ifndef _T_CMND_SNDER_H
#define _T_CMND_SNDER_H

#include "TQObject.h"

#include "TcpSocket.h"

class TCmndSnder : public TcpSocket, public TQObject {
 public:
  enum ENums { N_TRY = 5, BUFS = 256 };

 protected:

 public:
  TCmndSnder( int m );
  virtual ~TCmndSnder();

  void AddEcho( const char * ); // *SIGNAL*
  void AddText( const char * ); // *SIGNAL*

  void Init        ( void );
  void Connect     ( void );
  Bool_t Connect   ( const char *signal, const char *receiver_class, void *receiver, const char *slog );

  bool SendCommand ( struct CmndPacket *cmdPacket );
  void Acknowledge ( struct CmndPacket *cmdPacket );

  const char *ParseCommand( struct CmndPacket *cmdPacket );

  ClassDef( TCmndSnder, 0 )
};

extern TCmndSnder *gCmndSnder;

#endif
