#ifndef _CONN_REGISTER_H
#define _CONN_REGISTER_H

#include "BaseRegister.h"
#include "RegCheckButton.h"

class TGCheckButton;
class TGString;

class ConnRegister : public BaseRegister {
 public:
  enum ENums { N_ITEM = 8 };
  
 protected:
  RegCheckButton *fConn;
  
 public:
  ConnRegister( const TGWindow *p, TGString *title, UInt_t options );
  virtual ~ConnRegister();

  virtual void Init( void );

  ClassDef( ConnRegister, 0 )
};

#endif
