#ifndef _REG_RADIO_BUTTON_H
#define _REG_RADIO_BUTTON_H

#include "RegObject.h"

class TGRadioButton;

class RegRadioButton : public RegObject {
 public:
  enum ENums { N_MAX = 16 };

 protected:
  UInt_t fNbutton;
  TGRadioButton *fButton[N_MAX];

 public:
  RegRadioButton( unsigned char addr );
  virtual ~RegRadioButton();

  void SetNumOfButton( UInt_t num ) { fNbutton = num; }

  void SetButton( UInt_t i, TGRadioButton *cb );
  TGRadioButton *GetButton( UInt_t i );

  virtual void Scan( void );
  virtual void Show( void );

 protected:
  ClassDef( RegRadioButton, 0 )
};

#endif
