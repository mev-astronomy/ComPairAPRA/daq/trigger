#ifndef _REG_COMBO_BOX_H
#define _REG_COMBO_BOX_H

#include "RegObject.h"

class TGComboBox;

class RegComboBox : public RegObject {
 public:
  enum ENums { N_ITEM = 8 };
  enum EType { T_ONEH = 0, T_BINA = 1 }; // one-hot or binary

 protected:
  TGComboBox *fBox;
  Int_t fCtype;

 public:
  RegComboBox( unsigned char addr );
  RegComboBox( unsigned char addr, Int_t type );
  virtual ~RegComboBox();

  void SetComboBox( TGComboBox *cb );
  TGComboBox *GetComboBox( void );

  virtual void Scan( void );
  virtual void Show( void );

 protected:
  ClassDef( RegComboBox, 0 )
};

#endif
