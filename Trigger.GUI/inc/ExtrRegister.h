#ifndef _EXTR_REGISTER_H
#define _EXTR_REGISTER_H

#include "BaseRegister.h"
#include "RegComboBox.h"

class TGComboBox;
class TGString;

class ExtrRegister : public BaseRegister {
 public:
  enum ENums { N_ITEM = 8 };
  
 protected:
  RegComboBox    *fRough;
  RegComboBox    *fRfine;
  
 public:
  ExtrRegister( const TGWindow *p, TGString *title, UInt_t options );
  virtual ~ExtrRegister();

  virtual void Init( void );

  ClassDef( ExtrRegister, 0 )
};

#endif
