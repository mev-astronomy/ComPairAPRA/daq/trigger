#ifndef _T_DATA_RCVER_H
#define _T_DATA_RCVER_H

#include "TcpSocket.h"
#include "Pthreading.h"
#include "EvntBranch.h"

#include "TQObject.h"

#include "TTree.h"
#include "TFile.h"

#ifdef TCP_RCVER
#undef TCP_RCVER
#endif

#define TCP_RCVER

class HistBranch;
class EvntBranch;
class TGHProgressBar;

class TDataRcver : public Pthreading, public TQObject {
 public:
  enum ENums { N_TRY = 5, N_HEAD = 10, N_SENT = 256, BUFS = 256, N_TOTL = 256*1024*1024 };

 protected:
  unsigned long  fNrcv;

#ifdef TCP_RCVER
  TcpSocket     *fTcps;
#else
  UdpSocket     *fTcps;
#endif

  TTree         *fTree;
  TFile         *fFile;
  int            fDout;
  ULong64_t      fTotl;

  Double_t       fTime;
  EvntBranch    *fEvnt;
  HistBranch    *fHist;

  int            fHsel;

  DataHeader     fHead;

 public:
  TDataRcver();
  virtual ~TDataRcver();

  // access functions
  unsigned long GetNrcv( void ) const { return fNrcv; }
  unsigned int  GetEvID( void ) const { return fEvnt->GetEventID(); }

  // methods
  void Init     ( void );
  void Connect  ( void );

  void Process  ( void );
  void DrawHist ( int i );
  void DrawZoom ( void ) { DrawHist( fHsel ); }

  void Load     ( const char *fname, TGHProgressBar *fHProg );
  void Open     ( void );
  void Close    ( void );
  void Clear    ( void );
  void Write    ( void );
  void FillData ( void );
  void SetHsel  ( int i ) { fHsel = i; }

  bool IsConnected  ( void ) { return fTcps->IsConnected(); }
  void OpenRootFile ( void );
  void SaveRootFile ( void );
  void ClseRootFile ( void );
  void ChngRootFile ( void );

  void OpenDataFile ( void );
  void SaveDataFile ( void );
  void ClseDataFile ( void );
  void ChngDataFile ( void );

  void PrintStatus  ( void );

  Double_t    GetTime( void ) const { return fTime; }
  EvntBranch *GetEvnt( void ) const { return fEvnt; }
  HistBranch *GetHist( void ) const { return fHist; }

  void SetTime( Double_t time ) { fTime = time; }

 protected:
  unsigned char *GetHeader();
  virtual void RunThread( void );

  ClassDef( TDataRcver, 0 )
};

extern TDataRcver *gDataRcver;

#endif
