#ifndef _REG_NUMBER_ENTRY_H
#define _REG_NUMBER_ENTRY_H

#include "RegObject.h"

class TGNumberEntry;

class RegNumberEntry : public RegObject {
 public:
  enum ENums { N_MAX = 16 };
  
 protected:
  TGNumberEntry *fEntry;
    
 public:
   RegNumberEntry( unsigned char addr );
   virtual ~RegNumberEntry();
 
   void SetNumberEntry( TGNumberEntry *ne );
   TGNumberEntry *GetNumberEntry( void );
   
   virtual void Scan( void );
   virtual void Show( void );
   
 protected:
   ClassDef( RegNumberEntry, 0 )
};

#endif
