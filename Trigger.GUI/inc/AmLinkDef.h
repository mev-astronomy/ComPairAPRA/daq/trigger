#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class AmMainFrame+;
#pragma link C++ class BaseRegister+;
#pragma link C++ class ModeRegister+;
#pragma link C++ class ConnRegister+;
#pragma link C++ class LemoRegister+;
#pragma link C++ class GppsRegister+;
#pragma link C++ class ExtrRegister+;
#pragma link C++ class EvidRegister+;
#pragma link C++ class WdthRegister+;
#pragma link C++ class ChanRegister+;
#pragma link C++ class RegObject+;
#pragma link C++ class RegEnable+;
#pragma link C++ class RegRadioButton+;
#pragma link C++ class RegCheckButton+;
#pragma link C++ class RegNumberEntry+;
#pragma link C++ class RegComboBox+;
#pragma link C++ class TCmndSnder+;
#pragma link C++ class TDataRcver+;
#pragma link C++ class EvntBranch+;
#pragma link C++ class HistBranch+;

#endif

