#ifndef _CMD_HANDLER_H
#define _CMD_HANDLER_H

#define REGISTER_TABLE "dat/assign.dat"

#include <map>
using namespace std;

class CmdHandler {
 public:
  enum ENums { N_COMMBIT = 8, N_ADDRBIT = 8, N_DATABIT = 16 };

 private:
  std::map<int, struct regAttr> regs;
  std::map<int, unsigned int> vals;

 public:
  CmdHandler();
  virtual ~CmdHandler();

  std::map<int, unsigned int> GetValMap( void ) const { return vals; }
  void ReadTable ( void );
  void SetRegAttr( int addr, struct regAttr  reg );
  void GetRegAttr( int addr, struct regAttr *reg );
  unsigned int GetRegValue( int addr );

 private:
  unsigned int BtoI( char *val, unsigned int msb, unsigned int lsb );
};

#endif
