#ifndef _TCP_SOCKET_H
#define _TCP_SOCKET_H

#include <netinet/in.h>
#include "Packet.h"
#include "Pthreading.h"

class TcpSocket : public Pthreading {
 public:
  enum ENums{ SRV = 0, CLT = 1 };
  
 protected:
  int mode;
  int sock[2];
  struct sockaddr_in ownaddr;
  struct sockaddr_in hisaddr;

  unsigned char *rcvBuf;
  unsigned int   rcvBufLen;
  unsigned int   rcvLen;

  bool connected;

  unsigned long ev_tot;

 public:
  TcpSocket( int m );
  virtual ~TcpSocket();
  
  // access functions

  // methods
  int   CreateBuffer ( unsigned int bufferLen );
  unsigned char *GetBuffer    ( void ) const { return rcvBuf; }
  unsigned int   GetRcvLen    ( void ) const { return rcvLen; }

  bool  IsConnected  ( void ) const { return connected; }
  int   CreateSocket ( void );
  int   BindSocket   ( void );
  int   ConnectSocket( void );
  int   Listen       ( int backlog );
  int   Accept       ( void );
  void  CloseSocket  ( void );
  int   SetTimeOut   ( int seconds );
  int   SetOwnAddr   ( unsigned short port, const char *ip );
  int   SetHisAddr   ( unsigned short port, const char *ip );
  int   Send         ( void *buffer, unsigned int bufferLen );
  int   Recv         ( void *buffer, unsigned int bufferLen );
  void  PrintStatus  ( void );

  unsigned char myItoB( unsigned int   i, unsigned int n );
  unsigned int  myBtoI( unsigned char *b, unsigned int n );

  void Clear( void );
  void Print( void );
  
 protected:
  virtual void RunThread( void );
};

inline unsigned char TcpSocket::myItoB( unsigned int i, unsigned int n )
{
  return (i>>(8*n) & 0xff);
}

inline unsigned int TcpSocket::myBtoI( unsigned char *b, unsigned int n )
{
  unsigned int i = 0;
  for ( unsigned int j = 0; j < n; j++ ) i += (b[j]<<(8*j));
  return i;
}


#endif
