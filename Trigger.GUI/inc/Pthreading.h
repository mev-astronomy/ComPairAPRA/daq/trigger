/* $Id: Pthreading.h,v 1.8 2008/03/11 11:01:24 kusumoto Exp $ */
#ifndef _PTHREADING_H
#define _PTHREADING_H

#include <string>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/syscall.h>

using namespace std;

class Pthreading {

private:
  static pthread_mutex_t boot_lock;
  static void* Boot( void *addr );

protected:
  int status;
  int proc_id;
  string thread_name;

protected:
  Pthreading( const char *thre_name = "Pthreading" );
  virtual ~Pthreading();

  virtual void RunThread( void ) = 0;

public:
  void SetStatus( int s ) { status = s; }
  int  GetStatus( void ) const { return status;  }
  int  GetPid   ( void ) const { return proc_id; }
  long GetTid   ( void ) const { return (long)syscall(SYS_gettid); }
  const char *GetThrName( void ) const { return thread_name.c_str(); }
  void StartThread( void );
};

#endif

// Local Variables:
// mode:C++
// End:
