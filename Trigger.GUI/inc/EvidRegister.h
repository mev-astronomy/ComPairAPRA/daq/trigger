#ifndef _EVID_REGISTER_H
#define _EVID_REGISTER_H

#include "BaseRegister.h"
#include "RegNumberEntry.h"

class TGNumberEntry;
class TGString;

class EvidRegister : public BaseRegister {
 public:
  
 protected:
  RegNumberEntry *fDelay;
  RegNumberEntry *fWidth;
  
 public:
  EvidRegister( const TGWindow *p, TGString *title, UInt_t options );
  virtual ~EvidRegister();

  virtual void Init( void );

  ClassDef( EvidRegister, 0 )
};

#endif
