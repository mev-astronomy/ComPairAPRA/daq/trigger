#ifndef _CHAN_REGISTER_H
#define _CHAN_REGISTER_H

#include "BaseRegister.h"
#include "RegComboBox.h"

class TGComboBox;

class ChanRegister : public BaseRegister {
 public:
  enum ENums { N_CONN = 16, N_CHAN = 24 };

 protected:
  RegComboBox *fConn;
  RegComboBox *fChan;
  
 public:
  ChanRegister( const TGWindow *p, TGString *title, UInt_t options );
  virtual ~ChanRegister();

  virtual void Init( void );

  ClassDef( ChanRegister, 0 )
};

#endif
