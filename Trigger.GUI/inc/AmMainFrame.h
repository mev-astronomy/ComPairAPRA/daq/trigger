#ifndef _AM_MAIN_FRAME_H
#define _AM_MAIN_FRAME_H

#include "Packet.h"

#include "TGFrame.h"

class TCmndSnder;
class CmdHandler;
class TDataRcver;

class RegEnable;

class ConnRegister;
class LemoRegister;
class GppsRegister;
class ModeRegister;
class ExtrRegister;
class EvidRegister;
class WdthRegister;
class ChanRegister;

class TGTab;
class TGCheckButton;
class TGNumberEntry;
class TGLabel;
class TGComboBox;
class TGRadioButton;
class TRootEmbeddedCanvas;
class TObjArray;
class TTimer;
class TGTextView;
class TGTextEntry;

class TGFileContainer;
class TGTransientFrame;
class TGLVEntry;

class AmMainFrame : public TGMainFrame {
 public:
  enum ENums { CMND = 0, EVNT = 1, N_SUBD = 4, N_CHAN = 4, N_CONN = 16, N_MODE = 16, N_RAW = 3, N_COL = 4, N_HIST = 12 };

 private:
  Bool_t        fTrig;
  Bool_t        fClose;

  unsigned long fNprcv;
  unsigned int  fNzero;

  TTimer       *fTimr;

  TGTab        *fAtab;

  TGTextView   *fVcom;
  TGTextView   *fVech;

  TCmndSnder   *fCmnd;
  CmdHandler   *fChnd;

  TDataRcver   *fData;

  TGTextEntry  *fEvid[2];
  TGTextEntry  *fEvid_hex[2];

  TRootEmbeddedCanvas *fZcan;
  TObjArray    *fCarray;
  
  ModeRegister *fMode;
  ConnRegister *fConn[N_CONN];
  LemoRegister *fLemo;
  GppsRegister *fGpps;
  ExtrRegister *fExtr;
  EvidRegister *fPuls;
  WdthRegister *fWdth[N_SUBD+2];
  ChanRegister *fChan[N_CHAN];

  TGTextButton *fCnct_Cmnd;
  TGTextButton *fCnct_Data;

  TGTextButton *fLoad;
  TGTextButton *fOpen;
  TGTextButton *fClse;

  TGTextButton *fHsel[N_HIST];
  TGTextButton *fZsel[N_HIST];

  RegEnable    *fCtrl;
  TGTextButton *fEnbl;
  TGTextButton *fDsbl;

  TGFileContainer  *fContents;
  TGTransientFrame *fMain;

  struct CmndPacket cmdPacket;

 public:
  AmMainFrame( const TGWindow *p, UInt_t w, UInt_t h );
  virtual ~AmMainFrame();

  void AddCmnd( const char *cmnd );
  void AddEcho( const char *echo );

  void Connect_Cmnd ( void );
  void Connect_Data ( void );
  void Load    ( void );
  void Echo    ( void );
  void Fifo    ( void );
  void Status  ( void );
  void Reset   ( void );
  void Enable  ( void );
  void Disable ( void );

  // slots
  void DoClose    ( void );
  void CloseWindow( void  );

  //  void OnDoubleClick( TGLVEntry*, Int_t );
  //  void DoMenu       ( Int_t );

  // other
  //  void DisplayFile     ( const TString &fname );
  //  void DisplayDirectory( const TString &fname );
  //  void DisplayObject   ( const TString& fname, const TString& name );

  void RegLoad ( void );
  void Set     ( void );
  void Get     ( void );

  void Open    ( void );
  void Close   ( void );
  virtual void Clear   ( Option_t *opt );
  void HistSel ( Int_t i );

  void Selected( Int_t i );
  void AutoRefresh( void );

 protected:
  void ExecuteButtColumn( const TGWindow *p );
  void CommandEchoColumn( const TGWindow *p );
  void ExecuteCmndColumn( TGCompositeFrame *p );
  void ExecuteDataColumn( TGCompositeFrame *p, Int_t i );
  void CoincidenceColumn( TGCompositeFrame *p );
  void ExtTrigRateColumn( TGCompositeFrame *p );
  void ConnectorLPColumn( TGCompositeFrame *p );
  void ConnectorHPColumn( TGCompositeFrame *p );
  void ConnectorLemolumn( TGCompositeFrame *p );
  void GpsPPSselecColumn( TGCompositeFrame *p );
  void CoinciWidthColumn( TGCompositeFrame *p );
  void ChannSourceColumn( TGCompositeFrame *p );

  void DrawEventHists( void );
  void PrintStatus   ( void );
  void PrintEventID  ( void );

  ClassDef(AmMainFrame,0)
};

#endif
