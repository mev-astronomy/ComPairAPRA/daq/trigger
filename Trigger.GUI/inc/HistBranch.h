#ifndef _HIST_BRANCH_H
#define _HIST_BRANCH_H

#include "EvntBranch.h"

#include "TObject.h"

class TGraph;
class TH1F;
class TH2F;

class HistBranch : public TObject {
 public:
  enum ENums{ N_SITR = 12, N_MODE = 16, N_HIST = 12 };

 protected:
  EvntBranch fEvnt;

  Double_t fYmax[N_HIST];

  Double_t fTime;
  Double_t fPeriod;

  TGraph  *fLiveTM;
  TGraph  *fTrgLck;
  TGraph  *fTrgAck;
  TGraph  *fSwtAck;
  TGraph  *fExtAck;
  TGraph  *fLpcAck;
  TGraph  *fHpcAck;
  TGraph  *fCoMode[N_MODE];
  TGraph  *fCdMode[N_MODE];
  TGraph  *fLivACD;
  TGraph  *fLivCZT;
  TGraph  *fLivCs1;
  TGraph  *fLivCs2;
  TGraph  *fLivSiT[N_SITR];
  TGraph  *fHitACD;
  TGraph  *fOneSiT;
  TGraph  *fAndSiT;
  TGraph  *fTwoSiT;
  TGraph  *fHitCZT;
  TGraph  *f1stCsI;
  TGraph  *f2ndCsI;
  TGraph  *fHitXsi[N_SITR];
  TGraph  *fHitYsi[N_SITR];
  TH1F    *fAllTrg;
  TH1F    *fAllHit;

  TH2F    *fFrame[N_HIST];

 public:
  HistBranch();
  virtual ~HistBranch();

  void SetHist ( unsigned int *data );
  void Clear   ( Option_t *option = "" );
  void DrawHist( int i );

 protected:
  void CreateHistTrg( void );
  void CreateHistHit( void );

  void SetFrame ( TGraph *gra, int i );
  void SetHistX ( TGraph *gra );
  void SetHistY ( TGraph *gra, int i );

  void SetGraph ( TGraph *gra, Int_t i = 0 );

  void SetLiveTM( void );
  void SetTrgLck( void );
  void SetTrgAck( void );
  void SetSwtAck( void );
  void SetExtAck( void );
  void SetLpcAck( void );
  void SetHpcAck( void );
  void SetCoMode( int i );
  void SetCdMode( int i );

  void SetLivACD( void );
  void SetLivCZT( void );
  void SetLivCs1( void );
  void SetLivCs2( void );
  void SetLivSiT( int i );

  void SetHitACD( void );
  void SetOneSiT( void );
  void SetAndSiT( void );
  void SetTwoSiT( void );
  void SetHitCZT( void );
  void Set1stCsI( void );
  void Set2ndCsI( void );
  void SetHitXsi( int i );
  void SetHitYsi( int i );
  void SetAllTrg( void );
  void SetAllHit( void );

  void DrawLiveTM( void );
  void DrawTrgAck( void );
  void DrawTrgAll( void );
  void DrawLivDet( void );

  void DrawHitACD( void );
  void DrawHitSiT( void );
  void DrawHitCZT( void );
  void DrawHitCsI( void );

  void DrawHitXsi( void );
  void DrawHitYsi( void );
  void DrawAllTrg( void );
  void DrawAllHit( void );

  ClassDef( HistBranch, 1 )
};

#endif
