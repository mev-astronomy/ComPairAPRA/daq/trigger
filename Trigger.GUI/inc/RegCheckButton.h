#ifndef _REG_CHECK_BUTTON_H
#define _REG_CHECK_BUTTON_H

#include "RegObject.h"

class TGCheckButton;

class RegCheckButton : public RegObject {
 public:
  enum ENums { N_MAX = 16 };

 protected:
  UInt_t fNbutton;
  TGCheckButton *fButton[N_MAX];

 public:
  RegCheckButton( unsigned char addr );
  virtual ~RegCheckButton();

  void SetNumOfButton( UInt_t num ) { fNbutton = num; }

  void SetButton( UInt_t i, TGCheckButton *cb );
  TGCheckButton *GetButton( UInt_t i );

  virtual void Scan( void );
  virtual void Show( void );

 protected:
  ClassDef( RegCheckButton, 0 )
};

#endif
