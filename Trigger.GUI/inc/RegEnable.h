#ifndef _REG_ENABLE_H
#define _REG_ENABLE_H

#include "Packet.h"

#include "RegObject.h"

class RegEnable : public RegObject {
 public:

 protected:

 public:
  RegEnable( unsigned char addr );
  virtual ~RegEnable();

  virtual void Scan( void );
  virtual void Show( void );

 protected:
  ClassDef( RegEnable, 0 )
};

#endif
