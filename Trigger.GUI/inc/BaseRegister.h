#ifndef _BASE_REGISTER_H
#define _BASE_REGISTER_H

#include "RegHandler.h"
#include "Packet.h"
#include "RegObject.h"

#include "TGFrame.h"
#include "TGIcon.h"
#include "TGButton.h"
#include "TObjArray.h"

#include <map>

class TGIcon;
class TGTextButton;

class BaseRegister : public TGGroupFrame {
 public:
  enum EAtrb{ kOFF = 1, kON = 2, kSIZE = 8 };

 protected:
  TObjArray      *fArr;

  TGIcon         *fLED;
  TGTextButton   *fGet;
  TGTextButton   *fSet;

  bool fSync;

 public:
  BaseRegister( const TGWindow *p, TGString *title, UInt_t options );
  virtual ~BaseRegister();

  void   Status ( void );
  void   Get    ( void );
  void   Set    ( void );
  void   Add    ( RegObject *reg );
  void   Load   ( std::map<int, unsigned int> vals );

 protected:
  virtual void Init( void ) = 0;

  ClassDef( BaseRegister, 0 )
};

#endif
