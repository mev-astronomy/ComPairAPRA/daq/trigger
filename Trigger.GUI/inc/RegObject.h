#ifndef _REG_OBJECT_H
#define _REG_OBJECT_H

#include "Packet.h"

#include "TObject.h"

class RegObject : public TObject {
 public:
  enum ENums { N_MAX = 8 };

 protected:
  struct CmndPacket cmdPacket;

 public:
  RegObject( unsigned char addr );
  virtual ~RegObject();

  unsigned int GetAddress( void );
  unsigned int GetValue  ( void );

  void SetValue   ( unsigned int  val  );
  void SendCommand( unsigned char cmnd );

  virtual void Scan( void ) = 0;
  virtual void Show( void ) = 0;

 protected:
  ClassDef( RegObject, 0 )
};

#endif
