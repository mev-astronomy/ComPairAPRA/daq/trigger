#ifndef _REG_HANDLER_H
#define _REG_HANDLER_H

#include <string>
#include <unistd.h>

using namespace std;

class RegHandler {
 public:
  enum ERegIDs{ EVNUM = 0x0, CTROL = 0x1, CDMOD = 0x2, CTDW1 = 0x3, 
		CTDW2 = 0x4, CTDW3 = 0x5, CTDW4 = 0x6, WIDTH = 0x7,
		INLPC = 0x8, OTLPC = 0x9, CHLPC = 0xa, INHPC = 0xb,
		OTHPC = 0xc, CHHPC = 0xd, SPARE = 0xe, EVTRA = 0xf };

  enum ENums { MAX_ID = 0x10, NUM_TR = 0x20 };

 private:
  int  file_desc;
  int  currentID;
  char param [4];

 public:
  RegHandler();
  virtual ~RegHandler();

  // access function
  int  GetCurrentID( void ) const { return currentID; }

  // methods
  void InitDevice  ( void );
  void CloseDevice ( void );
  void SetRegValue ( struct regAttr reg, unsigned int  val );
  void GetRegValue ( struct regAttr reg, unsigned int *val );
  void TriggerReset   ( void );
  void TriggerEnable  ( void );
  void TriggerDisable ( void );
  void SetTransaction ( void );
  unsigned int IsEvent( void );

 private:
  bool IsOpen( void );
  int ioctl_SELECT  ( void );
  int ioctl_WRITE   ( void );
  int ioctl_READ    ( void );
  int SetRegisterID ( int id  );

  void SetValue( struct regAttr reg, unsigned int  val );
  void GetValue( struct regAttr reg, unsigned int *val );
};

struct regAttr {
  int id;
  unsigned int msb;
  unsigned int lsb;
};

#endif
