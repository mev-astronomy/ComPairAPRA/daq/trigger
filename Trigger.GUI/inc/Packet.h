#ifndef _PACKET_H
#define _PACKET_H

#define LENOVO
//#undef LENOVO

#ifdef LENOVO
#define SRVIP "192.168.1.10"
#define CLTIP "192.168.1.11"
#define IP_BASE "192.168.1.10"
#else
#define SRVIP "10.10.0.140"
#define CLTIP "10.10.0.122"
#define IP_BASE "10.10.0.140"
#endif

#define TCP_PORT 1081
#define TCP_CMND_PORT 1082
#define TCP_DATA_PORT 1083
#define UDP_PORT 4660
#define UDP_CMND_PORT 4662
#define UDP_DATA_PORT 4663
#define BUFSIZE 2048
#define TIMEOUT 60
#define MAXLOG  20


#define PKT_ATRB_SIZE 2
#define PKT_TIME_SIZE 8

struct DataHeader
{
  unsigned char atrb[PKT_ATRB_SIZE];
  unsigned char time[PKT_TIME_SIZE];
};

struct CmndPacket
{
  unsigned char cmnd;    // command
  unsigned char addr;    // Address
  unsigned char data[2]; // data
};

#define CMD_ECHO 0x00
#define CMD_REST 0x01
#define CMD_STRT 0x02
#define CMD_STOP 0x04
#define CMD_READ 0x08
#define CMD_WRTE 0x10
#define CMD_STAT 0x40

#define PKT_EVNM_SIZE 4
#define PKT_NOFC_SIZE 8

#define MSG_MAX_LEN 255

struct MssgPacket
{
  unsigned int att;       // packet attribute
  char body[MSG_MAX_LEN];  // message body
};

#define ATT_MSSG 0x01
#define ATT_DATA 0x02
#define ATT_EXIT 0x06

#endif
