#ifndef _MODE_REGISTER_H
#define _MODE_REGISTER_H

#include "BaseRegister.h"
#include "RegRadioButton.h"
#include "RegCheckButton.h"
#include "RegNumberEntry.h"

class TGRadioButton;
class TGCheckButton;
class TGNumberEntry;
class TGString;

class ModeRegister : public BaseRegister {
 public:
  enum ENums { N_SELS = 3, N_MODE = 16 };

 protected:
  RegRadioButton *fSels;
  RegCheckButton *fMode;
  RegNumberEntry *fNumb[N_MODE];

 public:
  ModeRegister( const TGWindow *p, TGString *title, UInt_t options );
  virtual ~ModeRegister();

  virtual void Init( void );

  ClassDef( ModeRegister, 0 )
};

#endif
