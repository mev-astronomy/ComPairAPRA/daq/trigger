#ifndef _EVNT_BRANCH_H
#define _EVNT_BRANCH_H

#include "TObject.h"

class EvntBranch : public TObject {
 public:
  enum ENums { N_SITR = 12, N_MODE = 16, N_DATA = 64 };

 protected:
  unsigned int fEvtPrev[N_DATA];

  unsigned int fEventID;
  unsigned int fNtrgLck;
  unsigned int fNclkPps;
  unsigned int fNclkRaw;
  unsigned int fNclkEna;
  unsigned int fNclkLiv;
  unsigned int fNgpsPPS;
  unsigned int fCoHitRw;
  unsigned int fCoHitCD;
  unsigned int fHitPatt;
  //  unsigned int fTsecond;
  //  unsigned int fTmicros;
  unsigned int fNtrgAck;
  unsigned int fNswtAck;
  unsigned int fNextAck;
  unsigned int fNlpcAck;
  unsigned int fNhpcAck;
  unsigned int fNcoMode[N_MODE];
  unsigned int fNcdMode[N_MODE];
  unsigned int fNclkACD;
  unsigned int fNclkCZT;
  unsigned int fNclkCs1;
  unsigned int fNclkCs2;
  unsigned int fNclkSiT[N_SITR];
  unsigned int fNhitACD;
  unsigned int fNoneSiT;
  unsigned int fNandSiT;
  unsigned int fNtwoSiT;
  unsigned int fNhitCZT;
  unsigned int fN1stCsI;
  unsigned int fN2ndCsI;
  unsigned int fNhitXsi[N_SITR];
  unsigned int fNhitYsi[N_SITR];

 public:
  EvntBranch();
  virtual ~EvntBranch();

  // access functions
  unsigned int GetEventID( void ) const { return fEventID; }
  unsigned int GetNtrgLck( void ) const { return fNtrgLck; }
  unsigned int GetNclkPps( void ) const { return fNclkPps; }
  unsigned int GetNclkRaw( void ) const { return fNclkRaw; }
  unsigned int GetNclkEna( void ) const { return fNclkEna; }
  unsigned int GetNclkLiv( void ) const { return fNclkLiv; }
  unsigned int GetNgpsPPS( void ) const { return fNgpsPPS; }
  unsigned int GetCoHitRw( void ) const { return fCoHitRw; }
  unsigned int GetCoHitCD( void ) const { return fCoHitCD; }
  unsigned int GetHitPatt( void ) const { return fHitPatt; }
  //  unsigned int GetTsecond( void ) const { return fTsecond; }
  //  unsigned int GetTmicros( void ) const { return fTmicros; }
  unsigned int GetNtrgAck( void ) const { return fNtrgAck; }
  unsigned int GetNswtAck( void ) const { return fNswtAck; }
  unsigned int GetNextAck( void ) const { return fNextAck; }
  unsigned int GetNlpcAck( void ) const { return fNlpcAck; }
  unsigned int GetNhpcAck( void ) const { return fNhpcAck; }
  unsigned int GetNcoMode( int mode );
  unsigned int GetNcdMode( int mode );
  unsigned int GetNclkACD( void ) const { return fNclkACD; }
  unsigned int GetNclkCZT( void ) const { return fNclkCZT; }
  unsigned int GetNclkCs1( void ) const { return fNclkCs1; }
  unsigned int GetNclkCs2( void ) const { return fNclkCs2; }
  unsigned int GetNclkSiT( int sitr );
  unsigned int GetNhitACD( void ) const { return fNhitACD; }
  unsigned int GetNoneSiT( void ) const { return fNoneSiT; }
  unsigned int GetNandSiT( void ) const { return fNandSiT; }
  unsigned int GetNtwoSiT( void ) const { return fNtwoSiT; }
  unsigned int GetNhitCZT( void ) const { return fNhitCZT; }
  unsigned int GetN1stCsI( void ) const { return fN1stCsI; }
  unsigned int GetN2ndCsI( void ) const { return fN2ndCsI; }
  unsigned int GetNhitXsi( int sitr );
  unsigned int GetNhitYsi( int sitr );

  // methods
  void SetEvent( unsigned int *data );
  void Clear   ( Option_t *option = "" );

  EvntBranch& operator= (const EvntBranch &evnt);

 protected:
  unsigned int GetMSI( unsigned int data ) 
    const { return (0xFFFF & (data>>16) ); }
  unsigned int GetLSI( unsigned int data )
    const { return (0xFFFF & (data>> 0) ); }

  ClassDef( EvntBranch, 1 )
};

#endif
