#ifndef _GPPS_REGISTER_H
#define _GPPS_REGISTER_H

#include "BaseRegister.h"
#include "RegCheckButton.h"

class TGCheckButton;
class TGString;

class GppsRegister : public BaseRegister {
 public:
  enum ENums { N_ITEM = 1 };
  
 protected:
  RegCheckButton *fGpps;
  
 public:
  GppsRegister( const TGWindow *p, TGString *title, UInt_t options );
  virtual ~GppsRegister();

  virtual void Init( void );

  ClassDef( GppsRegister, 0 )
};

#endif
