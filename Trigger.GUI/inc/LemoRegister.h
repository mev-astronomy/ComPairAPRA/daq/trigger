#ifndef _LEMO_REGISTER_H
#define _LEMO_REGISTER_H

#include "BaseRegister.h"
#include "RegCheckButton.h"

class TGCheckButton;
class TGString;

class LemoRegister : public BaseRegister {
 public:
  enum ENums { N_ITEM = 8 };
  
 protected:
  RegCheckButton *fLemo;
  
 public:
  LemoRegister( const TGWindow *p, TGString *title, UInt_t options );
  virtual ~LemoRegister();

  virtual void Init( void );

  ClassDef( LemoRegister, 0 )
};

#endif
