BASEDIR = .
INC_DIR = $(BASEDIR)/inc
SRC_DIR = $(BASEDIR)/src
OBJ_DIR = $(BASEDIR)/obj
DEP_DIR = $(BASEDIR)/dep
BIN_DIR = $(BASEDIR)/bin
LIB_DIR = $(BASEDIR)/lib

ARCH := $(shell uname -s)
KREL := $(shell uname -r)

LIBR = libAmRoot
AGUI = agui
AROT = aroot

ROOTCFLAGS  := $(shell root-config --cflags)
ROOTLIBS    := $(shell root-config --libs)
ROOTGLIBS   := $(shell root-config --glibs) -lEG
ROOTLIBDIR  := $(shell root-config --libdir)
ROOTVERSION := $(shell root-config --version)

ifeq ($(ARCH),Darwin)
CXXDEFS = -D_DARWIN_
CXX     = clang++
CINT    = rootcint6
else
CXXDEFS = -D_FILE_OFFSET_BITS=64
CXX     = g++
CINT    = rootcint
endif

CXXOPT = -Wall -O1 -fPIC
CXXDBG = -ggdb

CXXFLAGS = -I$(BASEDIR) -I$(INC_DIR) $(ROOTCFLAGS) $(CXXOPT) $(CXXDBG) $(CXXDEFS)

LDFLAGS  = -lpthread -ltermcap -lpanel $(ROOTLIBS)
BINFLAGS = -Wl,-rpath,$(LIB_DIR) -Wl,-rpath,$(ROOTLIBDIR)
SOFLAGS  = -shared

DICTB = AmDict.cpp
OBJSB = TcpSocket.o Pthreading.o CmdHandler.o AmMainFrame.o BaseRegister.o ModeRegister.o ConnRegister.o LemoRegister.o GppsRegister.o ExtrRegister.o EvidRegister.o WdthRegister.o ChanRegister.o RegObject.o RegEnable.o RegRadioButton.o RegCheckButton.o RegNumberEntry.o RegComboBox.o TCmndSnder.o TDataRcver.o EvntBranch.o HistBranch.o

INCSB = $(addprefix $(INC_DIR)/, $(addsuffix .h, $(basename $(OBJSB) )))\
	$(INC_DIR)/AmLinkDef.h

DICTS = $(DICTB)

ODEP  = $(OBJS0) $(OBJSB)

PROG  = $(AGUI) $(AROT)

DEPS  = $(addsuffix .d, $(PROG)) $(addsuffix .d, $(basename $(ODEP)))
OBJS  = $(addprefix $(OBJ_DIR)/, $(OBJSB))
DOBJS = $(addprefix $(OBJ_DIR)/, $(addsuffix .o, $(basename $(DICTS))))\
	$(addprefix $(OBJ_DIR)/, $(OBJSB))
BINS  = $(addprefix $(BIN_DIR)/, $(PROG))
BOBJS = $(addprefix $(OBJ_DIR)/, $(addsuffix .o,  $(PROG)))
LIBSR = $(addprefix $(LIB_DIR)/, $(addsuffix .so, $(LIBR)))
DICTP = $(basename $(DICTS))

ALL: $(LIBSR) $(BINS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

$(LIBSR): $(OBJS) $(DOBJS)
	$(CXX) $(LDFLAGS) $(SOFLAGS) $(ROOTGLIBS) $^ -o $@

$(BIN_DIR)/$(AGUI): $(OBJ_DIR)/$(AGUI).o $(LIBSR) 
	$(CXX) $^ $(ROOTGLIBS) -o $@

$(BIN_DIR)/$(AROT): $(OBJ_DIR)/$(AROT).o $(LIBSR)
	$(CXX) $^ $(ROOTGLIBS) -o $@

$(SRC_DIR)/$(DICTB) : $(INCSB)
	@echo "Generating dictionary $@..."
	@$(CINT) -f $@ -c $^
	@mv -f $(SRC_DIR)/$(DICTP)_rdict.pcm $(LIB_DIR)/

$(DEP_DIR)/%.d : $(SRC_DIR)/%.cpp | $(DEP_DIR)
	$(CXX) $(CXXFLAGS) -MM $< | sed 's,$*.o,$(OBJ_DIR)/$*.o $(DEP_DIR)/$*.d,g' > $@

include $(addprefix $(DEP_DIR)/, $(DEPS))

html:	$(LIBSR)
	root -q -l -b 'macro/html.C'

clean:
	rm -f $(BINS) $(OBJ_DIR)/*.o $(DEP_DIR)/*.d $(LIB_DIR)/lib*
	rm -f $(SRC_DIR)/*Dict.* .*~ *~ */*~

.PHONY : clean

$(OBJS): | $(OBJ_DIR)
$(INCSB):| $(LIB_DIR)
$(LIBSR):| $(LIB_DIR)
$(BINS): | $(BIN_DIR)

$(DEP_DIR):
	mkdir -p $(DEP_DIR)

$(OBJ_DIR):
	mkdir -p $(OBJ_DIR)

$(LIB_DIR):
	mkdir -p $(LIB_DIR)

$(BIN_DIR):
	mkdir -p $(BIN_DIR)
