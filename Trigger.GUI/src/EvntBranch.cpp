#include "EvntBranch.h"

#include <iostream>

ClassImp( EvntBranch )

EvntBranch::EvntBranch()
{
  for ( Int_t i = 0; i < N_DATA; i++ )
    fEvtPrev[i] = 0;

  Clear();
}

EvntBranch::~EvntBranch()
{
}

EvntBranch& EvntBranch::operator= (const EvntBranch &evnt)
{
  fEventID = evnt.fEventID;
  fNtrgLck = evnt.fNtrgLck;
  fNclkPps = evnt.fNclkPps;
  fNclkRaw = evnt.fNclkRaw;
  fNclkEna = evnt.fNclkEna;
  fNclkLiv = evnt.fNclkLiv;
  fNgpsPPS = evnt.fNgpsPPS;
  fCoHitRw = evnt.fCoHitRw;
  fCoHitCD = evnt.fCoHitCD;
  fHitPatt = evnt.fHitPatt;
  //  fTsecond = evnt.fTsecond;
  //  fTmicros = evnt.fTmicros;
  fNtrgAck = evnt.fNtrgAck;
  fNswtAck = evnt.fNswtAck;
  fNextAck = evnt.fNextAck;
  fNlpcAck = evnt.fNlpcAck;
  fNhpcAck = evnt.fNhpcAck;
  for ( Int_t i = 0; i < N_MODE; i++ ) {
    fNcoMode[i] = evnt.fNcoMode[i];
    fNcdMode[i] = evnt.fNcdMode[i];
  }
  fNclkACD = evnt.fNclkACD;
  fNclkCZT = evnt.fNclkCZT;
  fNclkCs1 = evnt.fNclkCs1;
  fNclkCs2 = evnt.fNclkCs2;
  for ( Int_t i = 0; i < N_SITR; i++ ) {
    fNclkSiT[i] = evnt.fNclkSiT[i];
  }
  fNhitACD = evnt.fNhitACD;
  fNoneSiT = evnt.fNoneSiT;
  fNandSiT = evnt.fNandSiT;
  fNtwoSiT = evnt.fNtwoSiT;
  fNhitCZT = evnt.fNhitCZT;
  fN1stCsI = evnt.fN1stCsI;
  fN2ndCsI = evnt.fN2ndCsI;
  for ( Int_t i = 0; i < N_SITR; i++ ) {
    fNhitXsi[i] = evnt.fNhitXsi[i];
    fNhitYsi[i] = evnt.fNhitYsi[i];
  }

  memcpy( (void *)&fEvtPrev, (void *)&evnt.fEvtPrev, N_DATA*4 );
  return *this;
}

unsigned int EvntBranch::GetNcoMode( int mode )
{
  if ( mode < 0 || mode >= N_MODE ) return 0;
  return fNcoMode[mode];
}

unsigned int EvntBranch::GetNcdMode( int mode )
{
  if ( mode < 0 || mode >= N_MODE ) return 0;
  return fNcdMode[mode];
}

unsigned int EvntBranch::GetNclkSiT( int sitr )
{
  if ( sitr < 0 || sitr >= N_SITR ) return 0;
  return fNclkSiT[sitr];
}

unsigned int EvntBranch::GetNhitXsi( int sitr )
{
  if ( sitr < 0 || sitr >= N_SITR ) return 0;
  return fNhitXsi[sitr];
}

unsigned int EvntBranch::GetNhitYsi( int sitr )
{
  if ( sitr < 0 || sitr >= N_SITR ) return 0;
  return fNhitYsi[sitr];
}

void EvntBranch::SetEvent( unsigned int *data )
{
  fEventID = *(data+1);
  fNtrgLck = *(data+1)-fEvtPrev[1];
  if ( fNtrgLck > 4294900000.0 ) {
    std::cout << "*(data+1) = " << *(data+1) << ", fEvtPref[1] = " << fEvtPrev[1] << std::endl;
  }
  fNclkPps = *(data+2);
  fNclkRaw = *(data+3)-fEvtPrev[3];
  fNclkEna = *(data+4)-fEvtPrev[4];
  fNclkLiv = *(data+5)-fEvtPrev[5];
  fNgpsPPS = *(data+6)-fEvtPrev[6];
  fCoHitRw = GetLSI( *(data+7) );
  fCoHitCD = GetMSI( *(data+7) );
  fHitPatt = *(data+8);
  //  fTsecond = *(data+9);
  //  fTmicros = *(data+10);
  fNtrgAck = *(data+9) - fEvtPrev[9];
  fNswtAck = (0xFFFF & (GetLSI( *(data+10) ) - GetLSI( fEvtPrev[10] )) );
  fNextAck = (0xFFFF & (GetMSI( *(data+10) ) - GetMSI( fEvtPrev[10] )) );
  fNlpcAck = (0xFFFF & (GetLSI( *(data+11) ) - GetLSI( fEvtPrev[11] )) );
  fNhpcAck = (0xFFFF & (GetMSI( *(data+11) ) - GetMSI( fEvtPrev[11] )) );
  for ( Int_t i = 0; i < N_MODE; i++ ) {
    fNcoMode[i] = (0xFFFF & (GetLSI( *(data+12+i) ) - GetLSI( fEvtPrev[12+i] )) );
    fNcdMode[i] = (0xFFFF & (GetMSI( *(data+12+i) ) - GetMSI( fEvtPrev[12+i] )) );
  }
  fNclkACD = *(data+28)-fEvtPrev[28];
  fNclkCZT = *(data+29)-fEvtPrev[29];
  fNclkCs1 = *(data+30)-fEvtPrev[30];
  fNclkCs2 = *(data+31)-fEvtPrev[31];
  for ( Int_t i = 0; i < N_SITR; i++ ) {
    fNclkSiT[i] = *(data+32+i) - fEvtPrev[32+i];
  }
  fNhitACD = *(data+44) - fEvtPrev[44];
  fNoneSiT = *(data+45) - fEvtPrev[45];
  fNandSiT = *(data+46) - fEvtPrev[46];
  fNtwoSiT = *(data+47) - fEvtPrev[47];
  fNhitCZT = *(data+48) - fEvtPrev[48];
  fN1stCsI = *(data+49) - fEvtPrev[49];
  fN2ndCsI = *(data+50) - fEvtPrev[50];
  for ( Int_t i = 0; i < N_SITR; i++ ) {
    fNhitXsi[i] = (0xFFFF & (GetLSI( *(data+51+i) ) - GetLSI( fEvtPrev[51+i] )) );
    fNhitYsi[i] = (0xFFFF & (GetMSI( *(data+51+i) ) - GetMSI( fEvtPrev[51+i] )) );
  }
  
  memcpy( (void *)&fEvtPrev, (void *)data, N_DATA*4 );
}

void EvntBranch::Clear( Option_t *option )
{
  fEventID = 0;
  fNtrgLck = 0;
  fNclkPps = 0;
  fNclkRaw = 0;
  fNclkEna = 0;
  fNclkLiv = 0;
  fNgpsPPS = 0;
  fCoHitRw = 0;
  fCoHitCD = 0;
  fHitPatt = 0;
  //  fTsecond = 0;
  //  fTmicros = 0;
  fNtrgAck = 0;
  fNswtAck = 0;
  fNextAck = 0;
  fNlpcAck = 0;
  fNhpcAck = 0;
  for ( Int_t i = 0; i < N_MODE; i++ ) {
    fNcoMode[i] = 0;
    fNcdMode[i] = 0;
  }
  fNclkACD = 0;
  fNclkCZT = 0;
  fNclkCs1 = 0;
  fNclkCs2 = 0;
  for ( Int_t i = 0; i < N_SITR; i++ )
    fNclkSiT[i] = 0;
  fNhitACD = 0;
  fNoneSiT = 0;
  fNandSiT = 0;
  fNtwoSiT = 0;
  fNhitCZT = 0;
  fN1stCsI = 0;
  fN2ndCsI = 0;
  for ( Int_t i = 0; i < N_SITR; i++ )
    fNhitXsi[i] = fNhitYsi[i] = 0;
}
