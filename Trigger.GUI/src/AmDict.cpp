// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME srcdIAmDict
#define R__NO_DEPRECATION

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// The generated code does not explicitly qualifies STL entities
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "inc/TcpSocket.h"
#include "inc/Pthreading.h"
#include "inc/CmdHandler.h"
#include "inc/AmMainFrame.h"
#include "inc/BaseRegister.h"
#include "inc/ModeRegister.h"
#include "inc/ConnRegister.h"
#include "inc/LemoRegister.h"
#include "inc/GppsRegister.h"
#include "inc/ExtrRegister.h"
#include "inc/EvidRegister.h"
#include "inc/WdthRegister.h"
#include "inc/ChanRegister.h"
#include "inc/RegObject.h"
#include "inc/RegEnable.h"
#include "inc/RegRadioButton.h"
#include "inc/RegCheckButton.h"
#include "inc/RegNumberEntry.h"
#include "inc/RegComboBox.h"
#include "inc/TCmndSnder.h"
#include "inc/TDataRcver.h"
#include "inc/EvntBranch.h"
#include "inc/HistBranch.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static void delete_AmMainFrame(void *p);
   static void deleteArray_AmMainFrame(void *p);
   static void destruct_AmMainFrame(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::AmMainFrame*)
   {
      ::AmMainFrame *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::AmMainFrame >(0);
      static ::ROOT::TGenericClassInfo 
         instance("AmMainFrame", ::AmMainFrame::Class_Version(), "inc/AmMainFrame.h", 39,
                  typeid(::AmMainFrame), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::AmMainFrame::Dictionary, isa_proxy, 4,
                  sizeof(::AmMainFrame) );
      instance.SetDelete(&delete_AmMainFrame);
      instance.SetDeleteArray(&deleteArray_AmMainFrame);
      instance.SetDestructor(&destruct_AmMainFrame);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::AmMainFrame*)
   {
      return GenerateInitInstanceLocal((::AmMainFrame*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::AmMainFrame*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_RegObject(void *p);
   static void deleteArray_RegObject(void *p);
   static void destruct_RegObject(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RegObject*)
   {
      ::RegObject *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::RegObject >(0);
      static ::ROOT::TGenericClassInfo 
         instance("RegObject", ::RegObject::Class_Version(), "inc/RegObject.h", 8,
                  typeid(::RegObject), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::RegObject::Dictionary, isa_proxy, 4,
                  sizeof(::RegObject) );
      instance.SetDelete(&delete_RegObject);
      instance.SetDeleteArray(&deleteArray_RegObject);
      instance.SetDestructor(&destruct_RegObject);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RegObject*)
   {
      return GenerateInitInstanceLocal((::RegObject*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::RegObject*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_BaseRegister(void *p);
   static void deleteArray_BaseRegister(void *p);
   static void destruct_BaseRegister(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::BaseRegister*)
   {
      ::BaseRegister *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::BaseRegister >(0);
      static ::ROOT::TGenericClassInfo 
         instance("BaseRegister", ::BaseRegister::Class_Version(), "inc/BaseRegister.h", 18,
                  typeid(::BaseRegister), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::BaseRegister::Dictionary, isa_proxy, 4,
                  sizeof(::BaseRegister) );
      instance.SetDelete(&delete_BaseRegister);
      instance.SetDeleteArray(&deleteArray_BaseRegister);
      instance.SetDestructor(&destruct_BaseRegister);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::BaseRegister*)
   {
      return GenerateInitInstanceLocal((::BaseRegister*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::BaseRegister*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_RegRadioButton(void *p);
   static void deleteArray_RegRadioButton(void *p);
   static void destruct_RegRadioButton(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RegRadioButton*)
   {
      ::RegRadioButton *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::RegRadioButton >(0);
      static ::ROOT::TGenericClassInfo 
         instance("RegRadioButton", ::RegRadioButton::Class_Version(), "inc/RegRadioButton.h", 8,
                  typeid(::RegRadioButton), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::RegRadioButton::Dictionary, isa_proxy, 4,
                  sizeof(::RegRadioButton) );
      instance.SetDelete(&delete_RegRadioButton);
      instance.SetDeleteArray(&deleteArray_RegRadioButton);
      instance.SetDestructor(&destruct_RegRadioButton);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RegRadioButton*)
   {
      return GenerateInitInstanceLocal((::RegRadioButton*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::RegRadioButton*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_RegCheckButton(void *p);
   static void deleteArray_RegCheckButton(void *p);
   static void destruct_RegCheckButton(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RegCheckButton*)
   {
      ::RegCheckButton *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::RegCheckButton >(0);
      static ::ROOT::TGenericClassInfo 
         instance("RegCheckButton", ::RegCheckButton::Class_Version(), "inc/RegCheckButton.h", 8,
                  typeid(::RegCheckButton), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::RegCheckButton::Dictionary, isa_proxy, 4,
                  sizeof(::RegCheckButton) );
      instance.SetDelete(&delete_RegCheckButton);
      instance.SetDeleteArray(&deleteArray_RegCheckButton);
      instance.SetDestructor(&destruct_RegCheckButton);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RegCheckButton*)
   {
      return GenerateInitInstanceLocal((::RegCheckButton*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::RegCheckButton*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_RegNumberEntry(void *p);
   static void deleteArray_RegNumberEntry(void *p);
   static void destruct_RegNumberEntry(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RegNumberEntry*)
   {
      ::RegNumberEntry *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::RegNumberEntry >(0);
      static ::ROOT::TGenericClassInfo 
         instance("RegNumberEntry", ::RegNumberEntry::Class_Version(), "inc/RegNumberEntry.h", 8,
                  typeid(::RegNumberEntry), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::RegNumberEntry::Dictionary, isa_proxy, 4,
                  sizeof(::RegNumberEntry) );
      instance.SetDelete(&delete_RegNumberEntry);
      instance.SetDeleteArray(&deleteArray_RegNumberEntry);
      instance.SetDestructor(&destruct_RegNumberEntry);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RegNumberEntry*)
   {
      return GenerateInitInstanceLocal((::RegNumberEntry*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::RegNumberEntry*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_ModeRegister(void *p);
   static void deleteArray_ModeRegister(void *p);
   static void destruct_ModeRegister(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ModeRegister*)
   {
      ::ModeRegister *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::ModeRegister >(0);
      static ::ROOT::TGenericClassInfo 
         instance("ModeRegister", ::ModeRegister::Class_Version(), "inc/ModeRegister.h", 14,
                  typeid(::ModeRegister), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::ModeRegister::Dictionary, isa_proxy, 4,
                  sizeof(::ModeRegister) );
      instance.SetDelete(&delete_ModeRegister);
      instance.SetDeleteArray(&deleteArray_ModeRegister);
      instance.SetDestructor(&destruct_ModeRegister);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ModeRegister*)
   {
      return GenerateInitInstanceLocal((::ModeRegister*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::ModeRegister*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_ConnRegister(void *p);
   static void deleteArray_ConnRegister(void *p);
   static void destruct_ConnRegister(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ConnRegister*)
   {
      ::ConnRegister *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::ConnRegister >(0);
      static ::ROOT::TGenericClassInfo 
         instance("ConnRegister", ::ConnRegister::Class_Version(), "inc/ConnRegister.h", 10,
                  typeid(::ConnRegister), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::ConnRegister::Dictionary, isa_proxy, 4,
                  sizeof(::ConnRegister) );
      instance.SetDelete(&delete_ConnRegister);
      instance.SetDeleteArray(&deleteArray_ConnRegister);
      instance.SetDestructor(&destruct_ConnRegister);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ConnRegister*)
   {
      return GenerateInitInstanceLocal((::ConnRegister*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::ConnRegister*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_LemoRegister(void *p);
   static void deleteArray_LemoRegister(void *p);
   static void destruct_LemoRegister(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::LemoRegister*)
   {
      ::LemoRegister *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::LemoRegister >(0);
      static ::ROOT::TGenericClassInfo 
         instance("LemoRegister", ::LemoRegister::Class_Version(), "inc/LemoRegister.h", 10,
                  typeid(::LemoRegister), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::LemoRegister::Dictionary, isa_proxy, 4,
                  sizeof(::LemoRegister) );
      instance.SetDelete(&delete_LemoRegister);
      instance.SetDeleteArray(&deleteArray_LemoRegister);
      instance.SetDestructor(&destruct_LemoRegister);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::LemoRegister*)
   {
      return GenerateInitInstanceLocal((::LemoRegister*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::LemoRegister*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_GppsRegister(void *p);
   static void deleteArray_GppsRegister(void *p);
   static void destruct_GppsRegister(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::GppsRegister*)
   {
      ::GppsRegister *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::GppsRegister >(0);
      static ::ROOT::TGenericClassInfo 
         instance("GppsRegister", ::GppsRegister::Class_Version(), "inc/GppsRegister.h", 10,
                  typeid(::GppsRegister), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::GppsRegister::Dictionary, isa_proxy, 4,
                  sizeof(::GppsRegister) );
      instance.SetDelete(&delete_GppsRegister);
      instance.SetDeleteArray(&deleteArray_GppsRegister);
      instance.SetDestructor(&destruct_GppsRegister);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::GppsRegister*)
   {
      return GenerateInitInstanceLocal((::GppsRegister*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::GppsRegister*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_RegComboBox(void *p);
   static void deleteArray_RegComboBox(void *p);
   static void destruct_RegComboBox(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RegComboBox*)
   {
      ::RegComboBox *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::RegComboBox >(0);
      static ::ROOT::TGenericClassInfo 
         instance("RegComboBox", ::RegComboBox::Class_Version(), "inc/RegComboBox.h", 8,
                  typeid(::RegComboBox), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::RegComboBox::Dictionary, isa_proxy, 4,
                  sizeof(::RegComboBox) );
      instance.SetDelete(&delete_RegComboBox);
      instance.SetDeleteArray(&deleteArray_RegComboBox);
      instance.SetDestructor(&destruct_RegComboBox);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RegComboBox*)
   {
      return GenerateInitInstanceLocal((::RegComboBox*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::RegComboBox*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_ExtrRegister(void *p);
   static void deleteArray_ExtrRegister(void *p);
   static void destruct_ExtrRegister(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ExtrRegister*)
   {
      ::ExtrRegister *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::ExtrRegister >(0);
      static ::ROOT::TGenericClassInfo 
         instance("ExtrRegister", ::ExtrRegister::Class_Version(), "inc/ExtrRegister.h", 10,
                  typeid(::ExtrRegister), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::ExtrRegister::Dictionary, isa_proxy, 4,
                  sizeof(::ExtrRegister) );
      instance.SetDelete(&delete_ExtrRegister);
      instance.SetDeleteArray(&deleteArray_ExtrRegister);
      instance.SetDestructor(&destruct_ExtrRegister);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ExtrRegister*)
   {
      return GenerateInitInstanceLocal((::ExtrRegister*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::ExtrRegister*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_EvidRegister(void *p);
   static void deleteArray_EvidRegister(void *p);
   static void destruct_EvidRegister(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::EvidRegister*)
   {
      ::EvidRegister *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::EvidRegister >(0);
      static ::ROOT::TGenericClassInfo 
         instance("EvidRegister", ::EvidRegister::Class_Version(), "inc/EvidRegister.h", 10,
                  typeid(::EvidRegister), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::EvidRegister::Dictionary, isa_proxy, 4,
                  sizeof(::EvidRegister) );
      instance.SetDelete(&delete_EvidRegister);
      instance.SetDeleteArray(&deleteArray_EvidRegister);
      instance.SetDestructor(&destruct_EvidRegister);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::EvidRegister*)
   {
      return GenerateInitInstanceLocal((::EvidRegister*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::EvidRegister*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_WdthRegister(void *p);
   static void deleteArray_WdthRegister(void *p);
   static void destruct_WdthRegister(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::WdthRegister*)
   {
      ::WdthRegister *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::WdthRegister >(0);
      static ::ROOT::TGenericClassInfo 
         instance("WdthRegister", ::WdthRegister::Class_Version(), "inc/WdthRegister.h", 10,
                  typeid(::WdthRegister), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::WdthRegister::Dictionary, isa_proxy, 4,
                  sizeof(::WdthRegister) );
      instance.SetDelete(&delete_WdthRegister);
      instance.SetDeleteArray(&deleteArray_WdthRegister);
      instance.SetDestructor(&destruct_WdthRegister);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::WdthRegister*)
   {
      return GenerateInitInstanceLocal((::WdthRegister*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::WdthRegister*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_ChanRegister(void *p);
   static void deleteArray_ChanRegister(void *p);
   static void destruct_ChanRegister(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ChanRegister*)
   {
      ::ChanRegister *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::ChanRegister >(0);
      static ::ROOT::TGenericClassInfo 
         instance("ChanRegister", ::ChanRegister::Class_Version(), "inc/ChanRegister.h", 9,
                  typeid(::ChanRegister), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::ChanRegister::Dictionary, isa_proxy, 4,
                  sizeof(::ChanRegister) );
      instance.SetDelete(&delete_ChanRegister);
      instance.SetDeleteArray(&deleteArray_ChanRegister);
      instance.SetDestructor(&destruct_ChanRegister);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ChanRegister*)
   {
      return GenerateInitInstanceLocal((::ChanRegister*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::ChanRegister*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_RegEnable(void *p);
   static void deleteArray_RegEnable(void *p);
   static void destruct_RegEnable(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RegEnable*)
   {
      ::RegEnable *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::RegEnable >(0);
      static ::ROOT::TGenericClassInfo 
         instance("RegEnable", ::RegEnable::Class_Version(), "inc/RegEnable.h", 8,
                  typeid(::RegEnable), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::RegEnable::Dictionary, isa_proxy, 4,
                  sizeof(::RegEnable) );
      instance.SetDelete(&delete_RegEnable);
      instance.SetDeleteArray(&deleteArray_RegEnable);
      instance.SetDestructor(&destruct_RegEnable);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RegEnable*)
   {
      return GenerateInitInstanceLocal((::RegEnable*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::RegEnable*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_TCmndSnder(void *p);
   static void deleteArray_TCmndSnder(void *p);
   static void destruct_TCmndSnder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TCmndSnder*)
   {
      ::TCmndSnder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::TCmndSnder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("TCmndSnder", ::TCmndSnder::Class_Version(), "inc/TCmndSnder.h", 8,
                  typeid(::TCmndSnder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::TCmndSnder::Dictionary, isa_proxy, 4,
                  sizeof(::TCmndSnder) );
      instance.SetDelete(&delete_TCmndSnder);
      instance.SetDeleteArray(&deleteArray_TCmndSnder);
      instance.SetDestructor(&destruct_TCmndSnder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TCmndSnder*)
   {
      return GenerateInitInstanceLocal((::TCmndSnder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::TCmndSnder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_EvntBranch(void *p = 0);
   static void *newArray_EvntBranch(Long_t size, void *p);
   static void delete_EvntBranch(void *p);
   static void deleteArray_EvntBranch(void *p);
   static void destruct_EvntBranch(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::EvntBranch*)
   {
      ::EvntBranch *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::EvntBranch >(0);
      static ::ROOT::TGenericClassInfo 
         instance("EvntBranch", ::EvntBranch::Class_Version(), "inc/EvntBranch.h", 6,
                  typeid(::EvntBranch), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::EvntBranch::Dictionary, isa_proxy, 4,
                  sizeof(::EvntBranch) );
      instance.SetNew(&new_EvntBranch);
      instance.SetNewArray(&newArray_EvntBranch);
      instance.SetDelete(&delete_EvntBranch);
      instance.SetDeleteArray(&deleteArray_EvntBranch);
      instance.SetDestructor(&destruct_EvntBranch);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::EvntBranch*)
   {
      return GenerateInitInstanceLocal((::EvntBranch*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::EvntBranch*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_TDataRcver(void *p = 0);
   static void *newArray_TDataRcver(Long_t size, void *p);
   static void delete_TDataRcver(void *p);
   static void deleteArray_TDataRcver(void *p);
   static void destruct_TDataRcver(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TDataRcver*)
   {
      ::TDataRcver *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::TDataRcver >(0);
      static ::ROOT::TGenericClassInfo 
         instance("TDataRcver", ::TDataRcver::Class_Version(), "inc/TDataRcver.h", 23,
                  typeid(::TDataRcver), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::TDataRcver::Dictionary, isa_proxy, 4,
                  sizeof(::TDataRcver) );
      instance.SetNew(&new_TDataRcver);
      instance.SetNewArray(&newArray_TDataRcver);
      instance.SetDelete(&delete_TDataRcver);
      instance.SetDeleteArray(&deleteArray_TDataRcver);
      instance.SetDestructor(&destruct_TDataRcver);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TDataRcver*)
   {
      return GenerateInitInstanceLocal((::TDataRcver*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::TDataRcver*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HistBranch(void *p = 0);
   static void *newArray_HistBranch(Long_t size, void *p);
   static void delete_HistBranch(void *p);
   static void deleteArray_HistBranch(void *p);
   static void destruct_HistBranch(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::HistBranch*)
   {
      ::HistBranch *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::HistBranch >(0);
      static ::ROOT::TGenericClassInfo 
         instance("HistBranch", ::HistBranch::Class_Version(), "inc/HistBranch.h", 12,
                  typeid(::HistBranch), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::HistBranch::Dictionary, isa_proxy, 4,
                  sizeof(::HistBranch) );
      instance.SetNew(&new_HistBranch);
      instance.SetNewArray(&newArray_HistBranch);
      instance.SetDelete(&delete_HistBranch);
      instance.SetDeleteArray(&deleteArray_HistBranch);
      instance.SetDestructor(&destruct_HistBranch);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::HistBranch*)
   {
      return GenerateInitInstanceLocal((::HistBranch*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::HistBranch*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
atomic_TClass_ptr AmMainFrame::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *AmMainFrame::Class_Name()
{
   return "AmMainFrame";
}

//______________________________________________________________________________
const char *AmMainFrame::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::AmMainFrame*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int AmMainFrame::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::AmMainFrame*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *AmMainFrame::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::AmMainFrame*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *AmMainFrame::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::AmMainFrame*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr RegObject::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *RegObject::Class_Name()
{
   return "RegObject";
}

//______________________________________________________________________________
const char *RegObject::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RegObject*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int RegObject::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RegObject*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *RegObject::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RegObject*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *RegObject::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RegObject*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr BaseRegister::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *BaseRegister::Class_Name()
{
   return "BaseRegister";
}

//______________________________________________________________________________
const char *BaseRegister::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::BaseRegister*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int BaseRegister::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::BaseRegister*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *BaseRegister::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::BaseRegister*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *BaseRegister::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::BaseRegister*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr RegRadioButton::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *RegRadioButton::Class_Name()
{
   return "RegRadioButton";
}

//______________________________________________________________________________
const char *RegRadioButton::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RegRadioButton*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int RegRadioButton::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RegRadioButton*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *RegRadioButton::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RegRadioButton*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *RegRadioButton::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RegRadioButton*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr RegCheckButton::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *RegCheckButton::Class_Name()
{
   return "RegCheckButton";
}

//______________________________________________________________________________
const char *RegCheckButton::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RegCheckButton*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int RegCheckButton::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RegCheckButton*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *RegCheckButton::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RegCheckButton*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *RegCheckButton::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RegCheckButton*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr RegNumberEntry::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *RegNumberEntry::Class_Name()
{
   return "RegNumberEntry";
}

//______________________________________________________________________________
const char *RegNumberEntry::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RegNumberEntry*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int RegNumberEntry::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RegNumberEntry*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *RegNumberEntry::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RegNumberEntry*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *RegNumberEntry::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RegNumberEntry*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr ModeRegister::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *ModeRegister::Class_Name()
{
   return "ModeRegister";
}

//______________________________________________________________________________
const char *ModeRegister::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ModeRegister*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int ModeRegister::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ModeRegister*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *ModeRegister::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ModeRegister*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *ModeRegister::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ModeRegister*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr ConnRegister::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *ConnRegister::Class_Name()
{
   return "ConnRegister";
}

//______________________________________________________________________________
const char *ConnRegister::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ConnRegister*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int ConnRegister::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ConnRegister*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *ConnRegister::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ConnRegister*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *ConnRegister::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ConnRegister*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr LemoRegister::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *LemoRegister::Class_Name()
{
   return "LemoRegister";
}

//______________________________________________________________________________
const char *LemoRegister::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::LemoRegister*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int LemoRegister::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::LemoRegister*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *LemoRegister::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::LemoRegister*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *LemoRegister::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::LemoRegister*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr GppsRegister::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *GppsRegister::Class_Name()
{
   return "GppsRegister";
}

//______________________________________________________________________________
const char *GppsRegister::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::GppsRegister*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int GppsRegister::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::GppsRegister*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *GppsRegister::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::GppsRegister*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *GppsRegister::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::GppsRegister*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr RegComboBox::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *RegComboBox::Class_Name()
{
   return "RegComboBox";
}

//______________________________________________________________________________
const char *RegComboBox::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RegComboBox*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int RegComboBox::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RegComboBox*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *RegComboBox::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RegComboBox*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *RegComboBox::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RegComboBox*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr ExtrRegister::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *ExtrRegister::Class_Name()
{
   return "ExtrRegister";
}

//______________________________________________________________________________
const char *ExtrRegister::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ExtrRegister*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int ExtrRegister::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ExtrRegister*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *ExtrRegister::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ExtrRegister*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *ExtrRegister::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ExtrRegister*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr EvidRegister::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *EvidRegister::Class_Name()
{
   return "EvidRegister";
}

//______________________________________________________________________________
const char *EvidRegister::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::EvidRegister*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int EvidRegister::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::EvidRegister*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *EvidRegister::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::EvidRegister*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *EvidRegister::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::EvidRegister*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr WdthRegister::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *WdthRegister::Class_Name()
{
   return "WdthRegister";
}

//______________________________________________________________________________
const char *WdthRegister::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::WdthRegister*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int WdthRegister::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::WdthRegister*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *WdthRegister::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::WdthRegister*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *WdthRegister::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::WdthRegister*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr ChanRegister::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *ChanRegister::Class_Name()
{
   return "ChanRegister";
}

//______________________________________________________________________________
const char *ChanRegister::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ChanRegister*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int ChanRegister::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ChanRegister*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *ChanRegister::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ChanRegister*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *ChanRegister::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ChanRegister*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr RegEnable::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *RegEnable::Class_Name()
{
   return "RegEnable";
}

//______________________________________________________________________________
const char *RegEnable::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RegEnable*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int RegEnable::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RegEnable*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *RegEnable::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RegEnable*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *RegEnable::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RegEnable*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr TCmndSnder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TCmndSnder::Class_Name()
{
   return "TCmndSnder";
}

//______________________________________________________________________________
const char *TCmndSnder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TCmndSnder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TCmndSnder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TCmndSnder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TCmndSnder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TCmndSnder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TCmndSnder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TCmndSnder*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr EvntBranch::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *EvntBranch::Class_Name()
{
   return "EvntBranch";
}

//______________________________________________________________________________
const char *EvntBranch::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::EvntBranch*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int EvntBranch::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::EvntBranch*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *EvntBranch::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::EvntBranch*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *EvntBranch::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::EvntBranch*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr TDataRcver::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TDataRcver::Class_Name()
{
   return "TDataRcver";
}

//______________________________________________________________________________
const char *TDataRcver::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TDataRcver*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TDataRcver::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TDataRcver*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TDataRcver::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TDataRcver*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TDataRcver::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TDataRcver*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr HistBranch::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HistBranch::Class_Name()
{
   return "HistBranch";
}

//______________________________________________________________________________
const char *HistBranch::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HistBranch*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HistBranch::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HistBranch*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HistBranch::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HistBranch*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HistBranch::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HistBranch*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
void AmMainFrame::Streamer(TBuffer &R__b)
{
   // Stream an object of class AmMainFrame.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(AmMainFrame::Class(),this);
   } else {
      R__b.WriteClassBuffer(AmMainFrame::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_AmMainFrame(void *p) {
      delete ((::AmMainFrame*)p);
   }
   static void deleteArray_AmMainFrame(void *p) {
      delete [] ((::AmMainFrame*)p);
   }
   static void destruct_AmMainFrame(void *p) {
      typedef ::AmMainFrame current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::AmMainFrame

//______________________________________________________________________________
void RegObject::Streamer(TBuffer &R__b)
{
   // Stream an object of class RegObject.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(RegObject::Class(),this);
   } else {
      R__b.WriteClassBuffer(RegObject::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RegObject(void *p) {
      delete ((::RegObject*)p);
   }
   static void deleteArray_RegObject(void *p) {
      delete [] ((::RegObject*)p);
   }
   static void destruct_RegObject(void *p) {
      typedef ::RegObject current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RegObject

//______________________________________________________________________________
void BaseRegister::Streamer(TBuffer &R__b)
{
   // Stream an object of class BaseRegister.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(BaseRegister::Class(),this);
   } else {
      R__b.WriteClassBuffer(BaseRegister::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_BaseRegister(void *p) {
      delete ((::BaseRegister*)p);
   }
   static void deleteArray_BaseRegister(void *p) {
      delete [] ((::BaseRegister*)p);
   }
   static void destruct_BaseRegister(void *p) {
      typedef ::BaseRegister current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::BaseRegister

//______________________________________________________________________________
void RegRadioButton::Streamer(TBuffer &R__b)
{
   // Stream an object of class RegRadioButton.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(RegRadioButton::Class(),this);
   } else {
      R__b.WriteClassBuffer(RegRadioButton::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RegRadioButton(void *p) {
      delete ((::RegRadioButton*)p);
   }
   static void deleteArray_RegRadioButton(void *p) {
      delete [] ((::RegRadioButton*)p);
   }
   static void destruct_RegRadioButton(void *p) {
      typedef ::RegRadioButton current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RegRadioButton

//______________________________________________________________________________
void RegCheckButton::Streamer(TBuffer &R__b)
{
   // Stream an object of class RegCheckButton.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(RegCheckButton::Class(),this);
   } else {
      R__b.WriteClassBuffer(RegCheckButton::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RegCheckButton(void *p) {
      delete ((::RegCheckButton*)p);
   }
   static void deleteArray_RegCheckButton(void *p) {
      delete [] ((::RegCheckButton*)p);
   }
   static void destruct_RegCheckButton(void *p) {
      typedef ::RegCheckButton current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RegCheckButton

//______________________________________________________________________________
void RegNumberEntry::Streamer(TBuffer &R__b)
{
   // Stream an object of class RegNumberEntry.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(RegNumberEntry::Class(),this);
   } else {
      R__b.WriteClassBuffer(RegNumberEntry::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RegNumberEntry(void *p) {
      delete ((::RegNumberEntry*)p);
   }
   static void deleteArray_RegNumberEntry(void *p) {
      delete [] ((::RegNumberEntry*)p);
   }
   static void destruct_RegNumberEntry(void *p) {
      typedef ::RegNumberEntry current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RegNumberEntry

//______________________________________________________________________________
void ModeRegister::Streamer(TBuffer &R__b)
{
   // Stream an object of class ModeRegister.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(ModeRegister::Class(),this);
   } else {
      R__b.WriteClassBuffer(ModeRegister::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_ModeRegister(void *p) {
      delete ((::ModeRegister*)p);
   }
   static void deleteArray_ModeRegister(void *p) {
      delete [] ((::ModeRegister*)p);
   }
   static void destruct_ModeRegister(void *p) {
      typedef ::ModeRegister current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ModeRegister

//______________________________________________________________________________
void ConnRegister::Streamer(TBuffer &R__b)
{
   // Stream an object of class ConnRegister.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(ConnRegister::Class(),this);
   } else {
      R__b.WriteClassBuffer(ConnRegister::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_ConnRegister(void *p) {
      delete ((::ConnRegister*)p);
   }
   static void deleteArray_ConnRegister(void *p) {
      delete [] ((::ConnRegister*)p);
   }
   static void destruct_ConnRegister(void *p) {
      typedef ::ConnRegister current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ConnRegister

//______________________________________________________________________________
void LemoRegister::Streamer(TBuffer &R__b)
{
   // Stream an object of class LemoRegister.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(LemoRegister::Class(),this);
   } else {
      R__b.WriteClassBuffer(LemoRegister::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_LemoRegister(void *p) {
      delete ((::LemoRegister*)p);
   }
   static void deleteArray_LemoRegister(void *p) {
      delete [] ((::LemoRegister*)p);
   }
   static void destruct_LemoRegister(void *p) {
      typedef ::LemoRegister current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::LemoRegister

//______________________________________________________________________________
void GppsRegister::Streamer(TBuffer &R__b)
{
   // Stream an object of class GppsRegister.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(GppsRegister::Class(),this);
   } else {
      R__b.WriteClassBuffer(GppsRegister::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_GppsRegister(void *p) {
      delete ((::GppsRegister*)p);
   }
   static void deleteArray_GppsRegister(void *p) {
      delete [] ((::GppsRegister*)p);
   }
   static void destruct_GppsRegister(void *p) {
      typedef ::GppsRegister current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::GppsRegister

//______________________________________________________________________________
void RegComboBox::Streamer(TBuffer &R__b)
{
   // Stream an object of class RegComboBox.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(RegComboBox::Class(),this);
   } else {
      R__b.WriteClassBuffer(RegComboBox::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RegComboBox(void *p) {
      delete ((::RegComboBox*)p);
   }
   static void deleteArray_RegComboBox(void *p) {
      delete [] ((::RegComboBox*)p);
   }
   static void destruct_RegComboBox(void *p) {
      typedef ::RegComboBox current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RegComboBox

//______________________________________________________________________________
void ExtrRegister::Streamer(TBuffer &R__b)
{
   // Stream an object of class ExtrRegister.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(ExtrRegister::Class(),this);
   } else {
      R__b.WriteClassBuffer(ExtrRegister::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_ExtrRegister(void *p) {
      delete ((::ExtrRegister*)p);
   }
   static void deleteArray_ExtrRegister(void *p) {
      delete [] ((::ExtrRegister*)p);
   }
   static void destruct_ExtrRegister(void *p) {
      typedef ::ExtrRegister current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ExtrRegister

//______________________________________________________________________________
void EvidRegister::Streamer(TBuffer &R__b)
{
   // Stream an object of class EvidRegister.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(EvidRegister::Class(),this);
   } else {
      R__b.WriteClassBuffer(EvidRegister::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_EvidRegister(void *p) {
      delete ((::EvidRegister*)p);
   }
   static void deleteArray_EvidRegister(void *p) {
      delete [] ((::EvidRegister*)p);
   }
   static void destruct_EvidRegister(void *p) {
      typedef ::EvidRegister current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::EvidRegister

//______________________________________________________________________________
void WdthRegister::Streamer(TBuffer &R__b)
{
   // Stream an object of class WdthRegister.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(WdthRegister::Class(),this);
   } else {
      R__b.WriteClassBuffer(WdthRegister::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_WdthRegister(void *p) {
      delete ((::WdthRegister*)p);
   }
   static void deleteArray_WdthRegister(void *p) {
      delete [] ((::WdthRegister*)p);
   }
   static void destruct_WdthRegister(void *p) {
      typedef ::WdthRegister current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::WdthRegister

//______________________________________________________________________________
void ChanRegister::Streamer(TBuffer &R__b)
{
   // Stream an object of class ChanRegister.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(ChanRegister::Class(),this);
   } else {
      R__b.WriteClassBuffer(ChanRegister::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_ChanRegister(void *p) {
      delete ((::ChanRegister*)p);
   }
   static void deleteArray_ChanRegister(void *p) {
      delete [] ((::ChanRegister*)p);
   }
   static void destruct_ChanRegister(void *p) {
      typedef ::ChanRegister current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ChanRegister

//______________________________________________________________________________
void RegEnable::Streamer(TBuffer &R__b)
{
   // Stream an object of class RegEnable.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(RegEnable::Class(),this);
   } else {
      R__b.WriteClassBuffer(RegEnable::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RegEnable(void *p) {
      delete ((::RegEnable*)p);
   }
   static void deleteArray_RegEnable(void *p) {
      delete [] ((::RegEnable*)p);
   }
   static void destruct_RegEnable(void *p) {
      typedef ::RegEnable current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RegEnable

//______________________________________________________________________________
void TCmndSnder::Streamer(TBuffer &R__b)
{
   // Stream an object of class TCmndSnder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(TCmndSnder::Class(),this);
   } else {
      R__b.WriteClassBuffer(TCmndSnder::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_TCmndSnder(void *p) {
      delete ((::TCmndSnder*)p);
   }
   static void deleteArray_TCmndSnder(void *p) {
      delete [] ((::TCmndSnder*)p);
   }
   static void destruct_TCmndSnder(void *p) {
      typedef ::TCmndSnder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TCmndSnder

//______________________________________________________________________________
void EvntBranch::Streamer(TBuffer &R__b)
{
   // Stream an object of class EvntBranch.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(EvntBranch::Class(),this);
   } else {
      R__b.WriteClassBuffer(EvntBranch::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_EvntBranch(void *p) {
      return  p ? new(p) ::EvntBranch : new ::EvntBranch;
   }
   static void *newArray_EvntBranch(Long_t nElements, void *p) {
      return p ? new(p) ::EvntBranch[nElements] : new ::EvntBranch[nElements];
   }
   // Wrapper around operator delete
   static void delete_EvntBranch(void *p) {
      delete ((::EvntBranch*)p);
   }
   static void deleteArray_EvntBranch(void *p) {
      delete [] ((::EvntBranch*)p);
   }
   static void destruct_EvntBranch(void *p) {
      typedef ::EvntBranch current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::EvntBranch

//______________________________________________________________________________
void TDataRcver::Streamer(TBuffer &R__b)
{
   // Stream an object of class TDataRcver.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(TDataRcver::Class(),this);
   } else {
      R__b.WriteClassBuffer(TDataRcver::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_TDataRcver(void *p) {
      return  p ? new(p) ::TDataRcver : new ::TDataRcver;
   }
   static void *newArray_TDataRcver(Long_t nElements, void *p) {
      return p ? new(p) ::TDataRcver[nElements] : new ::TDataRcver[nElements];
   }
   // Wrapper around operator delete
   static void delete_TDataRcver(void *p) {
      delete ((::TDataRcver*)p);
   }
   static void deleteArray_TDataRcver(void *p) {
      delete [] ((::TDataRcver*)p);
   }
   static void destruct_TDataRcver(void *p) {
      typedef ::TDataRcver current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TDataRcver

//______________________________________________________________________________
void HistBranch::Streamer(TBuffer &R__b)
{
   // Stream an object of class HistBranch.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(HistBranch::Class(),this);
   } else {
      R__b.WriteClassBuffer(HistBranch::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_HistBranch(void *p) {
      return  p ? new(p) ::HistBranch : new ::HistBranch;
   }
   static void *newArray_HistBranch(Long_t nElements, void *p) {
      return p ? new(p) ::HistBranch[nElements] : new ::HistBranch[nElements];
   }
   // Wrapper around operator delete
   static void delete_HistBranch(void *p) {
      delete ((::HistBranch*)p);
   }
   static void deleteArray_HistBranch(void *p) {
      delete [] ((::HistBranch*)p);
   }
   static void destruct_HistBranch(void *p) {
      typedef ::HistBranch current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::HistBranch

namespace {
  void TriggerDictionaryInitialization_AmDict_Impl() {
    static const char* headers[] = {
"inc/TcpSocket.h",
"inc/Pthreading.h",
"inc/CmdHandler.h",
"inc/AmMainFrame.h",
"inc/BaseRegister.h",
"inc/ModeRegister.h",
"inc/ConnRegister.h",
"inc/LemoRegister.h",
"inc/GppsRegister.h",
"inc/ExtrRegister.h",
"inc/EvidRegister.h",
"inc/WdthRegister.h",
"inc/ChanRegister.h",
"inc/RegObject.h",
"inc/RegEnable.h",
"inc/RegRadioButton.h",
"inc/RegCheckButton.h",
"inc/RegNumberEntry.h",
"inc/RegComboBox.h",
"inc/TCmndSnder.h",
"inc/TDataRcver.h",
"inc/EvntBranch.h",
"inc/HistBranch.h",
0
    };
    static const char* includePaths[] = {
"/opt/local/libexec/root6/include/root",
"/Users/msasaki/ComPair/Trigger.GUI/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "AmDict dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_AutoLoading_Map;
class __attribute__((annotate("$clingAutoload$inc/AmMainFrame.h")))  AmMainFrame;
class __attribute__((annotate("$clingAutoload$inc/BaseRegister.h")))  RegObject;
class __attribute__((annotate("$clingAutoload$inc/BaseRegister.h")))  BaseRegister;
class __attribute__((annotate("$clingAutoload$inc/ModeRegister.h")))  RegRadioButton;
class __attribute__((annotate("$clingAutoload$inc/ModeRegister.h")))  RegCheckButton;
class __attribute__((annotate("$clingAutoload$inc/ModeRegister.h")))  RegNumberEntry;
class __attribute__((annotate("$clingAutoload$inc/ModeRegister.h")))  ModeRegister;
class __attribute__((annotate("$clingAutoload$inc/ConnRegister.h")))  ConnRegister;
class __attribute__((annotate("$clingAutoload$inc/LemoRegister.h")))  LemoRegister;
class __attribute__((annotate("$clingAutoload$inc/GppsRegister.h")))  GppsRegister;
class __attribute__((annotate("$clingAutoload$inc/ExtrRegister.h")))  RegComboBox;
class __attribute__((annotate("$clingAutoload$inc/ExtrRegister.h")))  ExtrRegister;
class __attribute__((annotate("$clingAutoload$inc/EvidRegister.h")))  EvidRegister;
class __attribute__((annotate("$clingAutoload$inc/WdthRegister.h")))  WdthRegister;
class __attribute__((annotate("$clingAutoload$inc/ChanRegister.h")))  ChanRegister;
class __attribute__((annotate("$clingAutoload$inc/RegEnable.h")))  RegEnable;
class __attribute__((annotate("$clingAutoload$inc/TCmndSnder.h")))  TCmndSnder;
class __attribute__((annotate("$clingAutoload$inc/TDataRcver.h")))  EvntBranch;
class __attribute__((annotate("$clingAutoload$inc/TDataRcver.h")))  TDataRcver;
class __attribute__((annotate("$clingAutoload$inc/HistBranch.h")))  HistBranch;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "AmDict dictionary payload"


#define _BACKWARD_BACKWARD_WARNING_H
// Inline headers
#include "inc/TcpSocket.h"
#include "inc/Pthreading.h"
#include "inc/CmdHandler.h"
#include "inc/AmMainFrame.h"
#include "inc/BaseRegister.h"
#include "inc/ModeRegister.h"
#include "inc/ConnRegister.h"
#include "inc/LemoRegister.h"
#include "inc/GppsRegister.h"
#include "inc/ExtrRegister.h"
#include "inc/EvidRegister.h"
#include "inc/WdthRegister.h"
#include "inc/ChanRegister.h"
#include "inc/RegObject.h"
#include "inc/RegEnable.h"
#include "inc/RegRadioButton.h"
#include "inc/RegCheckButton.h"
#include "inc/RegNumberEntry.h"
#include "inc/RegComboBox.h"
#include "inc/TCmndSnder.h"
#include "inc/TDataRcver.h"
#include "inc/EvntBranch.h"
#include "inc/HistBranch.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[] = {
"AmMainFrame", payloadCode, "@",
"BaseRegister", payloadCode, "@",
"ChanRegister", payloadCode, "@",
"ConnRegister", payloadCode, "@",
"EvidRegister", payloadCode, "@",
"EvntBranch", payloadCode, "@",
"ExtrRegister", payloadCode, "@",
"GppsRegister", payloadCode, "@",
"HistBranch", payloadCode, "@",
"LemoRegister", payloadCode, "@",
"ModeRegister", payloadCode, "@",
"RegCheckButton", payloadCode, "@",
"RegComboBox", payloadCode, "@",
"RegEnable", payloadCode, "@",
"RegNumberEntry", payloadCode, "@",
"RegObject", payloadCode, "@",
"RegRadioButton", payloadCode, "@",
"TCmndSnder", payloadCode, "@",
"TDataRcver", payloadCode, "@",
"WdthRegister", payloadCode, "@",
nullptr
};
    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("AmDict",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_AmDict_Impl, {}, classesHeaders, /*hasCxxModule*/false);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_AmDict_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_AmDict() {
  TriggerDictionaryInitialization_AmDict_Impl();
}
