#include "BaseRegister.h"
#include "TCmndSnder.h"

#include "TGString.h"

#include <iostream>
#include <cstring>
#include <stdio.h>
#include <map>

ClassImp( BaseRegister )

BaseRegister::BaseRegister( const TGWindow *p, TGString *title, UInt_t options ) : TGGroupFrame( p, title, options )
{
  fSync = false;

  fArr = new TObjArray();

  TGHorizontalFrame *hfrm 
    = new TGHorizontalFrame( this, 280, 240, kDoubleBorder );
  
  fLED = new TGIcon( hfrm, 0, kSIZE, kSIZE );
  fLED->SetPicture( gClient->GetPicture( Form( "icons/led%d.xpm", kOFF ) ) );
  hfrm->AddFrame( fLED, new TGLayoutHints( kLHintsCenterY, 2, 2, 2, 2 ) );

  fSet = new TGTextButton( hfrm, "Set" );
  fSet->Connect( "Clicked()", "BaseRegister", this, "Set()" );
  hfrm->AddFrame( fSet, new TGLayoutHints( kLHintsExpandX | kLHintsCenterX | kLHintsCenterY, 2, 2, 2, 2 ) );

  fGet = new TGTextButton( hfrm, "Get" );
  fGet->Connect( "Clicked()", "BaseRegister", this, "Get()" );
  hfrm->AddFrame( fGet, new TGLayoutHints( kLHintsExpandX | kLHintsCenterX | kLHintsCenterY, 2, 2, 2, 2 ) );

  //  AddFrame( hfrm, new TGLayoutHints( kLHintsExpandX | kLHintsCenterX | kLHintsCenterY, 2, 2, 2, 2 ) );
  AddFrame( hfrm, new TGLayoutHints( kLHintsExpandX | kLHintsCenterX, 2, 2, 2, 2 ) );
}

BaseRegister::~BaseRegister()
{
}

void BaseRegister::Status( void )
{
  fLED->SetPicture( gClient->GetPicture( Form( "icons/led%d.xpm", kOFF) ) );

  Bool_t flag = kTRUE;

  for ( Int_t i = 0; i < fArr->GetEntries(); i++ ) {
    RegObject *reg = (RegObject *)fArr->At( i );
    if ( !reg ) continue;

    unsigned int val1 = reg->GetValue();
    reg->Scan();
    unsigned int val2 = reg->GetValue();
    if ( val1 != val2 ) flag = kFALSE;
  }

  if ( flag && fSync )
    fLED->SetPicture( gClient->GetPicture( Form( "icons/led%d.xpm", kON ) ) );
}

void BaseRegister::Set( void )
{
  fLED->SetPicture( gClient->GetPicture( Form( "icons/led%d.xpm", kOFF) ) );

  if ( !gCmndSnder->IsConnected() ) {
    fSync = false;
    return;
  }

  for ( Int_t i = 0; i < fArr->GetEntries(); i++ ) {
    RegObject *reg = (RegObject *)fArr->At( i );
    if ( !reg ) continue;
    reg->Scan();
    reg->SendCommand( CMD_WRTE );
  }

  fSync = true;

  fLED->SetPicture( gClient->GetPicture( Form( "icons/led%d.xpm", kON ) ) );
}

void BaseRegister::Get( void )
{
  fLED->SetPicture( gClient->GetPicture( Form( "icons/led%d.xpm", kOFF) ) );

  if ( !gCmndSnder->IsConnected() ) {
    std::cout << "<BaseRegister::Get> Sync failed" << std::endl;

    fSync = false;
    return;
  }

  for ( Int_t i = 0; i < fArr->GetEntries(); i++ ) {
    RegObject *reg = (RegObject *)fArr->At( i );
    if ( !reg ) continue;
    reg->SendCommand( CMD_READ );
    reg->Show();
  }

  fSync = true;

  fLED->SetPicture( gClient->GetPicture( Form( "icons/led%d.xpm", kON ) ) );
}

void BaseRegister::Add( RegObject *reg )
{
  fArr->Add( reg );
}

void BaseRegister::Load( std::map<int, unsigned int> vals )
{
  fLED->SetPicture( gClient->GetPicture( Form( "icons/led%d.xpm", kOFF) ) );

  for ( Int_t i = 0; i < fArr->GetEntries(); i++ ) {
    RegObject *reg = (RegObject *)fArr->At( i );
    if ( !reg ) continue;
    reg->SetValue( vals[reg->GetAddress()] );
    reg->Show();
  }
}
