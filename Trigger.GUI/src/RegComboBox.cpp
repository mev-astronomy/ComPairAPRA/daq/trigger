#include "RegComboBox.h"

#include "TGComboBox.h"

#include <iostream>

ClassImp( RegComboBox )

RegComboBox::RegComboBox( unsigned char addr ) : RegObject( addr )
{
  fCtype = T_ONEH;
}

RegComboBox::RegComboBox( unsigned char addr, Int_t type ) : RegObject( addr )
{
  fCtype = type;
}

RegComboBox::~RegComboBox()
{
}

void RegComboBox::SetComboBox( TGComboBox *cb ) 
{
  fBox = cb;
}

TGComboBox *RegComboBox::GetComboBox( void )
{
  return fBox;
}

void RegComboBox::Scan( void )
{
  Int_t val = fBox->GetSelected();
  //  Int_t val = 0;
  cmdPacket.data[0] = 0x0;
  cmdPacket.data[1] = 0x0;

  /*
  for ( Int_t j = 0; j < 2; j++ ) {
    for ( Int_t i = 0; i < 8; i++ ) {
      if ( val == j*8+i+1 ) cmdPacket.data[(j+1)%2] |= (1<<i);
    }
  }
  */

  if ( fCtype == T_ONEH ) {
    if ( val <= 8 && val >= 1 ) cmdPacket.data[1] = ( 1 << (val-1) );
  }
  else if ( fCtype == T_BINA ) {
    cmdPacket.data[1] = val;
  }
  else {
    std::cout << "[Warning] RegComboBox::Scan() Unknown counter type" << std::endl;
  }
  
#ifdef DEBUG
  std::cout << "RegComboBox::Scan() val = " << std::hex << val << std::endl;
  std::cout << "cmdPacket.data[0] = " << std::hex << (unsigned int)cmdPacket.data[0] << std::endl;
  std::cout << "cmdPacket.data[1] = " << std::hex << (unsigned int)cmdPacket.data[1] << std::endl;
#endif
}

void RegComboBox::Show( void )
{
  UInt_t val = 0;

  if ( fCtype == T_ONEH ) {
    // for External off (exception)
    if ( cmdPacket.data[0] == 0 && cmdPacket.data[1] == 0 ) {
      fBox->Select( 9, kFALSE );
      return;
    }
    
    for ( Int_t j = 0; j < 2; j++ ) {
      for ( Int_t i = 0; i < 8; i++ ) {
	if ( cmdPacket.data[(j+1)%2] & (1<<i) ) val = i+j*8+1;
      }
    }
  }
  else if ( fCtype == T_BINA ) {
    val = cmdPacket.data[1] + ( cmdPacket.data[0] << 8 );
  }
  else {
    std::cout << "[Warning] RegComboBox::Show() Unknown counter type" << std::endl;
  }
  
  fBox->Select( (Long_t)val, kFALSE );
}
