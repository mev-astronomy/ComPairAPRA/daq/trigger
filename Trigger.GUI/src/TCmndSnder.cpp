#include "TCmndSnder.h"

#include "TString.h"

#include <iostream>

ClassImp( TCmndSnder )

TCmndSnder *gCmndSnder = 0;

TCmndSnder::TCmndSnder( int m ) : TcpSocket( m )
{
  gCmndSnder = this;
}

TCmndSnder::~TCmndSnder()
{
}

void TCmndSnder::Init( void )
{
  CreateBuffer( BUFS );
  //  CreateSocket();
  //  SetHisAddr( TCP_CMND_PORT, SRVIP );
}

void TCmndSnder::Connect( void )
{
  CreateSocket();
  SetHisAddr( TCP_CMND_PORT, SRVIP );
  ConnectSocket();

  StartThread();
}

Bool_t TCmndSnder::Connect( const char *signal, const char *receiver_class, void *receiver, const char *slot )
{
  return TQObject::Connect( signal, receiver_class, receiver, slot );
}

bool TCmndSnder::SendCommand( struct CmndPacket *cmdPacket )
{
  if ( GetStatus() > 0 ) {
    std::cout << "<TCmndSnder::SetCommand> Clear status" << std::endl;
    SetStatus( 0 );
  }

  if ( Send( (void *)cmdPacket, 4 ) < 0 ) {
    std::cerr << Form( "Send failed: %s", ParseCommand( cmdPacket ) ) << std::endl;
    AddText( Form( "Command send failed: %s", ParseCommand( cmdPacket ) ) );
    return false;
  }

  AddText( Form( "Command sent: %s", ParseCommand( cmdPacket ) ) );

  usleep( 10000 );

  int i = 0;
  do {
    if ( GetStatus() > 0 ) {
      Acknowledge( cmdPacket );
      SetStatus( 0 );
      break;
    }
    else usleep( 10000 );
  } while ( 1 && ++i < N_TRY );

  if ( i == N_TRY ) {
    i = 0;
    do {
      if ( GetStatus() > 0 ) {
	Acknowledge( cmdPacket );
	SetStatus( 0 );
	break;
      }
      else usleep( 100000 );
    } while ( 1 && ++i < N_TRY );

    if ( i == N_TRY ) {
      std::cout << "<TCmndSnder::SendCommand> failed. i = " << i << std::endl;
      return false;
    }
  }

  return true;
}

void TCmndSnder::Acknowledge( struct CmndPacket *cmdPacket )
{
  unsigned char *buffer = GetBuffer();

  if ( cmdPacket->cmnd == *(buffer+0) && cmdPacket->addr == *(buffer+1) ) {
    cmdPacket->data[0] = *(buffer+2);
    cmdPacket->data[1] = *(buffer+3);
    // std::cout << Form( "Acknowledge: Success %s", ParseCommand( cmdPacket ) ) << std::endl;
    AddEcho( Form( "Acknowledge: Success %s", ParseCommand( cmdPacket ) ) );
  }
  else {
    std::cout << Form( "Acknowledge: Failed %s", ParseCommand( cmdPacket ) ) << std::endl;
    AddEcho( Form( "Acknowledge: Failed %s", ParseCommand( cmdPacket ) ) );
  }
}

const char *TCmndSnder::ParseCommand( struct CmndPacket *cmdPacket )
{
  return Form( "<%02x %02x %02x %02x>", (unsigned int)cmdPacket->cmnd, (unsigned int)cmdPacket->addr, (unsigned int )cmdPacket->data[0], (unsigned int )cmdPacket->data[1] );
}


void TCmndSnder::AddEcho( const char *text )
{
  Emit( "AddEcho(const char *)", text );
}

void TCmndSnder::AddText( const char *text )
{
  Emit( "AddText(const char *)", text );
}
