#include "ModeRegister.h"
#include "RegRadioButton.h"
#include "RegCheckButton.h"
#include "RegNumberEntry.h"

#include "TGButton.h"
#include "TGButtonGroup.h"
#include "TGNumberEntry.h"

ClassImp( ModeRegister )

ModeRegister::ModeRegister( const TGWindow *p, TGString *title, UInt_t options ) : BaseRegister( p, title, options )
{
}

ModeRegister::~ModeRegister()
{
}

void ModeRegister::Init( void )
{  
  fSels = (RegRadioButton *)fArr->At(0);
  fSels->SetNumOfButton( N_SELS );

  fMode = (RegCheckButton *)fArr->At(1);
  fMode->SetNumOfButton( N_MODE );

  for ( Int_t i = 0; i < fArr->GetEntries()-2; i++ ) {
    fNumb[i] = (RegNumberEntry *)fArr->At( i+2 );
  }

  TGButtonGroup *fG1 = new TGButtonGroup( this, "SIT_OR_SEL" );
  fG1->SetLayoutHints( new TGLayoutHints( kLHintsExpandX, 0, 0, 0, 0 ) );

  TGRadioButton *bt0 = new TGRadioButton( fG1, new TGHotString( "SIT_ONE_HIT" ), 0 );
  fG1->AddFrame( bt0, new TGLayoutHints( kLHintsExpandX, 0, 0, 0, 0 ) );
  bt0->Connect( "Clicked()", "ModeRegister", this, "Status()" );
  fSels->SetButton( 0, bt0 );
  TGRadioButton *bt1 = new TGRadioButton( fG1, new TGHotString( "SIT_AND_HIT" ), 1 );
  fG1->AddFrame( bt1, new TGLayoutHints( kLHintsExpandX, 0, 0, 0, 0 ) );
  bt1->Connect( "Clicked()", "ModeRegister", this, "Status()" );
  fSels->SetButton( 1, bt1 );
  TGRadioButton *bt2 = new TGRadioButton( fG1, new TGHotString( "SIT_TWO_HIT" ), 2 );
  fG1->AddFrame( bt2, new TGLayoutHints( kLHintsExpandX, 0, 0, 0, 0 ) );
  bt2->Connect( "Clicked()", "ModeRegister", this, "Status()" );
  fSels->SetButton( 2, bt2 );
  bt0->SetState(kButtonDown);

  AddFrame( fG1, new TGLayoutHints( kLHintsExpandX, 0, 0, 0, 0 ) );

  for ( UInt_t i = 0; i < N_MODE; i++ ) {
    TGHorizontalFrame *hfrm = new TGHorizontalFrame( this, 240, 120, kDoubleBorder );

    TGCheckButton *bt = new TGCheckButton( hfrm, new TGHotString( Form( "Mode-%X   1 /", i ) ) );
    bt->Connect( "Clicked()", "ModeRegister", this, "Status()" );
    hfrm->AddFrame( bt, new TGLayoutHints( kLHintsExpandX | kLHintsCenterY, 0, 0, 0 ) );
    fMode->SetButton( i, bt );

    TGNumberEntry *ne = new TGNumberEntry( hfrm, 1, 4, -1, TGNumberEntry::kNESInteger, TGNumberEntry::kNEANonNegative, TGNumberEntry::kNELLimitMinMax, 1, 255 );
    ne->Connect( "ValueSet(Long_t)", "ModeRegister", this, "Status()" );
    hfrm->AddFrame( ne, new TGLayoutHints( kLHintsRight, 2, 2, 3, 3 ));
    fNumb[i]->SetNumberEntry( ne );

    AddFrame( hfrm, new TGLayoutHints( kLHintsExpandX | kLHintsExpandY, 0, 0, 0, 0 ) );
  }
}
