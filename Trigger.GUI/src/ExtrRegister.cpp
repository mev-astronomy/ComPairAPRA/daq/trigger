#include "ExtrRegister.h"
#include "RegComboBox.h"
#include "RegNumberEntry.h"

#include "TGComboBox.h"
#include "TGNumberEntry.h"
#include "TGLabel.h"

ClassImp( ExtrRegister )

ExtrRegister::ExtrRegister( const TGWindow *p, TGString *title, UInt_t options ) : BaseRegister( p, title, options )
{
}

ExtrRegister::~ExtrRegister()
{
}

void ExtrRegister::Init( void )
{  
  // External Trigger rate
  TString rough[9] = {
    " 1/(16^1)", " 1/(16^2)", " 1/(16^3)", " 1/(16^4)",
    " 1/(16^5)", " 1/(16^6)", " 1/(16^7)", " 1/(16^8)", "N/A" };

  TString rfine[9] = {
    "128", " 64", " 32", " 16", "  8", "  4", "  2", "  1", "  0" };

  fRough = (RegComboBox *)fArr->At( 0 );
  fRfine = (RegComboBox *)fArr->At( 1 );

  TGHorizontalFrame *hfrm 
    = new TGHorizontalFrame( this, 240, 240, kDoubleBorder );

  TGComboBox *cb1 = new TGComboBox( hfrm );
  for ( Int_t i = 0; i < 9; i++ ) {
    cb1->AddEntry( rfine[i].Data(), i+1 );
  }
  cb1->Resize( 45, 26 );
  cb1->Select( 1 );
  cb1->Connect( "Selected(Int_t)", "ExtrRegister", this, "Status()" );

  hfrm->AddFrame( cb1, new TGLayoutHints( kLHintsLeft, 0, 0, 0, 0 ) );

  TGLabel *erough = new TGLabel( hfrm, Form( " x " ) );
  hfrm->AddFrame( erough, new TGLayoutHints( kLHintsExpandX | kLHintsBottom, 0, 0, 0, 3 ) );

  TGComboBox *cb2 = new TGComboBox( hfrm );
  for ( Int_t i = 0; i < 9; i++ ) {
    cb2->AddEntry( rough[i].Data(), i+1 );
  }
  cb2->Resize( 80, 26 );
  cb2->Select( 1 );
  cb2->Connect( "Selected(Int_t)", "ExtrRegister", this, "Status()" );

  hfrm->AddFrame( cb2, new TGLayoutHints( kLHintsLeft, 0, 0, 0, 0 ) );
  AddFrame( hfrm, new TGLayoutHints( kLHintsLeft, 0, 0, 0, 0 ) );

  fRfine->SetComboBox( cb1 );
  fRough->SetComboBox( cb2 );
}
