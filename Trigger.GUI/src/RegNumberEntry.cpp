#include "RegNumberEntry.h"

#include "TGNumberEntry.h"

#include <iostream>

//#define DEBUG

ClassImp( RegNumberEntry )

RegNumberEntry::RegNumberEntry( unsigned char addr ) : RegObject( addr )
{
}

RegNumberEntry::~RegNumberEntry()
{
}

void RegNumberEntry::SetNumberEntry( TGNumberEntry *ne ) 
{
  fEntry = ne;
}

TGNumberEntry *RegNumberEntry::GetNumberEntry( void )
{
  return fEntry;
}

void RegNumberEntry::Scan( void )
{
  UInt_t val = (UInt_t)fEntry->GetIntNumber();

  cmdPacket.data[0] = ( 0xFF & (val >> 8) );
  cmdPacket.data[1] = ( 0xFF & (val >> 0) );

#ifdef DEBUG
  std::cout << "RegNumberEntry::Scan() val = " << std::hex << val << std::endl;
  std::cout << "cmdPacket.data[0] = " << std::hex << (unsigned int)cmdPacket.data[0] << std::endl;
  std::cout << "cmdPacket.data[1] = " << std::hex << (unsigned int)cmdPacket.data[1] << std::endl;
#endif

}

void RegNumberEntry::Show( void )
{
  UInt_t val = (cmdPacket.data[0] << 8) + cmdPacket.data[1];

  fEntry->SetIntNumber( (Long_t)val );
}
