#include "ConnRegister.h"
#include "RegCheckButton.h"

ClassImp( ConnRegister )

ConnRegister::ConnRegister( const TGWindow *p, TGString *title, UInt_t options ) : BaseRegister( p, title, options )
{
}

ConnRegister::~ConnRegister()
{
}

void ConnRegister::Init( void )
{  
  TString label[8] = {
    "Trigger Ack   ", "Event ID Clock", 
    "Event ID Data ", "Trigger Ena   ", 
    "FEE Hit       ", "FEE Ready     ", 
    "FEE Busy      ", "FEE Spare     " };

  fConn = (RegCheckButton *)fArr->At(0);
  fConn->SetNumOfButton( N_ITEM );

  for ( UInt_t i = 0; i < N_ITEM; i++ ) {
    TGCheckButton *bt = new TGCheckButton( this, new TGHotString( Form( "%s", label[i].Data() ) ) );
    bt->Connect( "Clicked()", "ConnRegister", this, "Status()" );
    AddFrame( bt, new TGLayoutHints( 0, 0, 0, 0 ) );
    fConn->SetButton( i, bt );
  }
}
