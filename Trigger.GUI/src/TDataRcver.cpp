#include "TDataRcver.h"
#include "HistBranch.h"
#include "EvntBranch.h"

#include "TString.h"
#include "TSystem.h"

#include "TGProgressBar.h"

#include <sys/time.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <iostream>

TDataRcver *gDataRcver = 0;

TDataRcver::TDataRcver()
{
  fTime = 0.0;

  fNrcv = 0;
  fTcps = 0;
  fDout = 0;

  fTotl = 0;

  fEvnt = 0;
  fHist = 0;

  fHsel = 0;

  // -- 2020Sep20
  //  fHead.atrb[0] = 0xC0;
  fHead.atrb[0] = 0x40;
  fHead.atrb[1] = 0x00;
  // 2020Sep20 --

  gDataRcver = this;
}

TDataRcver::~TDataRcver()
{
  if ( fTcps ) delete fTcps;
  if ( fTree ) delete fTree;
  if ( fEvnt ) delete fEvnt;
  if ( fHist ) delete fHist;
}

void TDataRcver::Init( void )
{
#ifdef TCP_RCVER
  fTcps = new TcpSocket( TcpSocket::CLT );
#else
  std::cout << "<TDataRcver::Init> UDP" << std::endl;
  fTcps = new UdpSocket( UdpSocket::CLT );
#endif

  fTcps->CreateBuffer( BUFS );

  fEvnt = new EvntBranch();
  fHist = new HistBranch();

  fTree = new TTree( "ttree", "Trigger Event" );
  fTree->Branch( "fTime", &fTime, "fTime/D" );
  fTree->Branch( "fEvnt", "EvntBranch", &fEvnt );
}

void TDataRcver::Connect( void )
{
#ifdef TCP_RCVER
  fTcps->CreateSocket();
  fTcps->SetHisAddr( TCP_DATA_PORT, SRVIP );
  fTcps->ConnectSocket();
#else
  fTcps->CreateSocket();
  fTcps->SetHisAddr( UDP_DATA_PORT, SRVIP );
  fTcps->ConnectSocket();
  fTcps->Send( (void *)"connect", 8 );
#endif

  Open();

  StartThread();
}

void TDataRcver::Process( void )
{
  fHist->SetHist( (unsigned int *)fTcps->GetBuffer() );
}

void TDataRcver::DrawHist( int i )
{
  fHist->DrawHist( i );
}

void TDataRcver::FillData( void )
{
  Write();
  fEvnt->SetEvent( (unsigned int *)fTcps->GetBuffer() );
}

void TDataRcver::Write( void )
{
  if ( fDout < 0 ) return;

  unsigned int npkt = fTcps->GetRcvLen()/N_SENT;
  if ( npkt < 1 ) return;

  char buf_size[2];
  // -- 2019Aug28
  //  buf_size[0] =      0;
  //  buf_size[1] = N_SENT+N_HEAD;
  buf_size[0] =      1; // N_SENT = 256
  buf_size[1] = N_HEAD;
  // 2019Aug28 --

  unsigned char *buffer = fTcps->GetBuffer();
  unsigned char *header = GetHeader();

  for ( unsigned int i = 0; i < npkt; i++ ) {
    int ret = write( fDout, buf_size, 2 );
    ret    += write( fDout, (void *)header, N_HEAD );
    ret    += write( fDout, (void *)(buffer+i*N_SENT), N_SENT );
    ret    += write( fDout, buf_size, 2 );
    fTotl  += ret;
  }

  if ( fTotl > N_TOTL ) {
    std::cout << "<TDataRcver::Write> Change File" << std::endl;
    Open();
  }
}


unsigned char *TDataRcver::GetHeader( void )
{
  struct timeval time;
  gettimeofday( &time, NULL );

  fTime = time.tv_sec*1.0 + time.tv_usec*1.0e-6;
  memcpy( (void *)&(fHead.time[0]), &time.tv_sec, 4 );
  memcpy( (void *)&(fHead.time[4]), &time.tv_usec, 4 );

  return (unsigned char *)&fHead;
}

void TDataRcver::Load( const char *fname, TGHProgressBar *fHProg )
{
  Long_t id;
  Long_t size;
  Long_t flags;
  Long_t modtime;
  gSystem->GetPathInfo( fname, &id, &size, &flags, &modtime );

  UInt_t evsize = 2 + N_HEAD + N_SENT + 2;
  UInt_t nevt = size / evsize;

  int fDin = open( fname, O_RDONLY );
  if ( fDin <= 0 ) return;

  fHProg->SetRange( 0.0, 1.0*nevt );
  fHProg->ShowPos( kTRUE );

  int  i_buf_size_a;
  int  i_buf_size_b; 
  unsigned char buf_size_a[2];
  unsigned char buf_size_b[2];

  struct DataHeader fHead;
  unsigned int event[512];

  Double_t fPrev = -1.0;

  UInt_t ipre = 0;
  for ( UInt_t ievt = 0; ievt < nevt; ievt++ ) {
    int ret = read( fDin, buf_size_a, 2 );
    if ( ret == 2 ) {
      i_buf_size_a = (buf_size_a[0] << 8) + (buf_size_a[1]);
      
      ret += read( fDin, (void *)&fHead, PKT_ATRB_SIZE+PKT_TIME_SIZE );
      ret += read( fDin, (void *)&event, i_buf_size_a - (PKT_ATRB_SIZE+PKT_TIME_SIZE) );

      fTime = fHead.time[0]+(fHead.time[1]<<8)+(fHead.time[2]<<16)+(fHead.time[3]<<24)+1e-6*(fHead.time[4]+(fHead.time[5]<<8)+(fHead.time[6]<<16)+(fHead.time[7]<<24));
      
      if ( fPrev < 0 || fTime-fPrev > 2.0 ) {
	fHist->SetHist( (unsigned int *)&event[0] );
	fPrev = fTime;
	fHProg->Increment( 100.0*(ievt-ipre+1)/nevt );
	fHProg->ShowPos( kTRUE );
	ipre = ievt;
      }

      ret += read( fDin, buf_size_b, 2 );
      i_buf_size_b = (buf_size_b[0] << 8) + (buf_size_b[1]);
      
      if ( (i_buf_size_a != i_buf_size_b) || ret <= 0 ) return;

    }
    else return;
  }
}

void TDataRcver::Open( void )
{
  if ( fDout > 0 ) close( fDout );
  fTotl = 0;

  time_t lt;
  time( &lt );
  struct tm *ptr = localtime( &lt );
  int year = ptr->tm_year+1900;
  int mon  = ptr->tm_mon+1;
  int mday = ptr->tm_mday;
  int hour = ptr->tm_hour;
  int min  = ptr->tm_min;
  //  int sec  = ptr->tm_sec;
  
  string name = Form( "raw/%s-%04d-%02d%02d-%02d%02d", "trg_raw", 
		      year, mon, mday, hour, min );
  int file_num = 1;
  
  string full_name;
  
  do {
    full_name = name+Form( "-%03d.dat", file_num++ );
    fDout = open( full_name.c_str(), O_RDONLY );
  } while ( fDout > 0 );

  fDout = open( full_name.c_str(), O_CREAT | O_TRUNC | O_RDWR, 0666 );
  if ( fDout < 0 ) {
    std::cout << Form( "Error in opening file : %s", full_name.c_str() ) << std::endl;
  }
}

void TDataRcver::PrintStatus( void )
{
  fTcps->PrintStatus();
}

void TDataRcver::Close( void )
{
  if ( fDout < 0 ) return;
  close( fDout );
}

void TDataRcver::Clear( void )
{
  fHist->Clear();
}

void TDataRcver::RunThread( void )
{
  fTcps->StartThread();

  do {
    if ( fTcps->GetStatus() ) {
      fNrcv++;
      FillData();
      fTcps->SetStatus( 0 );
    }
  } while ( status >= 0 );
}
