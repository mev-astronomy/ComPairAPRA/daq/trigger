#include "TApplication.h"
#include "TGClient.h"
#include "AmMainFrame.h"

int main( int argc, char **argv ) {
  TApplication theApp( "App", &argc, argv );
  new AmMainFrame( gClient->GetRoot(), 400, 400 );
  theApp.Run();
  return 0;
}
