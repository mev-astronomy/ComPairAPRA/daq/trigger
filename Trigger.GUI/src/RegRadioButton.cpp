#include "RegRadioButton.h"

#include "TGButton.h"

#include <iostream>

ClassImp( RegRadioButton )

RegRadioButton::RegRadioButton( unsigned char addr ) : RegObject( addr )
{
  fNbutton = 0;
}

RegRadioButton::~RegRadioButton()
{

}

void RegRadioButton::SetButton( UInt_t i, TGRadioButton *cb ) 
{
  if ( i < 0 || i >= fNbutton ) return;
  fButton[i] = cb;
}

TGRadioButton *RegRadioButton::GetButton( UInt_t i )
{
  if ( i < 0 || i >= fNbutton ) return 0;
  return fButton[i];
}

void RegRadioButton::Scan( void )
{
  UInt_t val = 0;

  for ( UInt_t i = 0; i < fNbutton; i++ ) {
    if ( fButton[i]->IsDown() ) val += 1 << i;
  }

  cmdPacket.data[0] = ( 0xFF & (val >> 8) );
  cmdPacket.data[1] = ( 0xFF & (val >> 0) );

#ifdef DEBUG
  std::cout << "RegRadioButton::Scan() val = " << std::hex << val << std::endl;
  std::cout << "fNbutton = " << std::hex << fNbutton << std::endl;
  std::cout << "cmdPacket.data[0] = " << std::hex << (unsigned int)cmdPacket.data[0] << std::endl;
  std::cout << "cmdPacket.data[1] = " << std::hex << (unsigned int)cmdPacket.data[1] << std::endl;
#endif
}

void RegRadioButton::Show( void )
{
  UInt_t val = (cmdPacket.data[0] << 8) + cmdPacket.data[1];

  for ( UInt_t i = 0; i < fNbutton; i++ ) {
    if ( val & (1 << i) ) fButton[i]->SetDown( true  );
    else                  fButton[i]->SetDown( false );
  }
}
