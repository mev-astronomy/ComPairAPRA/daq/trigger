#include "EvidRegister.h"
#include "RegNumberEntry.h"

#include "TGNumberEntry.h"
#include "TGLabel.h"

ClassImp( EvidRegister )

EvidRegister::EvidRegister( const TGWindow *p, TGString *title, UInt_t options ) : BaseRegister( p, title, options )
{
}

EvidRegister::~EvidRegister()
{
}

void EvidRegister::Init( void )
{  
  fDelay = (RegNumberEntry *)fArr->At( 0 );
  fWidth = (RegNumberEntry *)fArr->At( 1 );

  TGHorizontalFrame *hfrm
    = new TGHorizontalFrame( this, 480, 120, kDoubleBorder );
  
  TGLabel *dlb = new TGLabel( hfrm, "TrgAck" );
  hfrm->AddFrame( dlb, new TGLayoutHints( kLHintsBottom | kLHintsExpandX, 0, 0, 0, 0 ) );
  TGNumberEntry *dne = new TGNumberEntry( hfrm, 0, 3, -1, TGNumberEntry::kNESInteger, TGNumberEntry::kNEANonNegative, TGNumberEntry::kNELLimitMinMax, 0, 255 );
  dne->Connect( "ValueSet(Long_t)", "EvidRegister", this, "Status()" );
  hfrm->AddFrame( dne, new TGLayoutHints( 0, 0, 0, 3, 0 ) );
  fDelay->SetNumberEntry( dne );

  TGLabel *wlb = new TGLabel( hfrm, "EvID" );
  hfrm->AddFrame( wlb, new TGLayoutHints( kLHintsBottom | kLHintsExpandX, 0, 0, 0, 0 ) );
  TGNumberEntry *wne = new TGNumberEntry( hfrm, 0, 3, -1, TGNumberEntry::kNESInteger, TGNumberEntry::kNEANonNegative, TGNumberEntry::kNELLimitMinMax, 0, 255 );
  wne->Connect( "ValueSet(Long_t)", "EvidRegister", this, "Status()" );
  hfrm->AddFrame( wne, new TGLayoutHints( 0, 0, 0, 3, 0 ) );
  fWidth->SetNumberEntry( wne );

  AddFrame( hfrm, new TGLayoutHints( kLHintsExpandX, 3, 0, 0, 0 ) );
}
