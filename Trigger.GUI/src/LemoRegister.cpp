#include "LemoRegister.h"
#include "RegCheckButton.h"

ClassImp( LemoRegister )

LemoRegister::LemoRegister( const TGWindow *p, TGString *title, UInt_t options ) : BaseRegister( p, title, options )
{
}

LemoRegister::~LemoRegister()
{
}

void LemoRegister::Init( void )
{  
  TString label[N_ITEM] = {
    "LPC Ext Ack  ", "LPC Ext Ready",
    "LPC Out [0]  ", "LPC Out [1]  ",
    "HPC Ext Ack  ", "HPC Ext Ready",
    "HPC Out [0]  ", "HPC Out [1]  " };

  fLemo = (RegCheckButton *)fArr->At(0);
  fLemo->SetNumOfButton( N_ITEM );

  TGHorizontalFrame *hfrm = new TGHorizontalFrame( this, 480, 580, 0 );
  
  for ( UInt_t i = 0; i < N_ITEM; i++ ) {
    TGCheckButton *bt = new TGCheckButton( hfrm, new TGHotString( Form( "%s", label[i].Data() ) ) );
    bt->Connect( "Clicked()", "LemoRegister", this, "Status()" );
    hfrm->AddFrame( bt, new TGLayoutHints( kLHintsExpandX, 0, 0, 0 ) );
    fLemo->SetButton( i, bt );
  }
  AddFrame( hfrm, new TGLayoutHints( kLHintsExpandX, 0, 0, 0, 0 ) );
}
