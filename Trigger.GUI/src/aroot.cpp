#include "Packet.h"
#include "EvntBranch.h"

#include "TFile.h"
#include "TTree.h"

#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <iostream>

#define LNGS 64

int main( int argc, char **argv ) {
  int fDin = 0;

  if ( argc > 1 ) {
    fDin = open( argv[1], O_RDONLY );
  }
  else {
    fDin = open( "raw/trg_raw-0119-0604-1601-001.dat", O_RDONLY );
  }

  int  i_buf_size_a;
  int  i_buf_size_b; 
  unsigned char buf_size_a[2];
  unsigned char buf_size_b[2];

  struct DataHeader fHead;
  unsigned int event[LNGS];
  Double_t    fTime;
  EvntBranch *fEvnt = new EvntBranch;

  bool flag = true;

  TTree *ttree = new TTree( "ttree", "Trigger Event Data" );
  ttree->Branch( "fTime", &fTime, "fTime/D" );
  ttree->Branch( "fEvnt", "EvntBranch", &fEvnt );

  int i = 0; 
  while ( flag && i++ < 1447660 ) {
    int ret = read( fDin, buf_size_a, 2 );
    if ( ret == 2 ) {
      i_buf_size_a = (buf_size_a[0] << 8) + (buf_size_a[1]);
      //    std::cout << i_buf_size_a << std::endl;
      
      ret += read( fDin, (void *)&fHead, PKT_ATRB_SIZE+PKT_TIME_SIZE );
      ret += read( fDin, (void *)&event, i_buf_size_a - (PKT_ATRB_SIZE+PKT_TIME_SIZE) );
      /*
      if ( i > 710670 ) {
	for ( int j = 0; j < 32; j++ ) {
	  std::cout << Form( "%10u ", event[j] );
	  if ( j%8 == 7 ) std::cout << std::endl;
	}
	std::cout << std::endl;
      }
      */
      
      fEvnt->SetEvent( &event[0] );
      
      ret += read( fDin, buf_size_b, 2 );
      i_buf_size_b = (buf_size_b[0] << 8) + (buf_size_b[1]);
      //    std::cout << i_buf_size_b << std::endl;
      
      fTime = fHead.time[0]+(fHead.time[1]<<8)+(fHead.time[2]<<16)+(fHead.time[3]<<24)+1e-6*(fHead.time[4]+(fHead.time[5]<<8)+(fHead.time[6]<<16)+(fHead.time[7]<<24));
      
      /*
	std::cout << "time = " << time << std::endl;
	std::cout << "time s = " << fHead.time[0]+(fHead.time[1]<<8)+(fHead.time[2]<<16)+(fHead.time[3]<<24) << std::endl;
	std::cout << "time u = " << 1e-6*(fHead.time[4]+(fHead.time[5]<<8)+(fHead.time[6]<<16)+(fHead.time[7]<<24)) << std::endl;
      */
      if ( (i_buf_size_a != i_buf_size_b) || ret <= 0 ) flag = false;
      ttree->Fill();
    }
  }

  std::cout << "nevent = " << ttree->GetEntries() << std::endl;

  TFile of( "trg_dst.root", "recreate" );
  ttree->Write();
  of.Close();
    
  return 0;
}
