#include "HistBranch.h"
#include "EvntBranch.h"

#include "TDataRcver.h"

#include "TPad.h"
#include "TGraph.h"
#include "TH1F.h"
#include "TH2F.h"

#include <iostream>

ClassImp( HistBranch )

HistBranch::HistBranch()
{
  fTime   = 0.0;
  fPeriod = 0.0;

  fLiveTM = new TGraph();
  SetGraph( fLiveTM, 0 );
  fTrgLck = new TGraph();
  SetGraph( fTrgLck, 1 );
  fTrgAck = new TGraph();
  SetGraph( fTrgAck, 0 );
  fSwtAck = new TGraph();
  SetGraph( fSwtAck, 1 );
  fExtAck = new TGraph();
  SetGraph( fExtAck, 2 );
  fLpcAck = new TGraph();
  SetGraph( fLpcAck, 3 );
  fHpcAck = new TGraph();
  SetGraph( fHpcAck, 4 );
  for ( Int_t i = 0; i < N_MODE; i++ ) {
    fCoMode[i] = new TGraph();
    SetGraph( fCoMode[i], i );
    fCdMode[i] = new TGraph();
    SetGraph( fCdMode[i], i );
  }

  fLivACD = new TGraph();
  SetGraph( fLivACD, 1 );
  fLivCZT = new TGraph();
  SetGraph( fLivCZT, 2 );
  fLivCs1 = new TGraph();
  SetGraph( fLivCs1, 3 );
  fLivCs2 = new TGraph();
  SetGraph( fLivCs2, 4 );
  for ( Int_t i = 0; i < N_SITR; i++ ) {
    fLivSiT[i] = new TGraph();
    SetGraph( fLivSiT[i], i );
  }

  fHitACD = new TGraph();
  SetGraph( fHitACD, 0 );
  fOneSiT = new TGraph();
  SetGraph( fOneSiT, 1 );
  fAndSiT = new TGraph();
  SetGraph( fAndSiT, 2 );
  fTwoSiT = new TGraph();
  SetGraph( fTwoSiT, 3 );
  fHitCZT = new TGraph();
  SetGraph( fHitCZT, 0 );
  f1stCsI = new TGraph();
  SetGraph( f1stCsI, 1 );
  f2ndCsI = new TGraph();
  SetGraph( f2ndCsI, 2 );
  for ( Int_t i = 0; i < N_SITR; i++ ) {
    fHitXsi[i] = new TGraph();
    SetGraph( fHitXsi[i], i );
    fHitYsi[i] = new TGraph();
    SetGraph( fHitYsi[i], i );
  }
  
  CreateHistTrg();
  CreateHistHit();

  for ( Int_t i = 0; i < N_HIST; i++ ) {
    fFrame[i] = 0;
  }
  Clear();
}

HistBranch::~HistBranch()
{
  for ( Int_t i = 0; i < N_HIST; i++ )
    if ( fFrame[i] ) delete fFrame[i];

  if ( fLiveTM ) delete fLiveTM;
  if ( fTrgLck ) delete fTrgLck;
  if ( fTrgAck ) delete fTrgAck;
  if ( fSwtAck ) delete fSwtAck;
  if ( fExtAck ) delete fExtAck;
  if ( fLpcAck ) delete fLpcAck;
  if ( fHpcAck ) delete fHpcAck;
  for ( Int_t i = 0; i < N_MODE; i++ ) {
    if ( fCoMode[i] ) delete fCoMode[i];
    if ( fCdMode[i] ) delete fCdMode[i];
  }
  if ( fLivACD ) delete fLivACD;
  if ( fLivCZT ) delete fLivCZT;
  if ( fLivCs1 ) delete fLivCs1;
  if ( fLivCs2 ) delete fLivCs2;
  for ( Int_t i = 0; i < N_SITR; i++ ) {
    if ( fLivSiT[i] ) delete fLivSiT[i];
  }
  if ( fHitACD ) delete fHitACD;
  if ( fOneSiT ) delete fOneSiT;
  if ( fAndSiT ) delete fAndSiT;
  if ( fTwoSiT ) delete fTwoSiT;
  if ( fHitCZT ) delete fHitCZT;
  if ( f1stCsI ) delete f1stCsI;
  if ( f2ndCsI ) delete f2ndCsI;
  for ( Int_t i = 0; i < N_SITR; i++ ) {
    if ( fHitXsi[i] ) delete fHitXsi[i];
    if ( fHitYsi[i] ) delete fHitYsi[i];
  }
  if ( fAllTrg ) delete fAllTrg;
  if ( fAllHit ) delete fAllHit;
}

void HistBranch::SetFrame ( TGraph *gra, int i )
{
  if ( gra->GetN() <= 1 ) return;

  TString htitle[N_HIST] = {
    "Live Time", "Trigger Ack", "Trigger Mode", "Subsystem Live Time",
    "ACD Hit Rate", "Si-Tracker Hit Rate", "CZT Hit Rate", "CsI Hit Rate", 
    "Si-Tracker X Hit Rate", 
    "Si-Tracker Y Hit Rate", 
    "Trigger hit", 
    "Detector Hit" };

  if ( fFrame[i] ) delete fFrame[i];
  Double_t x1, x2;
  Double_t y1, y2;
  gra->GetPoint( 0, x1, y1 );
  gra->GetPoint( gra->GetN()-1, x2, y2 );
  if ( i == 0 ) fYmax[0] = 0.91;
  if ( fYmax[i] == 0.0 ) fYmax[i] = 0.1;
  if ( fYmax[i] < y1 )   fYmax[i] = y1;
  if ( fYmax[i] < y2 )   fYmax[i] = y2;

  fFrame[i] = new TH2F( Form( "frame_%d", i ), 
			Form( "%s", htitle[i].Data() ), 
			1, x1, x2, 1, 0.01, fYmax[i]*1.2 );
  fFrame[i]->SetStats( 0 );
  if ( i == 10 || i == 11 ) {
    fFrame[i]->GetXaxis()->SetLabelSize( 0.05 );
  }
  else {
    fFrame[i]->GetXaxis()->SetLabelSize( 0.07 );
  }
  fFrame[i]->GetYaxis()->SetLabelSize( 0.07 );
  fFrame[i]->GetXaxis()->SetTimeDisplay( 1 );
  fFrame[i]->GetXaxis()->SetTimeOffset( 0, "local" );
  fFrame[i]->GetXaxis()->SetTimeFormat( "%b%d, %H:%M" );
  fFrame[i]->GetXaxis()->SetNdivisions( 303 );
}

void HistBranch::SetGraph ( TGraph *gra, Int_t i )
{
  Int_t icol = i%8+1;
  Int_t imks = (i/8) ? 25 : 21;
  gra->SetMarkerStyle( imks );
  gra->SetMarkerSize( 0.5 );
  gra->SetMarkerColor( icol );
  gra->SetLineColor( icol );
}

void HistBranch::CreateHistTrg( void )
{
  //  fAllTrg = new TH1F( "HistTrg", "Trigger Hist", 46, 0, 23 );
  fAllTrg = new TH1F( "HistTrg", "Trigger Hist", 52, 0, 26 );
  fAllTrg->SetStats( 0 );
  fAllTrg->GetXaxis()->SetLabelSize( 0.05 );
  fAllTrg->GetYaxis()->SetLabelSize( 0.05 );
  fAllTrg->GetXaxis()->SetBinLabel( 1*2, "Trig. Lock" );
  fAllTrg->GetXaxis()->SetBinLabel( 2*2, "Trig. Ack" );
  fAllTrg->GetXaxis()->SetBinLabel( 3*2, "Switch Ack" );
  fAllTrg->GetXaxis()->SetBinLabel( 4*2, "Ext. Ack" );
  fAllTrg->GetXaxis()->SetBinLabel( 5*2, "LPC Ack" );
  fAllTrg->GetXaxis()->SetBinLabel( 6*2, "HPC Ack" );
  for ( Int_t i = 0; i < N_MODE; i++ ) 
    fAllTrg->GetXaxis()->SetBinLabel( (7+i)*2, Form( "Coin-%X", i ) );
}

void HistBranch::CreateHistHit( void )
{
  fAllHit = new TH1F( "HistHit", "Decector Hit", 64, 0, 32 );
  fAllHit->SetStats( 0 );
  fAllHit->GetXaxis()->SetLabelSize( 0.05 );
  fAllHit->GetYaxis()->SetLabelSize( 0.05 );
  fAllHit->GetXaxis()->SetBinLabel( 1*2, "ACD Hit" );
  fAllHit->GetXaxis()->SetBinLabel( 2*2, "Si-T One Hit" );
  fAllHit->GetXaxis()->SetBinLabel( 3*2, "Si-T And Hit" );
  fAllHit->GetXaxis()->SetBinLabel( 4*2, "Si-T Two Hit" );
  fAllHit->GetXaxis()->SetBinLabel( 5*2, "CZT Hit" );
  fAllHit->GetXaxis()->SetBinLabel( 6*2, "CsI 1st" );
  fAllHit->GetXaxis()->SetBinLabel( 7*2, "CsI 2nd" );
  for ( Int_t i = 0; i < N_SITR; i++ ) {
    fAllHit->GetXaxis()->SetBinLabel( (8+i*2+0)*2, Form( "Si-%X X hit", i ) );
    fAllHit->GetXaxis()->SetBinLabel( (8+i*2+1)*2, Form( "Si-%X Y hit", i ) );
  }
}

void HistBranch::DrawHist( int i )
{
  if ( i < 0 || i >= N_HIST ) return;

  switch ( i ) {
  case 0: DrawLiveTM(); break;
  case 1: DrawTrgAck(); break;
  case 2: DrawTrgAll(); break;
  case 3: DrawLivDet(); break;
  case 4: DrawHitACD(); break;
  case 5: DrawHitSiT(); break;
  case 6: DrawHitCZT(); break;
  case 7: DrawHitCsI(); break;
  case 8: DrawHitXsi(); break;
  case 9: DrawHitYsi(); break;
  case 10: DrawAllTrg(); break;
  case 11: DrawAllHit(); break;
  default: break;
  }
}

void HistBranch::DrawLiveTM( void )
{
  if ( fLiveTM->GetN() <= 1 ) return;

  gPad->SetBottomMargin( 0.1 );
  fFrame[0]->Draw();
  fLiveTM->Draw("PL");
}

void HistBranch::DrawTrgAck( void )
{
  if ( fTrgLck->GetN() <= 1 ) return;

  gPad->SetBottomMargin( 0.1 );
  fFrame[1]->Draw();
  fTrgLck->Draw("PL");
  fTrgAck->Draw("PL");
}

void HistBranch::DrawTrgAll( void )
{
  if ( fSwtAck->GetN() <= 1 ) return;

  gPad->SetBottomMargin( 0.1 );
  fFrame[2]->Draw();
  fSwtAck->Draw("PL");
  fExtAck->Draw("PL");
  fLpcAck->Draw("PL");
  fHpcAck->Draw("PL");
  for ( Int_t i = 0; i < N_MODE; i++ ) {
    if ( fCdMode[i]->GetN() > 1 ) fCdMode[i]->Draw("PL");
  }
}

void HistBranch::DrawLivDet( void )
{
  if ( fLivACD->GetN() <= 1 ) return;

  gPad->SetBottomMargin( 0.1 );
  fFrame[3]->Draw();
  fLivACD->Draw("PL");
  fLivCZT->Draw("PL");
  fLivCs1->Draw("PL");
  fLivCs2->Draw("PL");
  for ( Int_t i = 0; i < N_SITR; i++ )
    fLivSiT[i]->Draw("PL");
}

void HistBranch::DrawHitACD( void )
{
  if ( fHitACD->GetN() <= 1 ) return;

  gPad->SetBottomMargin( 0.1 );
  fFrame[4]->Draw();
  fHitACD->Draw("PL");
}

void HistBranch::DrawHitSiT( void )
{
  if ( fOneSiT->GetN() <= 1 ) return;

  gPad->SetBottomMargin( 0.1 );
  fFrame[5]->Draw();
  fOneSiT->Draw("PL");
  fAndSiT->Draw("PL");
  fTwoSiT->Draw("PL");
}

void HistBranch::DrawHitCZT( void )
{
  if ( fHitCZT->GetN() <= 1 ) return;

  gPad->SetBottomMargin( 0.1 );
  fFrame[6]->Draw();
  fHitCZT->Draw("PL");
}

void HistBranch::DrawHitCsI( void )
{
  if ( f1stCsI->GetN() <= 1 ) return;

  gPad->SetBottomMargin( 0.1 );
  fFrame[7]->Draw();
  f1stCsI->Draw("PL");
  f2ndCsI->Draw("PL");
}

void HistBranch::DrawHitXsi( void )
{
  if ( fHitXsi[0]->GetN() <= 1 ) return;

  gPad->SetBottomMargin( 0.1 );
  fFrame[8]->Draw();

  for ( Int_t i = 0; i < N_SITR; i++ )
    fHitXsi[i]->Draw("PL");
}

void HistBranch::DrawHitYsi( void )
{
  if ( fHitYsi[0]->GetN() <= 1 ) return;

  gPad->SetBottomMargin( 0.1 );
  fFrame[9]->Draw();

  for ( Int_t i = 0; i < N_SITR; i++ )
    fHitYsi[i]->Draw("PL");
}

void HistBranch::DrawAllTrg( void )
{
  gPad->SetBottomMargin( 0.2 );
  fAllTrg->Draw( "HIST" );
}

void HistBranch::DrawAllHit( void )
{
  gPad->SetBottomMargin( 0.2 );
  fAllHit->Draw( "HIST" );
}

void HistBranch::SetHist( unsigned int *data )
{
  fEvnt.SetEvent( data );

  fTime   = gDataRcver->GetTime();
  fPeriod = fEvnt.GetNclkRaw()*20.0e-9;

  if ( fPeriod == 0.0 || fPeriod > 1500000000.0 ) return;
  
  SetLiveTM();
  SetTrgLck();
  SetTrgAck();
  SetSwtAck();
  SetExtAck();
  SetLpcAck();
  SetHpcAck();
  for ( Int_t i = 0; i < N_MODE; i++ ) {
    SetCoMode( i );
    SetCdMode( i );
  }
  SetLivACD();
  SetLivCZT();
  SetLivCs1();
  SetLivCs2();
  for ( Int_t i = 0; i < N_SITR; i++ ) {
    SetLivSiT( i );
  }
  SetHitACD();
  SetOneSiT();
  SetAndSiT();
  SetTwoSiT();
  SetHitCZT();
  Set1stCsI();
  Set2ndCsI();
  for ( Int_t i = 0; i < N_SITR; i++ ) {
    SetHitXsi( i );
    SetHitYsi( i );
  }
  SetAllTrg();
  SetAllHit();
}

void HistBranch::SetLiveTM( void )
{
  if ( fEvnt.GetNclkEna() > 0 ) {
    fLiveTM->SetPoint( fLiveTM->GetN(), fTime, 1.0*fEvnt.GetNclkLiv()/fEvnt.GetNclkEna() );
    SetFrame( fLiveTM, 0 );
  }
}

void HistBranch::SetTrgLck( void )
{
  fTrgLck->SetPoint( fTrgLck->GetN(), fTime, 1.0*fEvnt.GetNtrgLck()/fPeriod );
  SetFrame( fTrgLck, 1 );
}

void HistBranch::SetTrgAck( void )
{
  fTrgAck->SetPoint( fTrgAck->GetN(), fTime, 1.0*fEvnt.GetNtrgAck()/fPeriod );
  SetFrame( fTrgAck, 1 );
}

void HistBranch::SetSwtAck( void )
{
  fSwtAck->SetPoint( fSwtAck->GetN(), fTime, 1.0*fEvnt.GetNswtAck()/fPeriod );
  SetFrame( fSwtAck, 2 );
}

void HistBranch::SetExtAck( void )
{
  fExtAck->SetPoint( fExtAck->GetN(), fTime, 1.0*fEvnt.GetNextAck()/fPeriod );
  SetFrame( fExtAck, 2 );
}

void HistBranch::SetLpcAck( void )
{
  fLpcAck->SetPoint( fLpcAck->GetN(), fTime, 1.0*fEvnt.GetNlpcAck()/fPeriod );
  SetFrame( fLpcAck, 2 );
}

void HistBranch::SetHpcAck( void )
{
  fHpcAck->SetPoint( fHpcAck->GetN(), fTime, 1.0*fEvnt.GetNhpcAck()/fPeriod );
  SetFrame( fHpcAck, 2 );
}

void HistBranch::SetCoMode( int i )
{
  fCoMode[i]->SetPoint( fCoMode[i]->GetN(), fTime, 1.0*fEvnt.GetNcoMode( i )/fPeriod );
}

void HistBranch::SetCdMode( int i )
{
  fCdMode[i]->SetPoint( fCdMode[i]->GetN(), fTime, 1.0*fEvnt.GetNcdMode( i )/fPeriod );
  SetFrame( fCdMode[i], 2 );
}

void HistBranch::SetLivACD( void )
{
  if ( fEvnt.GetNclkEna() > 0 ) {
    fLivACD->SetPoint( fLivACD->GetN(), fTime, 1.0*fEvnt.GetNclkACD()/fEvnt.GetNclkEna() );
    SetFrame( fLivACD, 3 );
  }
}

void HistBranch::SetLivCZT( void )
{
  if ( fEvnt.GetNclkEna() > 0 ) {
    fLivCZT->SetPoint( fLivCZT->GetN(), fTime, 1.0*fEvnt.GetNclkCZT()/fEvnt.GetNclkEna() );
    SetFrame( fLivCZT, 3 );
  }
}

void HistBranch::SetLivCs1( void )
{
  if ( fEvnt.GetNclkEna() > 0 ) {
    fLivCs1->SetPoint( fLivCs1->GetN(), fTime, 1.0*fEvnt.GetNclkCs1()/fEvnt.GetNclkEna() );
    SetFrame( fLivCs1, 3 );
  }
}

void HistBranch::SetLivCs2( void )
{
  if ( fEvnt.GetNclkEna() > 0 ) {
    fLivCs2->SetPoint( fLivCs2->GetN(), fTime, 1.0*fEvnt.GetNclkCs2()/fEvnt.GetNclkEna() );
    SetFrame( fLivCs2, 3 );
  }
}

void HistBranch::SetLivSiT( int i )
{
  if ( fEvnt.GetNclkEna() > 0 ) {
    fLivSiT[i]->SetPoint( fLivSiT[i]->GetN(), fTime, 1.0*fEvnt.GetNclkSiT(i)/fEvnt.GetNclkEna() );
    SetFrame( fLivSiT[i], 3 );
  }
}

void HistBranch::SetHitACD( void )
{
  fHitACD->SetPoint( fHitACD->GetN(), fTime,1.0* fEvnt.GetNhitACD()/fPeriod );
  SetFrame( fHitACD, 4 );
}

void HistBranch::SetOneSiT( void )
{
  fOneSiT->SetPoint( fOneSiT->GetN(), fTime, 1.0*fEvnt.GetNoneSiT()/fPeriod );
  SetFrame( fOneSiT, 5 );
}

void HistBranch::SetAndSiT( void )
{
  fAndSiT->SetPoint( fAndSiT->GetN(), fTime, 1.0*fEvnt.GetNandSiT()/fPeriod );
  SetFrame( fAndSiT, 5 );
}

void HistBranch::SetTwoSiT( void )
{
  fTwoSiT->SetPoint( fTwoSiT->GetN(), fTime, 1.0*fEvnt.GetNtwoSiT()/fPeriod );
  SetFrame( fTwoSiT, 5 );
}

void HistBranch::SetHitCZT( void )
{
  fHitCZT->SetPoint( fHitCZT->GetN(), fTime, 1.0*fEvnt.GetNhitCZT()/fPeriod );
  SetFrame( fHitCZT, 6 );
}

void HistBranch::Set1stCsI( void )
{
  f1stCsI->SetPoint( f1stCsI->GetN(), fTime, 1.0*fEvnt.GetN1stCsI()/fPeriod );
  SetFrame( f1stCsI, 7 );
}

void HistBranch::Set2ndCsI( void )
{
  f2ndCsI->SetPoint( f2ndCsI->GetN(), fTime, 1.0*fEvnt.GetN2ndCsI()/fPeriod );
  SetFrame( f2ndCsI, 7 );
}

void HistBranch::SetHitXsi( int i )
{
  fHitXsi[i]->SetPoint( fHitXsi[i]->GetN(), fTime, 1.0*fEvnt.GetNhitXsi( i )/fPeriod );
  SetFrame( fHitXsi[i], 8 );
}

void HistBranch::SetHitYsi( int i )
{
  fHitYsi[i]->SetPoint( fHitYsi[i]->GetN(), fTime, 1.0*fEvnt.GetNhitYsi( i )/fPeriod );
  SetFrame( fHitYsi[i], 9 );
}

void HistBranch::SetAllTrg( void )
{
  fAllTrg->Fill( 1-0.5, fEvnt.GetNtrgLck() );
  fAllTrg->Fill( 2-0.5, fEvnt.GetNtrgAck() );
  fAllTrg->Fill( 3-0.5, fEvnt.GetNswtAck() );
  fAllTrg->Fill( 4-0.5, fEvnt.GetNextAck() );
  fAllTrg->Fill( 5-0.5, fEvnt.GetNlpcAck() );
  fAllTrg->Fill( 6-0.5, fEvnt.GetNhpcAck() );
  for ( Int_t i = 0; i < N_MODE; i++ ) 
    fAllTrg->Fill( 7+i-0.5, fEvnt.GetNcdMode( i ) );
}

void HistBranch::SetAllHit( void ) 
{
  fAllHit->Fill( 1-0.5, fEvnt.GetNhitACD() );
  fAllHit->Fill( 2-0.5, fEvnt.GetNoneSiT() );
  fAllHit->Fill( 3-0.5, fEvnt.GetNandSiT() );
  fAllHit->Fill( 4-0.5, fEvnt.GetNtwoSiT() );
  fAllHit->Fill( 5-0.5, fEvnt.GetNhitCZT() );
  fAllHit->Fill( 6-0.5, fEvnt.GetN1stCsI() ); 
  fAllHit->Fill( 7-0.5, fEvnt.GetN2ndCsI() );
  for ( Int_t i = 0; i < N_SITR; i++ ) {
    fAllHit->Fill( 8+i*2-0.5, fEvnt.GetNhitXsi( i ) );
    fAllHit->Fill( 8+i*2+0.5, fEvnt.GetNhitYsi( i ) );
  }
}

void HistBranch::Clear( Option_t *option )
{
  if ( fLiveTM ) fLiveTM->Set( 0 );
  if ( fTrgLck ) fTrgLck->Set( 0 );
  if ( fTrgAck ) fTrgAck->Set( 0 );
  if ( fSwtAck ) fSwtAck->Set( 0 );
  if ( fExtAck ) fExtAck->Set( 0 );
  if ( fLpcAck ) fLpcAck->Set( 0 );
  if ( fHpcAck ) fHpcAck->Set( 0 );
  for ( Int_t i = 0; i < N_MODE; i++ ) {
    if ( fCoMode[i] ) fCoMode[i]->Set( 0 );
    if ( fCdMode[i] ) fCdMode[i]->Set( 0 );
  }
  if ( fLivACD ) fLivACD->Set( 0 );
  if ( fLivCZT ) fLivCZT->Set( 0 );
  if ( fLivCs1 ) fLivCs1->Set( 0 );
  if ( fLivCs2 ) fLivCs2->Set( 0 );
  for ( Int_t i = 0; i < N_SITR; i++ ) {
    if ( fLivSiT[i] ) fLivSiT[i]->Set( 0 );
  }
  if ( fHitACD ) fHitACD->Set( 0 );
  if ( fOneSiT ) fOneSiT->Set( 0 );
  if ( fAndSiT ) fAndSiT->Set( 0 );
  if ( fTwoSiT ) fTwoSiT->Set( 0 );
  if ( fHitCZT ) fHitCZT->Set( 0 );
  if ( f1stCsI ) f1stCsI->Set( 0 );
  if ( f2ndCsI ) f2ndCsI->Set( 0 );
  for ( Int_t i = 0; i < N_SITR; i++ ) {
    if ( fHitXsi[i] ) fHitXsi[i]->Set( 0 );
    if ( fHitYsi[i] ) fHitYsi[i]->Set( 0 );
  }
  if ( fAllTrg ) fAllTrg->Reset();
  if ( fAllHit ) fAllHit->Reset();

  for ( Int_t i = 0; i < N_HIST; i++ ) {
    fYmax [i] = 0.0;
  }
}
