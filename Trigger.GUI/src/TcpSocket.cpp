#include "TcpSocket.h"

#include "TString.h"

#include <iostream>
#include <iomanip>
#include <cerrno>

#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

TcpSocket::TcpSocket( int m ) : mode( m )
{
  rcvBuf = 0;
  rcvLen = 0;
  rcvBufLen = 0;

  ev_tot = 0;
  Clear();
}

TcpSocket::~TcpSocket()
{
  if ( rcvBuf ) delete rcvBuf;
}

int TcpSocket::CreateBuffer( unsigned int bufferLen )
{
  if ( rcvBuf ) delete [] rcvBuf;

  rcvBufLen = bufferLen;
  rcvBuf = new unsigned char[rcvBufLen];

  for ( unsigned int i = 0; i < rcvBufLen; i++ )
    rcvBuf[i] = 0x0;

  return rcvBufLen;
}

int TcpSocket::CreateSocket( void )
{
  if ( ( sock[mode] = socket( PF_INET, SOCK_STREAM, 0 ) ) < 0 ) {
    std::cerr << "cannnot create socket" << std::endl;
    return -1;
  }

  return 0;
}

int TcpSocket::BindSocket( void )
{
  if ( ::bind( sock[mode], (struct sockaddr *)&ownaddr, sizeof(ownaddr) ) < 0 ) {
    std::cerr << "bind failed" << std::endl;
    return -1;
  }

  return 0;
}

int TcpSocket::ConnectSocket( void )
{
  if ( connect( sock[CLT], (struct sockaddr *)&hisaddr, sizeof(hisaddr) ) < 0 ) {
    std::cerr << "connect failed" << std::endl;
    return -1;
  }

  connected = true;
  return 0;
}

int TcpSocket::Listen( int backlog )
{
  if ( backlog > MAXLOG ) {
    std::cerr << "Number of backlog exceed " << MAXLOG << "." << std::endl;
    return -1;
  }

  if ( listen( sock[mode], backlog ) < 0 ) {
    std::cerr << "listen failed" << std::endl;
    return -1;
  }

  return 0;
}

int TcpSocket::Accept( void )
{
  socklen_t addrlen = sizeof(hisaddr);

  while ( ( sock[CLT] = accept( sock[SRV], (struct sockaddr *)&hisaddr, &addrlen )) < 0 ) {
    if ((errno != ECHILD) && (errno != EINTR)) {
      std::cerr << "accept failed" << std::endl;
      return -1;
    }
  }

  connected = true;

  return 0;
}

void TcpSocket::CloseSocket( void )
{
  if ( sock[0] ) { close( sock[0] ); sock[0] = 0; }
  if ( sock[1] ) { close( sock[1] ); sock[1] = 0; }

  connected = false;
}

int TcpSocket::SetTimeOut( int seconds )
{
  struct timeval tv;
  tv.tv_sec = seconds;
  if ( setsockopt( sock[mode], SOL_SOCKET, SO_RCVTIMEO, (struct timeval *)&tv, sizeof( struct timeval ) ) < 0 ) {
    std::cerr << "setsockopt failed" << std::endl;
    return -1;
  }

  return 0;
}

int TcpSocket::SetOwnAddr( unsigned short port, const char *ip )
{
  ownaddr.sin_family = AF_INET;
  ownaddr.sin_port = htons( port );
  if ( inet_pton( AF_INET, ip, &(ownaddr.sin_addr) ) != 1 ) {
    std::cerr << "inet_pton failed" << std::endl;
    return -1;
  }

  return 0;
}

int TcpSocket::SetHisAddr( unsigned short port, const char *ip )
{
  hisaddr.sin_family = AF_INET;
  hisaddr.sin_port = htons( port );
  if ( inet_pton( AF_INET, ip, &(hisaddr.sin_addr) ) != 1 ) {
    std::cerr << "inet_pton failed" << std::endl;
    return -1;
  }

  return 0;
}

int TcpSocket::Send( void *buffer, unsigned int bufferLen )
{
  unsigned int sentLen = 0;
  unsigned int sendLen = bufferLen;

  if ( !IsConnected() ) {
    std::cerr << "send failed: no connection" << std::endl;
    return -1;
  }

  do {
    int sent = send( sock[CLT], (void *)(((unsigned char *)buffer) + sentLen), sendLen, 0 );
    if ( sent < 0 ) {
      std::cerr << "send failed" << std::endl;
      return -1;
    }
    sentLen += sent;
    sendLen -= sent;
  } while ( sentLen < bufferLen );

  return sentLen;
}

int TcpSocket::Recv( void *buffer, unsigned int bufferLen )
{
  int rcvd = recv( sock[CLT], buffer, bufferLen, 0 );

  if ( rcvd < 0 ) {
    std::cerr << "recv failed" << std::endl;
    return -1;
  }
  else if ( rcvd == 0 ) {
    std::cerr << "connection closed and recv failed" << std::endl;
    close( sock[CLT] );
    sock[CLT] = 0;
    connected = false;
    return 0;
  }

  return rcvd;
}

void TcpSocket::Clear( void )
{
  connected = false;
  rcvBuf    = 0;
  rcvBufLen = 0;
  sock[0]   = 0;
  sock[1]   = 0;
  memset( (char *)&ownaddr, 0, sizeof(ownaddr) );
  memset( (char *)&hisaddr, 0, sizeof(hisaddr) );
}

void TcpSocket::Print( void )
{
}

void TcpSocket::RunThread( void )
{
  do {
    if ( sock[CLT] ) {
      if   ( status == 1 ) usleep( 10 );
      else if ( ( rcvLen = Recv( rcvBuf, rcvBufLen ) ) > 0 ) {
	SetStatus( 1 );
	ev_tot++;
      }
      else std::cout << "<TcpSocket::RunThread> Receive failed" << std::endl;
    }
    else {
      if ( mode == SRV ) Accept();
      else status = -1;
    }
  } while ( status >= 0 );
}

void TcpSocket::PrintStatus( void )
{
  std::cout << "<TcpSocket::PrintStatus> ev_tot = " << ev_tot << std::endl;
}
