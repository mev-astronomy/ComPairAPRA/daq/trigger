#include "RegObject.h"
#include "TCmndSnder.h"

ClassImp( RegObject )

RegObject::RegObject( unsigned char addr )
{
  cmdPacket.cmnd = 0x0;
  cmdPacket.addr = addr;
  cmdPacket.data[0] = 0x0;
  cmdPacket.data[1] = 0x0;
}

RegObject::~RegObject()
{
}

unsigned int RegObject::GetAddress( void )
{
  return cmdPacket.addr;
}

unsigned int RegObject::GetValue( void )
{
  return (cmdPacket.data[0] << 8) + (cmdPacket.data[1]);
}

void RegObject::SetValue( unsigned int val )
{
  cmdPacket.data[0] = ( 0xFF & ( val >> 8 ) );
  cmdPacket.data[1] = ( 0xFF & ( val >> 0 ) );
}

void RegObject::SendCommand( unsigned char cmnd )
{
  cmdPacket.cmnd = cmnd;
  gCmndSnder->SendCommand( &cmdPacket );
}
