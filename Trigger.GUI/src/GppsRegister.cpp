#include "GppsRegister.h"
#include "RegCheckButton.h"

ClassImp( GppsRegister )

GppsRegister::GppsRegister( const TGWindow *p, TGString *title, UInt_t options ) : BaseRegister( p, title, options )
{
}

GppsRegister::~GppsRegister()
{
}

void GppsRegister::Init( void )
{  
  fGpps = (RegCheckButton *)fArr->At(0);
  fGpps->SetNumOfButton( N_ITEM );

  TGHorizontalFrame *hfrm = new TGHorizontalFrame( this, 480, 580, 0 );
  
  TGCheckButton *bt = new TGCheckButton( hfrm, new TGHotString( "Internal PPS" ) );
  bt->Connect( "Clicked()", "GppsRegister", this, "Status()" );
  hfrm->AddFrame( bt, new TGLayoutHints( kLHintsExpandX, 0, 0, 0 ) );
  fGpps->SetButton( 0, bt );
  AddFrame( hfrm, new TGLayoutHints( kLHintsExpandX, 0, 0, 0, 0 ) );
}
