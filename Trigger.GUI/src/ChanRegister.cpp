#include "ChanRegister.h"
#include "RegComboBox.h"

#include "TGComboBox.h"

ClassImp( ChanRegister )

ChanRegister::ChanRegister( const TGWindow *p, TGString *title, UInt_t options ) : BaseRegister( p, title, options )
{
}

ChanRegister::~ChanRegister()
{
}

void ChanRegister::Init( void )
{  
    TString conns[N_CONN] = { 
      "Si-Tr LPC-0    ", "Si-Tr LPC-1    ", "Si-Tr LPC-2    ", "Si-Tr LPC-3    ", 
      "ACD            ", "CZT            ", "CsI (1st)      ", "CsI (2nd)      ",
      "Si-Tr MPC-0    ", "Si-Tr MPC-1    ", "Si-Tr MPC-2    ", "Si-Tr MPC-3    ", 
      "Si-Tr HPC-0    ", "Si-Tr HPC-1    ", "Si-Tr HPC-2    ", "Si-Tr HPC-3    " 
    };
    
    TString chans[N_CHAN] = {
      "Trigger Lock   ", 
      "Trigger Ack    ", "Event ID Clock ", "Event ID Data  ", "Trigger Ena    ", 
      "FEE Hit        ", "FEE Ready      ", "FEE Busy       ", "FEE Spare      ",
      "ACD Hit Coin   ", "SiT One Coin   ", "SiT AND Coin   ", "Sit Two Coin   ", 
      "CZT Hit Coin   ", "CsI Hit Coin   ", "CsI Hit 1st    ", "CsI Hit 2nd    ",
      "ACD Hit Lock   ", "Sit Hit Lock   ", "CZT Hit Lock   ", "CsI Hit Lock   ", 
      "LPC Lemo Lock  ", "HPC Lemo Lock  ", "N/A            "
    };

  fConn = (RegComboBox    *)fArr->At( 0 );
  fChan = (RegComboBox    *)fArr->At( 1 );

  TGHorizontalFrame *hfrm 
    = new TGHorizontalFrame( this, 240, 240, kDoubleBorder );

  TGComboBox *conn = new TGComboBox( hfrm );
  for ( Int_t i = 0; i < N_CONN; i++ ) {
    conn->AddEntry( conns[i].Data(), i );
  }
  //  conn->Resize( 128, 26 );
  conn->Resize( 100, 26 );
  conn->Select( 0 );
  conn->Connect( "Selected(Int_t)", "ChanRegister", this, "Status()" );
  fConn->SetComboBox( conn );
  hfrm->AddFrame( conn, new TGLayoutHints( kLHintsLeft, 0, 0, 0, 0 ) );

  TGComboBox *chan = new TGComboBox( hfrm );
  for ( Int_t i = 0; i < N_CHAN; i++ ) {
    chan->AddEntry( chans[i].Data(), i );
  }
  chan->Resize( 108, 26 );
  chan->Select( 0 );
  chan->Connect( "Selected(Int_t)", "ChanRegister", this, "Status()" );
  fChan->SetComboBox( chan );
  hfrm->AddFrame( chan, new TGLayoutHints( kLHintsLeft, 0, 0, 0, 0 ) );

  AddFrame( hfrm, new TGLayoutHints( kLHintsLeft, 0, 0, 0, 0 ) );
}
