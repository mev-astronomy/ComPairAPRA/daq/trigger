#include "AmMainFrame.h"
#include "TCmndSnder.h"
#include "CmdHandler.h"
#include "TDataRcver.h"

#include "RegEnable.h"

#include "ConnRegister.h"
#include "LemoRegister.h"
#include "GppsRegister.h"
#include "ModeRegister.h"
#include "ExtrRegister.h"
#include "EvidRegister.h"
#include "WdthRegister.h"
#include "ChanRegister.h"

#include "TGGC.h"
#include "TGFont.h"
#include "TGResourcePool.h"
#include "TStyle.h"
#include "TTimer.h"
#include "TGTab.h"
#include "TGButton.h"
#include "TRootEmbeddedCanvas.h"
#include "TObjArray.h"
#include "TCanvas.h"
#include "TGTextView.h"
#include "TGTextEntry.h"

#include "TGFileDialog.h"
#include "TGProgressBar.h"
#include "TSystem.h"

#include <iostream>

//#define DEBUG

ClassImp( AmMainFrame )

AmMainFrame::AmMainFrame( const TGWindow *p, UInt_t w, UInt_t h ) : TGMainFrame( p, w, h )
{
  //  TGGC myGC = *gClient->GetResourcePool()->GetFrameGC();
  //  TGFont *myFont = gClient->GetFont( "-adobe-helvetica-bold-r-*-*-10-*-*-*-*-*-iso8859-1" );
  //  if ( myFont ) myGC.SetFont( myFont->GetFontHandle() );

  fTrig = kFALSE;

  fNprcv = 0;
  fNzero = 0;

  gStyle->SetOptStat ( 0 );
  gStyle->SetPadGridX( 1 );
  gStyle->SetPadGridY( 1 );
  gStyle->SetPadBottomMargin( 0.10 );

  fCmnd = new TCmndSnder( TcpSocket::CLT );
  fCmnd->Init();
  fCmnd->Connect( "AddText(const char *)", "AmMainFrame", this, "AddCmnd(const char *)" );
  fCmnd->Connect( "AddEcho(const char *)", "AmMainFrame", this, "AddEcho(const char *)" );

  fChnd = new CmdHandler();
  fChnd->ReadTable();

  fData = new TDataRcver();
  fData->Init();

  fAtab = new TGTab( this, 720, 580 );
  fAtab->Associate( this );

  ExecuteButtColumn( this );

  // command tab
  {
    TGCompositeFrame *tab = fAtab->AddTab( "Register Settings" );
    ExecuteCmndColumn( tab );

    TGHorizontalFrame *hframe = new TGHorizontalFrame( tab, 720, 580, kSunkenFrame );

    TGVerticalFrame *vfrm1
      = new TGVerticalFrame( hframe, 480, 580, kDoubleBorder );

    CoincidenceColumn( vfrm1 );

    hframe->AddFrame( vfrm1, new TGLayoutHints( kLHintsExpandY, 0, 0, 0, 0 ) );

    TGVerticalFrame *vfrm2
      = new TGVerticalFrame( hframe, 480, 580, kDoubleBorder );

    ConnectorLPColumn( vfrm2 );
    ConnectorHPColumn( vfrm2 );
    CoinciWidthColumn( vfrm2 );

    TGHorizontalFrame *hfrm1 = new TGHorizontalFrame( vfrm2, 1280, 580, 0 );
    TGVerticalFrame *vfrm3 = new TGVerticalFrame( hfrm1, 1280, 580, 0 );
    GpsPPSselecColumn( vfrm3 );
    ExtTrigRateColumn( vfrm3 );
    hfrm1->AddFrame( vfrm3, new TGLayoutHints( kLHintsNormal, 0, 0, 0, 0 ) );

    TGVerticalFrame *vfrm4 = new TGVerticalFrame( hfrm1, 1280, 580, 0 );
    ConnectorLemolumn( vfrm4 );
    ChannSourceColumn( vfrm4 );
    hfrm1->AddFrame( vfrm4, new TGLayoutHints( kLHintsExpandX, 0, 0, 0, 0 ) );

    vfrm2->AddFrame( hfrm1, new TGLayoutHints( kLHintsExpandX, 0, 0, 0, 0 ) );
    hframe->AddFrame( vfrm2, new TGLayoutHints( kLHintsExpandX | kLHintsCenterX | kLHintsExpandY, 0, 0, 0, 0 ) );
    tab->AddFrame( hframe, new TGLayoutHints( kLHintsExpandX, 1, 1, 0, 0 ) );

  }

  // data tab
  {
    fCarray = new TObjArray();
    TGCompositeFrame *tab = fAtab->AddTab( "Trigger Event Data" );
    ExecuteDataColumn( tab, 0 );

    TGVerticalFrame *vframe = new TGVerticalFrame( tab, 720, 580, kSunkenFrame );
    TString htitle[N_HIST] = {
      "Live Time", "Trigger Ack", "Trigger Mode", "Subsystem Live Time",
      "ACD Hit Rate", "Si-Tracker Hit Rate", "CZT Hit Rate", "CsI Hit Rate",
      "Si-Tracker X Hit Rate", 
      "Si-Tracker Y Hit Rate", 
      "Trigger hit", 
      "Detector Hit" };
    
    for ( unsigned int iraw = 0; iraw < 3; iraw++ ) {
      TGHorizontalFrame *hfrm = new TGHorizontalFrame( vframe, 720, 145, kDoubleBorder );
      for ( unsigned int icol = 0; icol < 4; icol++ ) {
	TGVerticalFrame *vfrm = new TGVerticalFrame( hfrm, 720, 145, kSunkenFrame );
	TRootEmbeddedCanvas *fEcan = new TRootEmbeddedCanvas( Form( "Ecanvas_%X_%X", iraw, icol ), vfrm, 320, 180 );
	fCarray->Add( fEcan );
	vfrm->AddFrame( fEcan, new TGLayoutHints( 0, 0, 0, 0, 0 ) );
	TGTextButton *hsel = new TGTextButton( vfrm, Form( "%s", htitle[iraw*N_COL+icol].Data() ), iraw*N_COL+icol );
	hsel->Connect( "Clicked()", "AmMainFrame", this, Form( "HistSel(=%d)", iraw*N_COL+icol ) );
	vfrm->AddFrame( hsel, new TGLayoutHints( kLHintsExpandX, 3, 3, 3, 3 ));

	hfrm->AddFrame( vfrm, new TGLayoutHints( kLHintsExpandX, 0, 0, 0, 0 ) );
      }
      vframe->AddFrame( hfrm, new TGLayoutHints( kLHintsExpandX | kLHintsExpandY, 0, 0, 0, 0 ) );
    }
    tab->AddFrame( vframe, new TGLayoutHints( kLHintsExpandX | kLHintsExpandY, 0, 0, 0, 0 ) );
  }

  // Zoom tab
  {
    TGCompositeFrame *tab = fAtab->AddTab( "Expanded Display" );
    ExecuteDataColumn( tab, 1 );

    TGHorizontalFrame *hfrm = new TGHorizontalFrame( tab, 720, 580, kSunkenFrame );

    fZcan = new TRootEmbeddedCanvas( Form( "Zcanvas"), hfrm, 1120, 630 );
    hfrm->AddFrame( fZcan, new TGLayoutHints( 0, 2, 2, 2, 2 ) );

    TString htitle[N_HIST] = {
      "Live Time", "Trigger Ack", "Trigger Mode", "Subsystem Live Time",
      "ACD Hit Rate", "Si-Tracker Hit Rate", "CZT Hit Rate", "CsI Hit Rate",
      "Si-Tracker X Hit Rate", 
      "Si-Tracker Y Hit Rate", 
      "Trigger hit", 
      "Detector Hit" };

    TGVerticalFrame *vfrm = new TGVerticalFrame( hfrm, 720, 145, kSunkenFrame );
    for ( unsigned int iraw = 0; iraw < 3; iraw++ ) {
      for ( unsigned int icol = 0; icol < 4; icol++ ) {
	fZsel[iraw*N_COL+icol] = new TGTextButton( vfrm, Form( "%s", htitle[iraw*N_COL+icol].Data() ), iraw*N_COL+icol );
	fZsel[iraw*N_COL+icol]->Connect( "Clicked()", "AmMainFrame", this, Form( "HistSel(=%d)", iraw*N_COL+icol ) );
	vfrm->AddFrame( fZsel[iraw*N_COL+icol], new TGLayoutHints( kLHintsExpandX | kLHintsCenterX, 3, 3, 3, 3 ));
      }
    }

    hfrm->AddFrame( vfrm, new TGLayoutHints( kLHintsExpandX | kLHintsCenterX, 0, 0, 0, 0 ) );
    tab->AddFrame( hfrm, new TGLayoutHints( kLHintsExpandX | kLHintsCenterX, 0, 0, 0, 0 ) );
  }

  fAtab->Connect( "Selected(Int_t)", "AmMainFrame", this, "Selected(Int_t)" );
  AddFrame( fAtab, new TGLayoutHints( 0, 10, 10, 10, 0 ) );

  CommandEchoColumn( this );

  fTimr = new TTimer();
  fTimr->Connect( "Timeout()", "AmMainFrame", this, "AutoRefresh()" );
  fTimr->Start( 2000, kFALSE );

  SetWindowName( "AMEGO Trigger" );
  MapSubwindows();
  Resize( GetDefaultSize() );
  MapWindow();
}

AmMainFrame::~AmMainFrame()
{
}

void AmMainFrame::CoincidenceColumn( TGCompositeFrame *p )
{
  // coincidence column
  fMode = new ModeRegister( p, new TGString( "Coincidence" ), 0 );
  RegRadioButton *sels = new RegRadioButton( 0x06 );
  fMode->Add( sels );
  RegCheckButton *mode = new RegCheckButton( 0x10 );
  fMode->Add( mode );
  for ( Int_t i = 0; i < N_MODE; i++ ) {
    RegNumberEntry *numb = new RegNumberEntry( 0x20+i );
    fMode->Add( numb );
  }
  fMode->Init();
  p->AddFrame( fMode, new TGLayoutHints( kLHintsExpandY, 1, 0, 0, 0 ) );
}

void AmMainFrame::ExtTrigRateColumn( TGCompositeFrame *p )
{
  // External Trigger rate
  fExtr = new ExtrRegister( p, new TGString( "External Trigger Rate" ), 0 );

  RegComboBox *cbox1 = new RegComboBox( 0x11 );
  fExtr->Add( cbox1 );
  RegComboBox *cbox2 = new RegComboBox( 0x12 );
  fExtr->Add( cbox2 );
  fExtr->Init();
  p->AddFrame( fExtr, new TGLayoutHints( 0, 1, 0, 0, 0 ) );
}

void AmMainFrame::ConnectorLPColumn( TGCompositeFrame *p )
{
  TGHorizontalFrame *hfrm
    = new TGHorizontalFrame( p, 480, 240, kDoubleBorder );
  
  TString label[8] = { "Si-Tracker LPC-0", "Si-Tracker LPC-1", 
		       "Si-Tracker LPC-2", "Si-Tracker LPC-3", 
		       "ACD"             , "CZT"             , 
		       "CsI (1st)"       , "CsI (2nd)"         };

  for ( Int_t i = 0; i < 8; i++ ) {
    fConn[i] = new ConnRegister( hfrm, new TGString( Form( "%s", label[i].Data() ) ), 0 );
    RegCheckButton *conn = new RegCheckButton( 0x40+i );
    fConn[i]->Add( conn );
    fConn[i]->Init();
    hfrm->AddFrame( fConn[i], new TGLayoutHints( kLHintsExpandX, 0, 0, 0, 0 ) );
  }

  p->AddFrame( hfrm, new TGLayoutHints( kLHintsExpandX | kLHintsCenterX, 1, 1, 0, 0 ) );
}

void AmMainFrame::ConnectorHPColumn( TGCompositeFrame *p )
{	
  TGHorizontalFrame *hfrm
    = new TGHorizontalFrame( p, 480, 240, 0 );
  
  TString label[8] = { "Si-Tracker MPC-0", "Si-Tracker MPC-1", 
		       "Si-Tracker MPC-2", "Si-Tracker MPC-3", 
		       "Si-Tracker HPC-0", "Si-Tracker HPC-1", 
		       "Si-Tracker HPC-2", "Si-Tracker HPC-3" };

  for ( Int_t i = 8; i < 16; i++ ) {
    fConn[i] = new ConnRegister( hfrm, new TGString( Form( "%s", label[i-8].Data() ) ), 0 );
    RegCheckButton *conn = new RegCheckButton( 0x40+i );
    fConn[i]->Add( conn );
    fConn[i]->Init();
    hfrm->AddFrame( fConn[i], new TGLayoutHints( kLHintsExpandX, 0, 0, 0, 0 ) );
  }

  p->AddFrame( hfrm, new TGLayoutHints( kLHintsExpandX | kLHintsCenterX, 1, 1, 0, 0 ) );
}

void AmMainFrame::ConnectorLemolumn( TGCompositeFrame *p )
{	
  TGHorizontalFrame *hfrm
    = new TGHorizontalFrame( p, 480, 240, 0 );
  
  fLemo = new LemoRegister( hfrm, new TGString( "LEMO Input/Output" ), 0 );
  RegCheckButton *lemo = new RegCheckButton( 0x60 );
  fLemo->Add( lemo );
  fLemo->Init();
  hfrm->AddFrame( fLemo, new TGLayoutHints( kLHintsExpandX | kLHintsExpandY | kLHintsBottom, 1, 1, 0, 0 ) );

  p->AddFrame( hfrm, new TGLayoutHints( kLHintsExpandX | kLHintsCenterX | kLHintsExpandY, 0, 0, 0, 0 ) );
}

void AmMainFrame::GpsPPSselecColumn( TGCompositeFrame *p )
{
  fGpps = new GppsRegister( p, new TGString( "GPS PPS Source" ), 0 );
  RegCheckButton *gpps = new RegCheckButton( 0x07 );
  fGpps->Add( gpps );
  fGpps->Init();
  p->AddFrame( fGpps, new TGLayoutHints( kLHintsExpandX, 0, 0, 0, 0 ) );
}

void AmMainFrame::CoinciWidthColumn( TGCompositeFrame *p )
{
  // Coincidence Width
  Int_t cmnd[N_SUBD+2] = { 0x02, 0x03, 0x30, 0x31, 0x32, 0x33 };
  TString label[N_SUBD+2] = { "Trig Ack pulse ", "Event ID pulse",
			      "ACD coin. width  ", "Si-T coin. width ", 
			      "CZT coin. width  ", "CsI coin. width  " };

  TGHorizontalFrame *hfrm
    = new TGHorizontalFrame( p, 480, 240, kDoubleBorder );

  for ( Int_t i = 0; i < N_SUBD+2; i++ ) {
    fWdth[i] = new WdthRegister( hfrm, new TGString( Form( "%s", label[i].Data() ) ), kFixedWidth );
    RegNumberEntry *ne = new RegNumberEntry( cmnd[i] );
    fWdth[i]->Add( ne );
    fWdth[i]->Init();
    hfrm->AddFrame( fWdth[i], new TGLayoutHints( kLHintsExpandX, 0, 0, 0, 0 ) );
  }

  p->AddFrame( hfrm, new TGLayoutHints( kLHintsExpandX, 1, 1, 0, 0 ) );
}

void AmMainFrame::ChannSourceColumn( TGCompositeFrame *p )
{
  // Channel output selection
  TGHorizontalFrame *hfrm 
    = new TGHorizontalFrame( p, 120, 120, kDoubleBorder );
  
  TString label[N_CHAN] = {
    "LPC Out [0]", "LPC Out [1]", "HPC Out [0]", "HPC Out [1]"
  };
  
  for ( Int_t i = 0; i < N_CHAN; i++ ) {
    fChan[i] = new ChanRegister( hfrm, new TGString( Form( "%s", label[i].Data() ) ), kFixedWidth );
    RegComboBox *conn = new RegComboBox( 0x50+i*2, RegComboBox::T_BINA );
    fChan[i]->Add( conn );
    RegComboBox *chan = new RegComboBox( 0x50+i*2+1, RegComboBox::T_BINA );
    fChan[i]->Add( chan );
    fChan[i]->Init();
    hfrm->AddFrame( fChan[i], new TGLayoutHints( kLHintsExpandX, 0, 0, 0, 0 ) );
  }
  
  p->AddFrame( hfrm, new TGLayoutHints( kLHintsExpandX, 1, 1, 0, 0 ) );
}

void AmMainFrame::ExecuteButtColumn( const TGWindow *p )
{
  TGHorizontalFrame *hfrm = new TGHorizontalFrame( p, 480, 200, kSunkenFrame );

  fCnct_Cmnd = new TGTextButton( hfrm, "Connect Cmnd" );
  fCnct_Cmnd->Connect( "Clicked()", "AmMainFrame", this, "Connect_Cmnd()" );
  hfrm->AddFrame( fCnct_Cmnd, new TGLayoutHints( kLHintsExpandX, 2, 1, 2, 2 ));

  ULong_t blue;
  gClient->GetColorByName( "blue", blue );

  if ( gCmndSnder->IsConnected() )
    fCnct_Cmnd->SetTextColor( blue   );
  else
    fCnct_Cmnd->SetTextColor( kBlack );

  fCnct_Data = new TGTextButton( hfrm, "Connect Data" );
  fCnct_Data->Connect( "Clicked()", "AmMainFrame", this, "Connect_Data()" );
  hfrm->AddFrame( fCnct_Data, new TGLayoutHints( kLHintsExpandX, 1, 1, 2, 2 ));

  if ( gDataRcver->IsConnected() )
    fCnct_Data->SetTextColor( blue   );
  else 
    fCnct_Data->SetTextColor( kBlack );

  fLoad = new TGTextButton( hfrm, "Load" );
  fLoad->Connect( "Clicked()", "AmMainFrame", this, "Load()" );
  hfrm->AddFrame( fLoad, new TGLayoutHints( kLHintsExpandX, 1, 1, 2, 2 ));

  fOpen = new TGTextButton( hfrm, "Open" );
  fOpen->Connect( "Clicked()", "AmMainFrame", this, "Open()" );
  hfrm->AddFrame( fOpen, new TGLayoutHints( kLHintsExpandX, 1, 1, 2, 2 ));

  fClse = new TGTextButton( hfrm, "Close" );
  fClse->Connect( "Clicked()", "AmMainFrame", this, "Close()" );
  hfrm->AddFrame( fClse, new TGLayoutHints( kLHintsExpandX, 1, 1, 2, 2 ));

  TGTextButton *echo = new TGTextButton( hfrm, "Command Ec&ho" );
  echo->Connect( "Clicked()", "AmMainFrame", this, "Echo()" );
  hfrm->AddFrame( echo, new TGLayoutHints( kLHintsExpandX, 1, 1, 2, 2 ));

  TGTextButton *fifo = new TGTextButton( hfrm, "Check FIFO" );
  fifo->Connect( "Clicked()", "AmMainFrame", this, "Fifo()" );
  hfrm->AddFrame( fifo, new TGLayoutHints( kLHintsExpandX, 1, 1, 2, 2 ));

  TGTextButton *status = new TGTextButton( hfrm, "Status" );
  status->Connect( "Clicked()", "AmMainFrame", this, "Status()" );
  hfrm->AddFrame( status, new TGLayoutHints( kLHintsExpandX, 1, 1, 2, 2 ));

  TGTextButton *reset = new TGTextButton( hfrm, "&Reset" );
  reset->Connect( "Clicked()", "AmMainFrame", this, "Reset()" );
  hfrm->AddFrame( reset, new TGLayoutHints( kLHintsExpandX, 1, 1, 2, 2 ));

  fCtrl = new RegEnable( 0x01 );

  fEnbl = new TGTextButton( hfrm, "Trigger &Enable" );
  fEnbl->Connect( "Clicked()", "AmMainFrame", this, "Enable()" );
  hfrm->AddFrame( fEnbl, new TGLayoutHints( kLHintsExpandX, 1, 1, 2, 2 ));
  
  fDsbl = new TGTextButton( hfrm, "Trigger &Disable" );
  fDsbl->Connect( "Clicked()", "AmMainFrame", this, "Disable()" );
  hfrm->AddFrame( fDsbl, new TGLayoutHints( kLHintsExpandX, 1, 1, 2, 2 ));
  
  TGTextButton *regexit = new TGTextButton( hfrm, "E&xit", "gApplication->Terminate()" );
  hfrm->AddFrame( regexit, new TGLayoutHints( kLHintsExpandX, 1, 2, 2, 2 ));

  AddFrame( hfrm, new TGLayoutHints( kLHintsExpandX, 10, 10, 5, 0 ) );
}

void AmMainFrame::CommandEchoColumn( const TGWindow *p )
{
  TGHorizontalFrame *hfrm = new TGHorizontalFrame( p, 480, 200, kSunkenFrame );

  fVcom = new TGTextView( hfrm, 240, 60 );
  hfrm->AddFrame( fVcom, new TGLayoutHints( kLHintsExpandX | kLHintsCenterX | kLHintsExpandY, 0, 0, 0, 0 ) );
  
  fVech = new TGTextView( hfrm, 240, 60 );
  hfrm->AddFrame( fVech, new TGLayoutHints( kLHintsExpandX | kLHintsCenterX | kLHintsExpandY, 0, 0, 0, 0 ) );
  
  AddFrame( hfrm, new TGLayoutHints( kLHintsExpandX, 10, 10, 0, 5 ) );
}

void AmMainFrame::ExecuteCmndColumn( TGCompositeFrame *p )
{
  TGHorizontalFrame *hfrm = new TGHorizontalFrame( p, 480, 200, kSunkenFrame );

  TGTextButton *rgload = new TGTextButton( hfrm, "Config &Load" );
  rgload->Connect( "Clicked()", "AmMainFrame", this, "RegLoad()" );
  hfrm->AddFrame( rgload, new TGLayoutHints( kLHintsExpandX, 3, 3, 0, 0 ));
  
  TGTextButton *rgset = new TGTextButton( hfrm, "Register &Set" );
  rgset->Connect( "Clicked()", "AmMainFrame", this, "Set()" );
  hfrm->AddFrame( rgset, new TGLayoutHints( kLHintsExpandX, 3, 3, 0, 0 ));

  TGTextButton *rgget = new TGTextButton( hfrm, "Register &Get" );
  rgget->Connect( "Clicked()", "AmMainFrame", this, "Get()" );
  hfrm->AddFrame( rgget, new TGLayoutHints( kLHintsExpandX, 3, 3, 0, 0 ));

  p->AddFrame( hfrm, new TGLayoutHints( kLHintsExpandX, 0, 0, 0, 0 ) );
}

void AmMainFrame::ExecuteDataColumn( TGCompositeFrame *p, Int_t i )
{
  TGHorizontalFrame *hfrm = new TGHorizontalFrame( p, 480, 200, kSunkenFrame );

  TGTextButton *clear = new TGTextButton( hfrm, "Clear" );
  clear->Connect( "Clicked()", "AmMainFrame", this, "Clear()" );
  hfrm->AddFrame( clear, new TGLayoutHints( kLHintsExpandX, 3, 3, 3, 3 ));
  fEvid[i] = new TGTextEntry( hfrm, "0000000000", -1 );
  fEvid[i]->SetMaxLength( 12 );
  hfrm->AddFrame( fEvid[i], new TGLayoutHints( kLHintsLeft, 1, 1, 2, 0 ) );
  fEvid_hex[i] = new TGTextEntry( hfrm, "0x00000000", -1 );
  fEvid_hex[i]->SetMaxLength( 12 );
  hfrm->AddFrame( fEvid_hex[i], new TGLayoutHints( kLHintsLeft, 1, 1, 2, 0 ) );

  p->AddFrame( hfrm, new TGLayoutHints( kLHintsExpandX, 0, 0, 0, 0 ) );
}

void AmMainFrame::Selected( Int_t i )
{
}

void AmMainFrame::Connect_Cmnd( void )
{
  if ( gCmndSnder->IsConnected() ) {
    std::cout << "<AmMainFrame::Connect_Cmnd> Already Connected." << std::endl;
    return;
  }

  fCmnd->Connect();

  if ( !gCmndSnder->IsConnected() ) {
    std::cout << "<AmMainFrame::Connect_Cmnd> Connect failed." << std::endl;
    return;
  }
  
  ULong_t blue;
  gClient->GetColorByName( "blue", blue );
  fCnct_Cmnd->SetTextColor( blue );

  fCtrl->SendCommand( CMD_READ );

  if ( fCtrl->GetValue() == 1 ) {
    fEnbl->SetTextColor( blue   );
    fDsbl->SetTextColor( kBlack );
  }
  else {
    fEnbl->SetTextColor( kBlack );
    fDsbl->SetTextColor( blue   );
  }

  Get(); // read register value;
}

void AmMainFrame::Connect_Data( void )
{
  if ( gDataRcver->IsConnected() ) {
    std::cout << "<AmMainFrame::Connect_Data> Already Connected." << std::endl;
    return;
  }

  fData->Connect();
  
  if ( !gDataRcver->IsConnected() ) {
    std::cout << "<AmMainFrame::Connect_Data> Connect failed." << std::endl;
    return;
  }

  ULong_t blue;
  gClient->GetColorByName( "blue", blue );
  fCnct_Data->SetTextColor( blue );
  fOpen->SetTextColor( blue );
  fClse->SetTextColor( kBlack );
}


void AmMainFrame::RegLoad( void )
{
  std::map<int, unsigned int> vals = fChnd->GetValMap();
  fMode->Load( vals );
  fExtr->Load( vals );
  for ( Int_t i = 0; i < N_CONN; i++ )
    fConn[i]->Load( vals );
  fLemo->Load( vals );
  fGpps->Load( vals );
  for ( Int_t i = 0; i < N_SUBD+2; i++ )
    fWdth[i]->Load( vals );
  for ( Int_t i = 0; i < N_CHAN; i++ )
    fChan[i]->Load( vals );
}

void AmMainFrame::Echo( void )
{
  cmdPacket.cmnd    = CMD_ECHO;
  cmdPacket.addr    = 0x0;
  cmdPacket.data[0] = 0x0;
  cmdPacket.data[1] = 0x0;

  if ( !gCmndSnder->SendCommand( &cmdPacket ) )
    std::cerr << "<AmMainFrame::Echo> failed" << std::endl;
}

void AmMainFrame::Fifo( void )
{
  cmdPacket.cmnd    = CMD_READ;
  cmdPacket.addr    = 0x0;
  cmdPacket.data[0] = 0x0;
  cmdPacket.data[1] = 0x0;

  if ( !gCmndSnder->SendCommand( &cmdPacket ) )
    std::cerr << "<AmMainFrame::Echo> failed" << std::endl;
}

void AmMainFrame::Status( void )
{
  cmdPacket.cmnd    = CMD_STAT;
  cmdPacket.addr    = 0x0;
  cmdPacket.data[0] = 0x0;
  cmdPacket.data[1] = 0x0;

  if ( !gCmndSnder->SendCommand( &cmdPacket ) )
    std::cerr << "<AmMainFrame::Status> failed" << std::endl;

  PrintStatus ();
  PrintEventID();
  fData->PrintStatus();
}

void AmMainFrame::CloseWindow( void )
{
  delete fMain;
}

void AmMainFrame::DoClose( void )
{
  if ( fClose )
    CloseWindow();
  else {
    fClose = kTRUE;
    TTimer::SingleShot( 150, "AmMainFrame", this, "CloseWindow()" );
  }
}

void AmMainFrame::Load( void )
{
  UInt_t w = 400;
  UInt_t h = 200;

  const char *filetypes[] = { "All files" , "*",
			      "ROOT files", "*.root",
			      "Raw files" , "*.dat",
			      0           , 0 };

  static TString dir( "raw/" );
  TGFileInfo fi;
  fi.fFileTypes = filetypes;
  fi.fIniDir    = StrDup( dir );
  new TGFileDialog( gClient->GetRoot(), this, kFDOpen, &fi );
  dir = fi.fIniDir;

  if ( fi.fFilename == NULL ) return;
  std::cout << "Load file: " << fi.fFilename << std::endl;

  fMain = new TGTransientFrame( gClient->GetRoot(), this, w, h );
  fMain->Connect( "CloseWindow()", "AmMainFrame", this, "DoClose()" );
  fMain->DontCallClose();

  // use hierarchical cleaning
  fMain->SetCleanup( kDeepCleanup );
  fMain->ChangeOptions( ( fMain->GetOptions() & ~kVerticalFrame ) | kHorizontalFrame );
  
  TGHProgressBar *fHProg = new TGHProgressBar( fMain, TGProgressBar::kStandard, 300 );
  fHProg->SetFillType( TGProgressBar::kBlockFill );
  fHProg->ShowPos( kTRUE );

  fMain->AddFrame( fHProg, new TGLayoutHints( kLHintsTop | kLHintsLeft | kLHintsExpandX, 5, 5, 5, 10 ) );

  fClose = kFALSE;

  fMain->SetWindowName( "Progress .." );
  TGDimension size = fMain->GetDefaultSize();
  fMain->Resize( size );

  // position relative to the parent's window
  fMain->CenterOnParent();
  fMain->MapSubwindows();
  fMain->MapWindow();

  fData->Load( fi.fFilename, fHProg );

  fClose = kTRUE;

  CloseWindow();
  if ( fMain ) fMain = 0;

  DrawEventHists();

  //  gClient->WaitFor( fMain );
}

void AmMainFrame::Open( void )
{
  fData->Open();

  ULong_t blue;
  gClient->GetColorByName( "blue", blue );
  fOpen->SetTextColor( blue   );
  fClse->SetTextColor( kBlack );
}

void AmMainFrame::Close( void )
{
  fData->Close();

  ULong_t blue;
  gClient->GetColorByName( "blue", blue );
  fOpen->SetTextColor( kBlack );
  fClse->SetTextColor( blue   );
}

void AmMainFrame::Clear( Option_t *opt )
{
  fData->Clear();
}

void AmMainFrame::HistSel( int i )
{
  ULong_t blue;
  gClient->GetColorByName( "blue", blue );
  fAtab->SetTab( 2 );
  fData->SetHsel( i );
  for ( Int_t j = 0; j < N_HIST; j++ ) {
    if ( j == i ) fZsel[j]->SetTextColor( blue   );
    else          fZsel[j]->SetTextColor( kBlack );
  }
  DrawEventHists();
}


void AmMainFrame::Reset( void )
{
  cmdPacket.cmnd    = CMD_REST;
  cmdPacket.addr    = 0x0;
  cmdPacket.data[0] = 0x0;
  cmdPacket.data[1] = 0x0;

  if ( !gCmndSnder->SendCommand( &cmdPacket ) )
    std::cerr << "<AmMainFrame::Reset> failed" << std::endl;
}

void AmMainFrame::Enable( void )
{
  cmdPacket.cmnd    = CMD_STRT;
  cmdPacket.addr    = 0x0;
  cmdPacket.data[0] = 0x0;
  cmdPacket.data[1] = 0x0;

  if ( gCmndSnder->SendCommand( &cmdPacket ) ) {
    fTrig = kTRUE;
    ULong_t blue;
    gClient->GetColorByName( "blue", blue );
    fEnbl->SetTextColor( blue   );
    fDsbl->SetTextColor( kBlack );

    //    fTimr->Start( 5000, kFALSE );
    //    fTimr->Start( 2000, kFALSE );
  }
  else
    std::cerr << "<AmMainFrame::Enable> failed" << std::endl;
}

void AmMainFrame::Disable( void )
{
  cmdPacket.cmnd    = CMD_STOP;
  cmdPacket.addr    = 0x0;
  cmdPacket.data[0] = 0x0;
  cmdPacket.data[1] = 0x0;

  if ( gCmndSnder->SendCommand( &cmdPacket ) ) {
    fTrig = kFALSE;
    ULong_t blue;
    gClient->GetColorByName( "blue", blue );
    fEnbl->SetTextColor( kBlack );
    fDsbl->SetTextColor( blue   );
    //    fTimr->Stop();
  }
  else
    std::cerr << "<AmMainFrame::Disable> failed" << std::endl;
}

void AmMainFrame::Set( void )
{
  if ( fTrig ) return;

  fMode->Set();
  fExtr->Set();
  for ( Int_t i = 0; i < N_CONN; i++ )
    fConn[i]->Set();
  fLemo->Set();
  fGpps->Set();
  for ( Int_t i = 0; i < N_SUBD+2; i++ )
    fWdth[i]->Set();
  for ( Int_t i = 0; i < N_CHAN; i++ )
    fChan[i]->Set();
}

void AmMainFrame::Get( void )
{
  if ( fTrig ) return;

  fMode->Get();
  fExtr->Get();
  for ( Int_t i = 0; i < N_CONN; i++ )
    fConn[i]->Get();
  fLemo->Get();
  fGpps->Get();
  for ( Int_t i = 0; i < N_SUBD+2; i++ )
    fWdth[i]->Get();
  for ( Int_t i = 0; i < N_CHAN; i++ )
    fChan[i]->Get();
}

void AmMainFrame::AutoRefresh( void )
{
  if ( !gDataRcver->IsConnected() ) return;

  gDataRcver->Process();

  DrawEventHists();
  PrintStatus   ();
  PrintEventID  ();
}

void AmMainFrame::AddCmnd( const char *cmnd )
{
  fVcom->AddLine( cmnd );
  fVcom->ShowBottom();
}

void AmMainFrame::AddEcho( const char *echo )
{
  fVech->AddLine( echo );
  fVech->ShowBottom();
}

void AmMainFrame::DrawEventHists( void )
{
  for ( unsigned int iraw = 0; iraw < N_RAW; iraw++ ) {
    for ( unsigned int icol = 0; icol < N_COL; icol++ ) {
      TCanvas *fCanvas = ((TRootEmbeddedCanvas *)fCarray->At( iraw*N_COL+icol ))->GetCanvas();
      fCanvas->cd();
      if ( iraw == N_RAW-1 && icol >= 2 ) {
	gPad->SetGridx( 0 );
      }
      else {
	gPad->SetGridx( 1 );
      }
      gDataRcver->DrawHist( iraw*N_COL+icol );
      fCanvas->Update();
    }
  }
  TCanvas *fCanvas = fZcan->GetCanvas();
  fCanvas->cd();
  gPad->SetGridx( 0 );
  gDataRcver->DrawZoom();
  fCanvas->Update();
}

void AmMainFrame::PrintStatus( void )
{
  time_t fNow;
  time( &fNow );

  unsigned long fNrcv = gDataRcver->GetNrcv();
  struct tm *ptr = localtime( &fNow );
  int year = ptr->tm_year;
  int mon  = ptr->tm_mon;
  int mday = ptr->tm_mday;
  int hour = ptr->tm_hour;
  int min  = ptr->tm_min;
  int sec  = ptr->tm_sec;
  
  static const char smon[12][4] = {
    "Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
  
  TString today = Form( "%04d%s%02d", 1900+year, smon[mon], mday );
  TString jikan = Form( "%02d:%02d:%02d", hour, min, sec );
  
  if ( fNrcv-fNprcv ) {
    std::cout << Form( "%8lu / %8lu event received at %s/%s", fNrcv-fNprcv, fNrcv, today.Data(), jikan.Data() )<< std::endl;
    fNzero = 0;
  }
  else {
    if ( fNzero%10 == 0 ) {
      std::cout << Form( "%8lu / %8lu event received at %s/%s", fNrcv-fNprcv, fNrcv, today.Data(), jikan.Data() )<< std::endl;
    }
    fNzero++;
  }
  
  fNprcv = fNrcv;
}

void AmMainFrame::PrintEventID( void )
{
  unsigned int evid = gDataRcver->GetEvID()+0x80000000;
  fEvid[0]->SetText( Form( "%010u", evid ) );
  fEvid[1]->SetText( Form( "%010u", evid ) );
  fEvid_hex[0]->SetText( Form( "0x%08X", evid ) );
  fEvid_hex[1]->SetText( Form( "0x%08X", evid ) );
}
