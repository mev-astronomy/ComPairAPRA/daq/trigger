#include "CmdHandler.h"
#include "RegHandler.h"

#include <stdlib.h>
#include <iostream>
#include <fstream>

#include <string.h>

CmdHandler::CmdHandler()
{
  regs.clear();
}

CmdHandler::~CmdHandler()
{
}

void CmdHandler::ReadTable()
{
  ifstream fin( REGISTER_TABLE );
  if ( !fin ) {
    std::cout << "File not found: " << REGISTER_TABLE << std::endl;
    return;
  }

  char buf[256];
  char cid[16];
  char val[32];

  int addr;

  unsigned int msb;
  unsigned int lsb;

  while ( 1 ) {
    fin >> buf;
    if ( fin.eof() ) break;
    if ( buf[0] == '#' ) {
      fin.getline( buf, 256 );
      continue;
    }

    addr = strtol( buf, NULL, 16 );
    fin >> cid >> msb >> lsb >> val;
    fin.getline( buf, 256 );

    struct regAttr reg;
    reg.id  = strtol( cid, NULL, 16 );
    reg.msb = msb;
    reg.lsb = lsb;
    regs[addr] = reg;
    vals[addr] = BtoI( val, msb, lsb );

    //    std::cout << hex << addr << " " << msb << " " << lsb << " " << vals[addr] << std::endl;
  }
}

void CmdHandler::SetRegAttr( int addr, struct regAttr reg )
{
  regs[addr] = reg;
}

void CmdHandler::GetRegAttr( int addr, struct regAttr *reg )
{
  *reg = regs[addr];
}

unsigned int CmdHandler::GetRegValue( int addr )
{
  if ( vals.find( addr ) != vals.end() ) return vals[addr];
  else return 0;
}

unsigned int CmdHandler::BtoI( char *cval, unsigned int msb, unsigned int lsb )
{
  // 10111111 --> 1<<7 + 0<<6 + 1<<5 + 1<<4 + 1<<3 + 1<<2 + 1<<1 + 1<<0

  unsigned int val = 0;
  for ( unsigned int i = 0; i <= msb-lsb; i++ ) {
    if ( cval[i] == '1' ) val += (1 << (msb-lsb-i));
  }

  return val;
}
