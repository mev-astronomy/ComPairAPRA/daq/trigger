#include "WdthRegister.h"
#include "RegNumberEntry.h"

#include "TGNumberEntry.h"
#include "TGLabel.h"

ClassImp( WdthRegister )

WdthRegister::WdthRegister( const TGWindow *p, TGString *title, UInt_t options ) : BaseRegister( p, title, options )
{
}

WdthRegister::~WdthRegister()
{
}

void WdthRegister::Init( void )
{  
  fEntry = (RegNumberEntry *)fArr->At( 0 );

  TGHorizontalFrame *hfrm
    = new TGHorizontalFrame( this, 120, 120, kDoubleBorder );
  
  TGLabel *lb = new TGLabel( hfrm, "Width" );
  hfrm->AddFrame( lb, new TGLayoutHints( kLHintsBottom | kLHintsExpandX, 3, 0, 0, 0 ) );
  TGNumberEntry *ne = new TGNumberEntry( hfrm, 1, 4, -1, TGNumberEntry::kNESInteger, TGNumberEntry::kNEANonNegative, TGNumberEntry::kNELLimitMinMax, 1, 255 );
  ne->Connect( "ValueSet(Long_t)", "WdthRegister", this, "Status()" );
  hfrm->AddFrame( ne, new TGLayoutHints( 0, 0, 0, 3, 0 ) );
  fEntry->SetNumberEntry( ne );

  AddFrame( hfrm, new TGLayoutHints( kLHintsExpandX, 3, 0, 0, 0 ) );
}
