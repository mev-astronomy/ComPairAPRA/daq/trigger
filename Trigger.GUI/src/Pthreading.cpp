/* $Id: Pthreading.cc,v 1.8 2008/03/11 11:01:24 kusumoto Exp $ */
#include "Pthreading.h"
#include <unistd.h>
#include <iostream>

//#define DEBUG

using namespace std;

pthread_mutex_t Pthreading::boot_lock = PTHREAD_MUTEX_INITIALIZER;

Pthreading::Pthreading( const char *thre_name )
{
  status  = 0;
  proc_id = 0;
  thread_name = thre_name;
}

Pthreading::~Pthreading()
{
}

void* Pthreading::Boot( void *addr )
{
#ifdef DEBUG
  cout << "Start thread : "
       << "[" << ((Pthreading*)addr)->GetTid() << "] "
       << ((Pthreading*)addr)->GetThrName() << endl;
#endif
  ((Pthreading*)addr)->proc_id = getpid();
  ((Pthreading*)addr)->RunThread();

  return NULL;
}

void Pthreading::StartThread( void )
{
  pthread_mutex_lock( &boot_lock );

  pthread_t tid;
  pthread_create( &tid, NULL, Boot, this );
  pthread_detach( tid );

  pthread_mutex_unlock( &boot_lock );

  usleep( 1000 );
}


